<?php

namespace Tests\Day\Y2018;

use App\Day\Y2018\Day2;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day2>
 */
class Day2Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield ['abcdef bababc abbcde abcccd aabcdd abcdee ababab', 12];
    }

    /**
     * @return \Generator<string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield ['abcde fghij klmno pqrst fguij axcye wvxyz', 'fgij'];
    }
}
