<?php

namespace Tests\Day\Y2018;

use App\Day\Y2018\Day1;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day1>
 */
class Day1Test extends DayTestCase
{
    public static function getExamplesPart1(): \Generator
    {
        yield ['+1, -2, +3, +1', 3];
        yield ['+1, +1, +1', 3];
        yield ['+1, +1, -2', 0];
        yield ['-1, -2, -3', -6];
    }

    public static function getExamplesPart2(): \Generator
    {
        yield ['+1, -2, +3, +1, +1, -2', 2];
        yield ['+1, -1', 0];
        yield ['+3, +3, +4, -2, -4', 10];
        yield ['-6, +3, +8, +5, -6', 5];
    }
}
