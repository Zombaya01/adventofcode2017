<?php

declare(strict_types=1);

namespace Tests\Day\Y2023;

use App\Day\Y2023\Day2;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day2>
 */
class Day2Test extends DayTestCase
{
    public const EXAMPLE = <<<'EOF'
        Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
        Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
        Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
        Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            8,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            2286,
        ];
    }
}
