<?php

declare(strict_types=1);

namespace Tests\Day\Y2023;

use App\Day\Y2023\Day5;
use App\Day\Y2023\Day5\Day5Converter;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day5>
 */
class Day5Test extends DayTestCase
{
    public const EXAMPLE = <<<'EOF'
        seeds: 79 14 55 13

        seed-to-soil map:
        50 98 2
        52 50 48
        
        soil-to-fertilizer map:
        0 15 37
        37 52 2
        39 0 15
        
        fertilizer-to-water map:
        49 53 8
        0 11 42
        42 0 7
        57 7 4
        
        water-to-light map:
        88 18 7
        18 25 70
        
        light-to-temperature map:
        45 77 23
        81 45 19
        68 64 13
        
        temperature-to-humidity map:
        0 69 1
        1 0 69
        
        humidity-to-location map:
        60 56 37
        56 93 4
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            35,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            46,
        ];
    }

    #[DataProvider('provide_Day5Converter_transform_cases')]
    public function testDay5ConverterTransform(string $input, int $value, int $expected): void
    {
        $converter = new Day5Converter($input);
        $actual    = $converter->transform($value);
        $this->assertEquals($expected, $actual);
    }

    public static function provide_Day5Converter_transform_cases(): iterable
    {
        $example1 = <<<'EOF'
            50 98 2
            52 50 48
            EOF;

        yield [
            $example1,
            98,
            50,
        ];

        yield [
            $example1,
            99,
            51,
        ];

        yield [
            $example1,
            100,
            100,
        ];
        yield 'non-mapped' => [
            $example1,
            10,
            10,
        ];
    }

    #[DataProvider('provide_Day5Converter_transformRanges_cases')]
    public function testDay5ConverterTransformRanges(string $input, array $ranges, array $expected): void
    {
        $converter = new Day5Converter($input);
        $actual    = $converter->transformRanges($ranges);
        $this->assertEquals($expected, $actual);
    }

    public static function provide_Day5Converter_transformRanges_cases(): iterable
    {
        $example1 = <<<'EOF'
            50 98 2
            52 50 48
            EOF;

        yield 'complete-in-1' => [
            $example1,
            [[98, 1]],
            [[50, 1]],
        ];

        yield 'complete-in-2' => [
            $example1,
            [[98, 2]],
            [[50, 2]],
        ];

        yield 'split-and-without-range' => [
            $example1,
            [[98, 3]],
            [[50, 2], [100, 1]],
        ];

        yield 'split-over-multiple-ranges' => [
            $example1,
            [[96, 4]],
            [[98, 2], [50, 2]],
        ];

        yield 'completely-out-range' => [
            $example1,
            [[100, 3]],
            [[100, 3]],
        ];

        yield 'multiple-out-range' => [
            $example1,
            [[10, 5], [15, 5]],
            [[10, 5], [15, 5]],
        ];
    }
}
