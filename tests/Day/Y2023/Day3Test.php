<?php

declare(strict_types=1);

namespace Tests\Day\Y2023;

use App\Day\Y2023\Day3;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day3>
 */
class Day3Test extends DayTestCase
{
    public const EXAMPLE = <<<'EOF'
        467..114..
        ...*......
        ..35..633.
        ......#...
        617*......
        .....+.58.
        ..592.....
        ......755.
        ...$.*....
        .664.598..
        EOF;
    public const EXAMPLE2 = <<<'EOF'
        467..114..
        ...*......
        ..35..633.
        ......#...
        617*......
        .....+.58.
        ..592.....
        ......755.
        ...$.*....
        .664.598.1
        EOF;

    public const EXAMPLE3 = <<<'EOF'
        467..114..
        ...*....1=
        ..35..633.
        ......#...
        617*......
        .....+....
        ..592...=1
        ......755.
        ...$.*....
        .664.598..
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            4361,
        ];
        yield 'example2' => [
            self::EXAMPLE2,
            4361,
        ];
        yield 'example3' => [
            self::EXAMPLE3,
            4363,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            467835,
        ];
    }
}
