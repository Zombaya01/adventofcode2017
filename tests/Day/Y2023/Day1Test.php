<?php

declare(strict_types=1);

namespace Tests\Day\Y2023;

use App\Day\Y2023\Day1;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day1>
 */
class Day1Test extends DayTestCase
{
    public const EXAMPLE = <<<'EOF'
        1abc2
        pqr3stu8vwx
        a1b2c3d4e5f
        treb7uchet
        EOF;

    public const EXAMPLE_2 = <<<'EOF'
        two1nine
        eightwothree
        abcone2threexyz
        xtwone3four
        4nineeightseven2
        zoneight234
        7pqrstsixteen
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            142,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE_2,
            281,
        ];
    }
}
