<?php

declare(strict_types=1);

namespace Tests\Day\Y2022\Day15;

use App\Day\Y2022\Day15\Sensor;
use PHPUnit\Framework\TestCase;

class SensorTest extends TestCase
{
    #[\PHPUnit\Framework\Attributes\DataProvider('provideGetLength')]
    public function testGetLength(string $input, int $expectedLength): void
    {
        $sensor = new Sensor($input);
        $this->assertEquals($expectedLength,$sensor->getLength());
    }

    /** @return iterable<mixed> */
    public static function provideGetLength(): iterable
    {
        yield [
            'Sensor at x=8, y=7: closest beacon is at x=2, y=10',
            9,
        ];
    }

    /**
     * @param array<array<int>> $expectedRange
     */
    #[\PHPUnit\Framework\Attributes\DataProvider('provideGetRangeAt')]
    public function testGetRangeAt(string $input, int $y, array $expectedRange): void
    {
        $sensor = new Sensor($input);
        $this->assertEquals($expectedRange,$sensor->getRangeAt($y));
    }
    /*
               1    1    2    2
     01234567890    5    0    5
-2 ..........#.................
-1 .........###................
 0 ....S...#####...............
 1 .......#######........S.....
 2 ......#########S............
 3 .....###########SB..........
 4 ....#############...........
 5 ...###############..........
 6 ..#################.........
 7 .#########S#######S#........
 8 ..#################.........
 9 ...###############..........
10 ....B############...........
11 ..S..###########............
12 ......#########.............
13 .......#######..............
14 ........#####.S.......S.....
15 B........###................
16 ..........#SB...............
17 ................S..........B
18 ....S.......................
19 ............................
20 ............S......S........
21 ............................
22 .......................B....
     */

    /** @return iterable<mixed> */
    public static function provideGetRangeAt(): iterable
    {
        yield [
            'Sensor at x=8, y=7: closest beacon is at x=2, y=10',
            -3,
            [],
        ];
        yield [
            'Sensor at x=8, y=7: closest beacon is at x=2, y=10',
            -2,
            [8, 8],
        ];
        yield [
            'Sensor at x=8, y=7: closest beacon is at x=2, y=10',
            0,
            [6, 10],
        ];
        yield [
            'Sensor at x=8, y=7: closest beacon is at x=2, y=10',
            16,
            [8, 8],
        ];
        yield [
            'Sensor at x=8, y=7: closest beacon is at x=2, y=10',
            14,
            [6, 10],
        ];
        yield [
            'Sensor at x=8, y=7: closest beacon is at x=2, y=10',
            17,
            [],
        ];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('provideIsInRange')]
    public function testIsInRange(string $input, int $x, int $y, bool $expected): void
    {
        $this->assertEquals($expected, (new Sensor($input))->isInRange($x,$y));
    }

    public static function provideIsInRange(): iterable
    {
        // length = 9
        $input = 'Sensor at x=8, y=7: closest beacon is at x=2, y=10';

        yield [
            $input,
            8,
            16,
            true,
        ];
        yield [
            $input,
            8,
            17,
            false,
        ];

        yield [
            $input,
            8,
            -2,
            true,
        ];
        yield [
            $input,
            8,
            -3,
            false,
        ];

        yield [
            $input,
            -1,
            7,
            true,
        ];
        yield [
            $input,
            -2,
            7,
            false,
        ];

        yield [
            $input,
            17,
            7,
            true,
        ];
        yield [
            $input,
            18,
            7,
            false,
        ];

        yield [
            $input,
            16,
            8,
            true,
        ];
        yield [
            $input,
            16,
            9,
            false,
        ];
    }
}
