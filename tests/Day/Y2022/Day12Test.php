<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day12;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day12>
 */
class Day12Test extends DayTestCase
{
    public const EXAMPLE = <<<'EOF'
        Sabqponm
        abcryxxl
        accszExk
        acctuvwj
        abdefghi
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            31,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            29,
        ];
    }
}
