<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day16;
use Tests\BaseDayTestCase;
use Tests\TestDay1Trait;

/**
 * @extends BaseDayTestCase<Day16>
 */
class Day16Test extends BaseDayTestCase
{
    use TestDay1Trait;

    public const EXAMPLE = <<<'EOF'
        Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
        Valve BB has flow rate=13; tunnels lead to valves CC, AA
        Valve CC has flow rate=2; tunnels lead to valves DD, BB
        Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
        Valve EE has flow rate=3; tunnels lead to valves FF, DD
        Valve FF has flow rate=0; tunnels lead to valves EE, GG
        Valve GG has flow rate=0; tunnels lead to valves FF, HH
        Valve HH has flow rate=22; tunnel leads to valve GG
        Valve II has flow rate=0; tunnels lead to valves AA, JJ
        Valve JJ has flow rate=21; tunnel leads to valve II
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            1651,
        ];
    }

    public function testParseInput(): void
    {
        $valves = self::getDay()->parseInput(self::EXAMPLE);
        $this->assertCount(10,$valves);
        $this->assertEquals('AA',$valves['AA']->name);
        $this->assertCount(3,$valves['AA']->getNeighbouringValves());
        $this->assertEquals('DD',$valves['AA']->getNeighbouringValves()[0]->name);
        $this->assertEquals(1,$valves['AA']->getDistance($valves['BB']));
    }

    public function testToUml(): void
    {
        $day    = self::getDay();
        $valves = $day->parseInput(self::EXAMPLE);
        $uml    = $day->toUml($valves);

        $this->markTestSkipped();
    }
}
