<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day3;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day3>
 */
class Day3Test extends DayTestCase
{
    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [<<<EOF
            vJrwpWtwJgWrhcsFMMfFFhFp
            jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
            PmmdzqPrVvPwwTWBwg
            wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
            ttgJtRGJQctTZtZT
            CrZsJsPPZsGzwwsLwLmpwMDw
            EOF,
            157,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [<<<EOF
            vJrwpWtwJgWrhcsFMMfFFhFp
            jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
            PmmdzqPrVvPwwTWBwg
            wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
            ttgJtRGJQctTZtZT
            CrZsJsPPZsGzwwsLwLmpwMDw
            EOF,
            70,
        ];
    }
}
