<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day5;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day5>
 */
class Day5Test extends DayTestCase
{
    private const EXAMPLE = <<<'EOF'
            [D]    
        [N] [C]    
        [Z] [M] [P]
         1   2   3 
        
        move 1 from 2 to 1
        move 3 from 1 to 3
        move 2 from 2 to 1
        move 1 from 1 to 2
        EOF;

    public function testParseStackInput(): void
    {
        $expected = [
            1 => ['Z', 'N'],
            2 => ['M', 'C', 'D'],
            3 => ['P'],
        ];

        $input =  new Day5\Input(self::EXAMPLE);

        $this->assertEquals($expected, $input->stacks);
        $this->assertCount(4,$input->moves);
        $this->assertEquals(1,$input->moves[0]->amount);
        $this->assertEquals(2,$input->moves[0]->from);
        $this->assertEquals(1,$input->moves[0]->to);
    }

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            'CMZ',
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            'MCD',
        ];
    }
}
