<?php

declare(strict_types=1);

namespace Tests\Day\Y2022\Day14;

use App\Day\Y2022\Day14\Cave;
use PHPUnit\Framework\TestCase;
use Tests\Day\Y2022\Day14Test;

class CaveTest extends TestCase
{
    public function testPrintExample(): void
    {
        $expected = <<<'EOF'
            ..........
            ..........
            ..........
            ..........
            ....#...##
            ....#...#.
            ..###...#.
            ........#.
            ........#.
            #########.
            EOF;
        $cave = new Cave(Day14Test::EXAMPLE);
        $this->assertEquals($expected,(string) $cave);

        // 1 kernel
        $cave->dropSandKernel(500);
        $expected = <<<'EOF'
            ..........
            ..........
            ..........
            ..........
            ....#...##
            ....#...#.
            ..###...#.
            ........#.
            ......o.#.
            #########.
            EOF;
        $this->assertEquals($expected,(string) $cave);

        // 2 kernel
        $cave->dropSandKernel(500);
        $expected = <<<'EOF'
            ..........
            ..........
            ..........
            ..........
            ....#...##
            ....#...#.
            ..###...#.
            ........#.
            .....oo.#.
            #########.
            EOF;
        $this->assertEquals($expected,(string) $cave);

        // 5 kernel
        $cave->dropSandKernel(500,3);
        $expected = <<<'EOF'
            ..........
            ..........
            ..........
            ..........
            ....#...##
            ....#...#.
            ..###...#.
            ......o.#.
            ....oooo#.
            #########.
            EOF;
        $this->assertEquals($expected,(string) $cave);

        // 22 kernel
        $cave->dropSandKernel(500,17);
        $expected = <<<'EOF'
            ..........
            ..........
            ......o...
            .....ooo..
            ....#ooo##
            ....#ooo#.
            ..###ooo#.
            ....oooo#.
            ...ooooo#.
            #########.
            EOF;
        $this->assertEquals($expected,(string) $cave);

        // 24 kernel
        $cave->dropSandKernel(500,2);
        $expected = <<<'EOF'
            ..........
            ..........
            ......o...
            .....ooo..
            ....#ooo##
            ...o#ooo#.
            ..###ooo#.
            ....oooo#.
            .o.ooooo#.
            #########.
            EOF;
        $this->assertEquals($expected,(string) $cave);

        // Should be same
        $cave->dropSandKernel(500,1);
        $expected = <<<'EOF'
            ..........
            ..........
            ......o...
            .....ooo..
            ....#ooo##
            ...o#ooo#.
            ..###ooo#.
            ....oooo#.
            .o.ooooo#.
            #########.
            EOF;
        $this->assertEquals($expected,(string) $cave);
    }
}
