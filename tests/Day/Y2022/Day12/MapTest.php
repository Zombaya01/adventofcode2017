<?php

declare(strict_types=1);

namespace Tests\Day\Y2022\Day12;

use App\Day\Y2022\Day12\Map;
use PHPUnit\Framework\TestCase;
use Tests\Day\Y2022\Day12Test;

class MapTest extends TestCase
{
    public function testExample(): void
    {
        $map = new Map(Day12Test::EXAMPLE,true);
        $this->assertSame(str_replace(['S', 'E'],['a', 'z'],Day12Test::EXAMPLE), (string) $map);
    }
}
