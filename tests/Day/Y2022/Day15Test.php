<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day15;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day15>
 */
class Day15Test extends DayTestCase
{
    public const EXAMPLE = <<<'EOF'
        Sensor at x=2, y=18: closest beacon is at x=-2, y=15
        Sensor at x=9, y=16: closest beacon is at x=10, y=16
        Sensor at x=13, y=2: closest beacon is at x=15, y=3
        Sensor at x=12, y=14: closest beacon is at x=10, y=16
        Sensor at x=10, y=20: closest beacon is at x=10, y=16
        Sensor at x=14, y=17: closest beacon is at x=10, y=16
        Sensor at x=8, y=7: closest beacon is at x=2, y=10
        Sensor at x=2, y=0: closest beacon is at x=2, y=10
        Sensor at x=0, y=11: closest beacon is at x=2, y=10
        Sensor at x=20, y=14: closest beacon is at x=25, y=17
        Sensor at x=17, y=20: closest beacon is at x=21, y=22
        Sensor at x=16, y=7: closest beacon is at x=15, y=3
        Sensor at x=14, y=3: closest beacon is at x=15, y=3
        Sensor at x=20, y=1: closest beacon is at x=15, y=3
        EOF;

    public function testExamplePart1(): void
    {
        $actual = self::getDay()->calculatePart1(self::EXAMPLE,10);
        $this->assertEquals(26,$actual);
    }

    public function testExamplePart2(): void
    {
        $actual = self::getDay()->calculatePart2(self::EXAMPLE,20);
        $this->assertEquals(56000011,$actual);
    }

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::getPuzzleInput(),
            4861076,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::getPuzzleInput(),
            10649103160102,
        ];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('provideMergeRanges')]
    public function testMergeRanges(array $input, array $expected): void
    {
        $this->assertEquals($expected, self::getDay()->mergeRanges($input));
    }

    /**
     * @return iterable<array{0: array, 1: array}>
     */
    public static function provideMergeRanges(): iterable
    {
        yield 'empties' => [
            [[], [], []],
            [],
        ];
        yield 'empties-with-one' => [
            [[], [1, 1], []],
            [[1, 1]],
        ];
        yield 'non-overlap' => [
            [[1, 2], [4, 5]],
            [[1, 2], [4, 5]],
        ];
        yield 'non-overlap-unsorted' => [
            [[4, 5], [1, 2]],
            [[1, 2], [4, 5]],
        ];
        yield 'continious' => [
            [[1, 2], [3, 4]],
            [[1, 4]],
        ];
        yield 'within' => [
            [[1, 4], [2, 3]],
            [[1, 4]],
        ];
        yield 'overlapping' => [
            [[1, 3], [2, 4]],
            [[1, 4]],
        ];
        yield 'overlapping-and-next' => [
            [[1, 3], [2, 4], [6, 7]],
            [[1, 4], [6, 7]],
        ];
        yield 'overlapping-and-continious' => [
            [[1, 3], [2, 4], [5, 7]],
            [[1, 7]],
        ];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('provideIsNeverInRange')]
    public function testIsNeverInRange(string $sensorInput, int $x, int $y, bool $expected): void
    {
        $day     = self::getDay();
        $sensors = $day->toSensorArray($sensorInput);

        $this->assertEquals($expected, $day->isNeverInRange($sensors,$x,$y));
    }

    public static function provideIsNeverInRange(): iterable
    {
        yield [
            self::EXAMPLE,
            14,
            11,
            true,
        ];
        yield [
            self::EXAMPLE,
            14,
            10,
            false,
        ];
        yield [
            self::EXAMPLE,
            14,
            12,
            false,
        ];
    }
}
