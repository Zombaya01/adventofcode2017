<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day2;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day2>
 */
class Day2Test extends DayTestCase
{
    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [<<<EOF
            A Y
            B X
            C Z
            EOF,
            15,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [<<<EOF
            A Y
            B X
            C Z
            EOF,
            12,
        ];
    }
}
