<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day13;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day13>
 */
class Day13Test extends DayTestCase
{
    private const EXAMPLE = <<<'EOF'
        [1,1,3,1,1]
        [1,1,5,1,1]
        
        [[1],[2,3,4]]
        [[1],4]
        
        [9]
        [[8,7,6]]
        
        [[4,4],4,4]
        [[4,4],4,4,4]
        
        [7,7,7,7]
        [7,7,7]
        
        []
        [3]
        
        [[[]]]
        [[]]
        
        [1,[2,[3,[4,[5,6,7]]]],8,9]
        [1,[2,[3,[4,[5,6,0]]]],8,9]
        EOF;

    public static function getExamplesPart1(): iterable
    {
        $f = static fn (int $example): string => explode("\n\n",self::EXAMPLE)[$example] ?? '';

        yield 'example' => [
            self::EXAMPLE,
            13,
        ];

        yield 'example1' => [
            $f(0),
            1,
        ];
        yield 'example2' => [
            $f(1),
            1,
        ];
        yield 'example3' => [
            $f(2),
            0,
        ];
        yield 'example4' => [
            $f(3),
            1,
        ];
        yield 'example5' => [
            $f(4),
            0,
        ];
        yield 'example6' => [
            $f(5),
            1,
        ];
        yield 'example7' => [
            $f(6),
            0,
        ];
        yield 'example8' => [
            $f(7),
            0,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            140,
        ];
    }
}
