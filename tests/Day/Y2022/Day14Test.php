<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day14;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day14>
 */
class Day14Test extends DayTestCase
{
    public const EXAMPLE = <<<'EOF'
        498,4 -> 498,6 -> 496,6
        503,4 -> 502,4 -> 502,9 -> 494,9
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            24,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            93,
        ];
    }
}
