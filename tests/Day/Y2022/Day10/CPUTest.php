<?php

declare(strict_types=1);

namespace Tests\Day\Y2022\Day10;

use App\Day\Y2022\Day10\CPU;
use PHPUnit\Framework\TestCase;
use Tests\Day\Y2022\Day10Test;

class CPUTest extends TestCase
{
    public function testSmallExample(): void
    {
        $input = explode(
            "\n",
            <<<'EOF'
                noop
                addx 3
                addx -5
                EOF
        );
        $cpu = new CPU($input);

        // 0
        $this->assertSame(1,$cpu->getRegister('x'));

        // 1
        $cpu->tick();
        $this->assertSame(1,$cpu->getRegister('x'));

        // 2
        $cpu->tick();
        $this->assertSame(1,$cpu->getRegister('x'));

        // 3
        $cpu->tick();
        $this->assertSame(4,$cpu->getRegister('x'));

        // 4
        $cpu->tick();
        $this->assertSame(4,$cpu->getRegister('x'));

        // 5
        $cpu->tick();
        $this->assertSame(-1,$cpu->getRegister('x'));

        // 5
        $cpu->tick();
        $this->assertSame(-1,$cpu->getRegister('x'));

        // 5
        $cpu->tick();
        $this->assertSame(-1,$cpu->getRegister('x'));

        // 5
        $cpu->tick();
        $this->assertSame(-1,$cpu->getRegister('x'));

        // 5
        $cpu->tick();
        $this->assertSame(-1,$cpu->getRegister('x'));

        // 5
        $cpu->tick();
        $this->assertSame(-1,$cpu->getRegister('x'));
    }

    public function testLargeExample(): void
    {
        $cpu = new CPU(explode("\n",Day10Test::EXAMPLE));

        // 20
        $cpu->ticks(19);
        $this->assertSame(21, $cpu->getRegister('x'));

        // 60
        $cpu->ticks(40);
        $this->assertSame(19, $cpu->getRegister('x'));

        // 100
        $cpu->ticks(40);
        $this->assertSame(18, $cpu->getRegister('x'));

        // 140
        $cpu->ticks(40);
        $this->assertSame(21, $cpu->getRegister('x'));

        // 180
        $cpu->ticks(40);
        $this->assertSame(16, $cpu->getRegister('x'));

        // 220
        $cpu->ticks(40);
        $this->assertSame(18, $cpu->getRegister('x'));
    }
}
