<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day1;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day1>
 */
class Day1Test extends DayTestCase
{
    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [<<<EOF
            1000
            2000
            3000
            
            4000
            
            5000
            6000
            
            7000
            8000
            9000
            
            10000
            EOF,
            24000,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [<<<EOF
            1000
            2000
            3000
            
            4000
            
            5000
            6000
            
            7000
            8000
            9000
            
            10000
            EOF,
            45000,
        ];
    }
}
