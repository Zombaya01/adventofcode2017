<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day6;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day6>
 */
class Day6Test extends DayTestCase
{
    private const EXAMPLE1 = <<<'EOF'
        mjqjpqmgbljsphdztnvjfqwrcgsmlb
        EOF;
    private const EXAMPLE2 = <<<'EOF'
        bvwbjplbgvbhsrlpgdmjqwftvncz
        EOF;
    private const EXAMPLE3 = <<<'EOF'
        nppdvjthqldpwncqszvftbrmjlhg
        EOF;
    private const EXAMPLE4 = <<<'EOF'
        nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg
        EOF;
    private const EXAMPLE5 = <<<'EOF'
        zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield [
            self::EXAMPLE1,
            7,
        ];
        yield [
            self::EXAMPLE2,
            5,
        ];
        yield [
            self::EXAMPLE3,
            6,
        ];
        yield [
            self::EXAMPLE4,
            10,
        ];
        yield [
            self::EXAMPLE5,
            11,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield [
            self::EXAMPLE1,
            19,
        ];
        yield [
            self::EXAMPLE2,
            23,
        ];
        yield [
            self::EXAMPLE3,
            23,
        ];
        yield [
            self::EXAMPLE4,
            29,
        ];
        yield [
            self::EXAMPLE5,
            26,
        ];
    }
}
