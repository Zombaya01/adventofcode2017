<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day9;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day9>
 */
class Day9Test extends DayTestCase
{
    private const EXAMPLE = <<<'EOF'
        R 4
        U 4
        L 3
        D 1
        R 4
        D 1
        L 5
        R 2
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            13,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            1,
        ];

        yield 'example2' => [
            <<<'EOF'
                R 5
                U 8
                L 8
                D 3
                R 17
                D 10
                L 25
                U 20
                EOF,
            36,
        ];
    }
}
