<?php

declare(strict_types=1);

namespace Tests\Day\Y2022\Day11;

use App\Day\Y2022\Day11;
use App\Day\Y2022\Day11\Group;
use PHPUnit\Framework\TestCase;
use Tests\Day\Y2022\Day11Test;

class GroupTest extends TestCase
{
    public function testExample(): void
    {
        $group = new Group(Day11Test::EXAMPLE, Day11::part1WorryReducer());

        $expectedAtStart = <<<'EOF'
            Monkey 0: 79, 98
            Monkey 1: 54, 65, 75, 74
            Monkey 2: 79, 60, 97
            Monkey 3: 74
            EOF;

        $this->assertSame($expectedAtStart, (string) $group);

        $group->rounds();
        $expectedAfterRound1 = <<<'EOF'
            Monkey 0: 20, 23, 27, 26
            Monkey 1: 2080, 25, 167, 207, 401, 1046
            Monkey 2: 
            Monkey 3: 
            EOF;
        $this->assertSame($expectedAfterRound1, (string) $group);

        $group->rounds(19);
        $expectedAfterRound20 = <<<'EOF'
            Monkey 0: 10, 12, 14, 26, 34
            Monkey 1: 245, 93, 53, 199, 115
            Monkey 2: 
            Monkey 3: 
            EOF;
        $this->assertSame($expectedAfterRound20, (string) $group);
    }

    public function testExampleWithInspectionsOutput(): void
    {
        $expected = <<<'EOF'
            == After round 1 ==
            Monkey 0 inspected items 2 times.
            Monkey 1 inspected items 4 times.
            Monkey 2 inspected items 3 times.
            Monkey 3 inspected items 6 times.
            
            == After round 20 ==
            Monkey 0 inspected items 99 times.
            Monkey 1 inspected items 97 times.
            Monkey 2 inspected items 8 times.
            Monkey 3 inspected items 103 times.
            
            == After round 1000 ==
            Monkey 0 inspected items 5204 times.
            Monkey 1 inspected items 4792 times.
            Monkey 2 inspected items 199 times.
            Monkey 3 inspected items 5192 times.
            
            == After round 2000 ==
            Monkey 0 inspected items 10419 times.
            Monkey 1 inspected items 9577 times.
            Monkey 2 inspected items 392 times.
            Monkey 3 inspected items 10391 times.
            
            == After round 3000 ==
            Monkey 0 inspected items 15638 times.
            Monkey 1 inspected items 14358 times.
            Monkey 2 inspected items 587 times.
            Monkey 3 inspected items 15593 times.
            
            == After round 4000 ==
            Monkey 0 inspected items 20858 times.
            Monkey 1 inspected items 19138 times.
            Monkey 2 inspected items 780 times.
            Monkey 3 inspected items 20797 times.
            
            == After round 5000 ==
            Monkey 0 inspected items 26075 times.
            Monkey 1 inspected items 23921 times.
            Monkey 2 inspected items 974 times.
            Monkey 3 inspected items 26000 times.
            
            == After round 6000 ==
            Monkey 0 inspected items 31294 times.
            Monkey 1 inspected items 28702 times.
            Monkey 2 inspected items 1165 times.
            Monkey 3 inspected items 31204 times.
            
            == After round 7000 ==
            Monkey 0 inspected items 36508 times.
            Monkey 1 inspected items 33488 times.
            Monkey 2 inspected items 1360 times.
            Monkey 3 inspected items 36400 times.
            
            == After round 8000 ==
            Monkey 0 inspected items 41728 times.
            Monkey 1 inspected items 38268 times.
            Monkey 2 inspected items 1553 times.
            Monkey 3 inspected items 41606 times.
            
            == After round 9000 ==
            Monkey 0 inspected items 46945 times.
            Monkey 1 inspected items 43051 times.
            Monkey 2 inspected items 1746 times.
            Monkey 3 inspected items 46807 times.
            
            == After round 10000 ==
            Monkey 0 inspected items 52166 times.
            Monkey 1 inspected items 47830 times.
            Monkey 2 inspected items 1938 times.
            Monkey 3 inspected items 52013 times.
            
            
            EOF;

        $rounds = [1, 19, 980, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000];
        $group  = new Group(Day11Test::EXAMPLE, Day11::part2WorryReducer(Day11Test::EXAMPLE));

        $actual = '';
        foreach ($rounds as $round) {
            $group->rounds($round);
            $actual .= $group->showInspections();
        }

        $this->assertEquals($expected,$actual);
    }
}
