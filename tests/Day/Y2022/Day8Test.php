<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day8;
use App\Utils\DataConverter;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day8>
 */
class Day8Test extends DayTestCase
{
    private const EXAMPLE = <<<'EOF'
        30373
        25512
        65332
        33549
        35390
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            21,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            8,
        ];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('provideCalculateVisibleTreeScore')]
    public function testCalculateVisibleTreeScore(array $forrest, int $y, int $x, int $expectedAmount): void
    {
        $actual = self::getDay()->calculateVisibleTreeScore($forrest,$y,$x);
        $this->assertSame($expectedAmount,$actual);
    }

    public static function provideCalculateVisibleTreeScore(): iterable
    {
        yield [
            DataConverter::tableOfIntNumbers(self::EXAMPLE),
            1,
            2,
            4,
        ];
        yield [
            DataConverter::tableOfIntNumbers(self::EXAMPLE),
            3,
            2,
            8,
        ];
    }
}
