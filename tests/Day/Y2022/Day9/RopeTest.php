<?php

declare(strict_types=1);

namespace Tests\Day\Y2022\Day9;

use App\Day\Y2022\Day9\Rope;
use PHPUnit\Framework\TestCase;

class RopeTest extends TestCase
{
    public function testMoveHeadExample(): void
    {
        $rope = new Rope(2);

        $this->assertSame('0:0',$rope->getHead());
        $this->assertSame('0:0',$rope->getTail());

        $rope->moveHead('R');
        $this->assertSame('0:1',$rope->getHead());
        $this->assertSame('0:0',$rope->getTail());

        $rope->moveHead('R');
        $this->assertSame('0:2',$rope->getHead());
        $this->assertSame('0:1',$rope->getTail());

        $rope->moveHead('R');
        $this->assertSame('0:3',$rope->getHead());
        $this->assertSame('0:2',$rope->getTail());

        $rope->moveHead('R');
        $this->assertSame('0:4',$rope->getHead());
        $this->assertSame('0:3',$rope->getTail());

        $rope->moveHead('U');
        $this->assertSame('1:4',$rope->getHead());
        $this->assertSame('0:3',$rope->getTail());

        $rope->moveHead('U');
        $this->assertSame('2:4',$rope->getHead());
        $this->assertSame('1:4',$rope->getTail());
    }

    public function testMoveHead(): void
    {
        $rope = new Rope(2);

        $this->assertSame('0:0',$rope->getHead());
        $this->assertSame('0:0',$rope->getTail());

        $rope->moveHead('U');
        $this->assertSame('1:0',$rope->getHead());
        $this->assertSame('0:0',$rope->getTail());

        $rope->moveHead('U');
        $this->assertSame('2:0',$rope->getHead());
        $this->assertSame('1:0',$rope->getTail());

        $rope->moveHead('R');
        $this->assertSame('2:1',$rope->getHead());
        $this->assertSame('1:0',$rope->getTail());

        $rope->moveHead('R');
        $this->assertSame('2:2',$rope->getHead());
        $this->assertSame('2:1',$rope->getTail());
    }
}
