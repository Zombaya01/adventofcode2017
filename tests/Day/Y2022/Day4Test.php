<?php

declare(strict_types=1);

namespace Tests\Day\Y2022;

use App\Day\Y2022\Day4;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day4>
 */
class Day4Test extends DayTestCase
{
    /**
     * @return iterable{array{int,string}}
     */
    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [<<<EOF
            2-4,6-8
            2-3,4-5
            5-7,7-9
            2-8,3-7
            6-6,4-6
            2-6,4-8
            EOF,
            2,
        ];
        yield 'example-2' => [<<<EOF
            6-8,2-4
            4-5,2-3
            7-9,5-7
            3-7,2-8
            4-6,6-6
            4-8,2-6
            EOF,
            2,
        ];
        yield 'example-3' => [<<<EOF
            6-8,6-7
            4-5,2-3
            7-9,5-7
            3-7,2-8
            4-6,6-6
            4-8,2-6
            EOF,
            3,
        ];
        yield 'example-4' => [<<<EOF
            1006-1008,1006-1007
            4-5,2-3
            7-9,5-7
            3-7,2-8
            4-6,6-6
            4-8,2-6
            EOF,
            3,
        ];
        yield 'example-5' => [<<<EOF
            1006-1008,1006-1008
            EOF,
            1,
        ];
    }

    /**
     * @return iterable{array{int,string}}
     */
    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [<<<EOF
            2-4,6-8
            2-3,4-5
            5-7,7-9
            2-8,3-7
            6-6,4-6
            2-6,4-8
            EOF,
            4,
        ];
        yield 'test-1' => [<<<EOF
            1-2,3-4
            EOF,
            0,
        ];
        yield 'test-2' => [<<<EOF
            1-2,2-3
            EOF,
            1,
        ];
        yield 'test-3' => [<<<EOF
            1-3,2-3
            EOF,
            1,
        ];
        yield 'test-4' => [<<<EOF
            2-3,2-3
            EOF,
            1,
        ];
        yield 'test-5' => [<<<EOF
            2-4,2-3
            EOF,
            1,
        ];
        yield 'test-6' => [<<<EOF
            3-4,2-3
            EOF,
            1,
        ];
        yield 'test-7' => [<<<EOF
            3-5,2-3
            EOF,
            1,
        ];
        yield 'test-8' => [<<<EOF
            4-5,2-3
            EOF,
            0,
        ];
    }
}
