<?php

declare(strict_types=1);

namespace Tests\Day\Y2024;

use App\Day\Y2024\Day1;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day1>
 */
class Day1Test extends DayTestCase
{
    public const EXAMPLE = <<<'EOF'
        3   4
        4   3
        2   5
        1   3
        3   9
        3   3
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            11,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            31,
        ];
    }
}
