<?php

declare(strict_types=1);

namespace Tests\Day\Y2024;

use App\Day\Y2024\Day2;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day2>
 */
class Day2Test extends DayTestCase
{
    public const EXAMPLE = <<<'EOF'
        7 6 4 2 1
        1 2 7 8 9
        9 7 6 2 1
        1 3 2 4 5
        8 6 4 4 1
        1 3 6 7 9
        EOF;

    public static function getExamplesPart1(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            2,
        ];
    }

    public static function getExamplesPart2(): iterable
    {
        yield 'example' => [
            self::EXAMPLE,
            4,
        ];
    }
}
