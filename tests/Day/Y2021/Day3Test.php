<?php

declare(strict_types=1);

namespace Tests\Day\Y2021;

use App\Day\Y2021\Day3;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day3>
 */
class Day3Test extends DayTestCase
{
    private const EXAMPLE = <<<EOF
            00100
            11110
            10110
            10111
            10101
            01111
            00111
            11100
            10000
            11001
            00010
            01010
        EOF;

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield 'example' => [
            self::EXAMPLE,
            198,
        ];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield 'example' => [
            self::EXAMPLE,
            230,
        ];
    }
}
