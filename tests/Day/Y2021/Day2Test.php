<?php

declare(strict_types=1);

namespace Tests\Day\Y2021;

use App\Day\Y2021\Day2;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day2>
 */
class Day2Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield 'example' => [<<<EOF
            forward 5
            down 5
            forward 8
            up 3
            down 8
            forward 2
            EOF,
            150,
        ];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield 'example' => [<<<EOF
            forward 5
            down 5
            forward 8
            up 3
            down 8
            forward 2
            EOF,
            900,
        ];
    }
}
