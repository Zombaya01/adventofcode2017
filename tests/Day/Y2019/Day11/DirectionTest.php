<?php

declare(strict_types=1);

namespace Tests\Day\Y2019\Day11;

use App\Day\Y2019\Day11\Direction;
use App\Day\Y2019\Day11\Turn;
use PHPUnit\Framework\TestCase;

class DirectionTest extends TestCase
{
    public function testTurns(): void
    {
        $this->assertEquals(Direction::Left,  Direction::Up->left());
        $this->assertEquals(Direction::Right, Direction::Up->right());

        $this->assertEquals(Direction::Right, Direction::Down->left());
        $this->assertEquals(Direction::Left,  Direction::Down->right());

        $this->assertEquals(Direction::Up,   Direction::Right->left());
        $this->assertEquals(Direction::Down, Direction::Right->right());

        $this->assertEquals(Direction::Down, Direction::Left->left());
        $this->assertEquals(Direction::Up,   Direction::Left->right());

        $this->assertEquals(Direction::Left,  Direction::Up->turn(Turn::Left));
        $this->assertEquals(Direction::Right, Direction::Up->turn(Turn::Right));
    }

    public function testX(): void
    {
        $this->assertEquals(0,Direction::Up->x());
        $this->assertEquals(0,Direction::Down->x());
        $this->assertEquals(1,Direction::Right->x());
        $this->assertEquals(-1,Direction::Left->x());
    }

    public function testY(): void
    {
        $this->assertEquals(0,Direction::Right->y());
        $this->assertEquals(0,Direction::Left->y());
        $this->assertEquals(1,Direction::Up->y());
        $this->assertEquals(-1,Direction::Down->y());
    }
}
