<?php

namespace Tests\Day\Y2019\Day11\Thread;

use App\Day\Y2019\Day11\Thread\StreamArrayThread;
use App\Day\Y2019\SignalBuffer;
use PHPUnit\Framework\TestCase;

class StreamArrayThreadTest extends TestCase
{
    public function testAnEmptyInputIsDirectlyReady(): void
    {
        $output = new SignalBuffer();
        $thread = new StreamArrayThread([], $output);

        $this->assertFalse($thread->isRunnable());
        $this->assertTrue($thread->hasEnded());
        $this->assertSame(0,$thread->getExitCode());
        $this->assertEquals([],$output->toArray());
    }

    public function testRunAddsOneToBuffer(): void
    {
        $input  = [1, 2, 3];
        $output = new SignalBuffer();
        $thread = new StreamArrayThread($input, $output);

        $this->assertTrue($thread->isRunnable());
        $this->assertFalse($thread->hasEnded());
        $this->assertEquals([],$output->toArray());

        $thread->run();

        $this->assertTrue($thread->isRunnable());
        $this->assertFalse($thread->hasEnded());
        $this->assertEquals([1],$output->toArray());
        $thread->run();

        $this->assertTrue($thread->isRunnable());
        $this->assertFalse($thread->hasEnded());
        $this->assertEquals([1, 2],$output->toArray());

        $thread->run();

        $this->assertFalse($thread->isRunnable());
        $this->assertTrue($thread->hasEnded());
        $this->assertSame(0,$thread->getExitCode());
        $this->assertEquals([1, 2, 3],$output->toArray());
    }
}
