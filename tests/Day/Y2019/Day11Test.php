<?php

declare(strict_types=1);

namespace Tests\Day\Y2019;

use App\Day\Y2019\Day11;
use App\Day\Y2019\Day11\Color;
use App\Day\Y2019\SignalBuffer;
use Tests\BaseDayTestCase;

/**
 * @extends BaseDayTestCase<Day11>
 */
class Day11Test extends BaseDayTestCase
{
    #[\PHPUnit\Framework\Attributes\DataProvider('provideRunPart1')]
    public function testRunPart1(array $input, int $expected, array $expectedOutput, string $expectedHull): void
    {
        $signalBuffer = new SignalBuffer();
        $streamThead  = new Day11\Thread\StreamArrayThread($input,$signalBuffer);
        $output       = new SignalBuffer();
        $paintMachine = self::getDay()->runPart1($streamThead,$signalBuffer, $output,Color::Black);
        $this->assertEquals($expected,$paintMachine->getNumberOfPaintedPanels());
        $this->assertEquals($expectedOutput, $output->toArray());
        $this->assertEquals($expectedHull, $paintMachine->paint(-2,2));
    }

    /**
     * @return iterable{array{int},int}
     */
    public static function provideRunPart1(): iterable
    {
        yield [
            [],
            0,
            [0],
            <<<EOF
            .....
            .....
            ..^..
            .....
            .....
            EOF,
        ];
        yield [
            [1, 0],
            1,
            [0, 0],
            <<<EOF
            .....
            .....
            .<#..
            .....
            .....
            EOF,
        ];
        yield [
            [1, 0, 0, 0],
            2,
            [0, 0, 0],
            <<<EOF
            .....
            .....
            ..#..
            .v...
            .....
            EOF,
        ];
        yield [
            [1, 0, 0, 0, 1, 0, 1, 0],
            4,
            [0, 0, 0, 0, 1],
            <<<EOF
            .....
            .....
            ..^..
            .##..
            .....
            EOF,
        ];
        yield [
            [1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0],
            6,
            [0, 0, 0, 0, 1, 0, 0, 0],
            <<<EOF
            .....
            ..<#.
            ...#.
            .##..
            .....
            EOF,
        ];
    }
}
