<?php

namespace Tests\Day\Y2019;

use App\Day\Y2019\Instruction;
use App\Day\Y2019\Mode;
use App\Day\Y2019\OpCode;
use PHPUnit\Framework\TestCase;

class InstructionTest extends TestCase
{
    #[\PHPUnit\Framework\Attributes\DataProvider('dataProvider')]
    public function testInstruction(int $input, OpCode $opCode, Mode $modeA, Mode $modeB, Mode $modeC): void
    {
        $actual = new Instruction($input);
        $this->assertEquals($opCode,$actual->opCode);
        $this->assertEquals($modeA,$actual->modeA);
        $this->assertEquals($modeB,$actual->modeB);
        $this->assertEquals($modeC,$actual->modeC);
    }

    public static function dataProvider(): iterable
    {
        yield '1002' => [
            1002,
            OpCode::Multiply,
            Mode::Position,
            Mode::Immediate,
            Mode::Position,
        ];

        yield '21002' => [
            21002,
            OpCode::Multiply,
            Mode::Position,
            Mode::Immediate,
            Mode::Relative,
        ];
    }
}
