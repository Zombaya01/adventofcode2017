<?php

namespace Tests\Day\Y2019;

use App\Day\Y2019\Day8;
use Tests\BaseDayTestCase;
use Tests\TestDay1Trait;

/**
 * @extends BaseDayTestCase<Day8>
 */
class Day8Test extends BaseDayTestCase
{
    use TestDay1Trait;

    public static function getExamplesPart1(): \Generator
    {
        yield ['2 3 123456789012', 1];
        yield ['2 3 121256789012', 4];
        yield ['2 3 121200789012', 1];
    }
}
