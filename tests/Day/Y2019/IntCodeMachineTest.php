<?php

namespace Tests\Day\Y2019;

use App\Day\Y2019\Day11\Scheduler;
use App\Day\Y2019\Day11\Thread\RunnableThread;
use App\Day\Y2019\Day11\Thread\StreamArrayThread;
use App\Day\Y2019\IntCodeMachine;
use App\Day\Y2019\SignalBuffer;
use PHPUnit\Framework\TestCase;

class IntCodeMachineTest extends TestCase
{
    /**
     * @param int[] $code
     * @param int[] $input
     * @param int[] $expectedOutput
     */
    #[\PHPUnit\Framework\Attributes\DataProvider('runProvider')]
    public function testRun(array $code, array $input, array $expectedOutput, int $expectedResult): void
    {
        $machine = new IntCodeMachine($code,new SignalBuffer($input), new SignalBuffer());

        $actualResult = $machine->run();
        $this->assertEquals($expectedResult,$actualResult);
        $this->assertEquals($expectedOutput,$machine->getOutput()->toArray());
    }

    /**
     * @param int[] $code
     * @param int[] $input
     * @param int[] $expectedOutput
     */
    #[\PHPUnit\Framework\Attributes\DataProvider('runProvider')]
    public function testRunUsingThreadRunner(array $code, array $input, array $expectedOutput, int $expectedResult): void
    {
        $machine      = new IntCodeMachine($code,new SignalBuffer($input), new SignalBuffer());
        $thread       = new RunnableThread($machine);
        $scheduler    = new Scheduler($thread);
        $scheduler->runUntillAllHaveEnded();

        $actualResult = $thread->getExitCode();

        $this->assertEquals($expectedResult,$actualResult);
        $this->assertEquals($expectedOutput,$machine->getOutput()->toArray());
    }

    /**
     * @param int[] $code
     * @param int[] $input
     * @param int[] $expectedOutput
     */
    #[\PHPUnit\Framework\Attributes\DataProvider('runProvider')]
    public function testRunUsingThreadRunnerWithStreamingInputThread(array $code, array $input, array $expectedOutput, int $expectedResult): void
    {
        $machine = new IntCodeMachine($code,new SignalBuffer(), new SignalBuffer());

        $intCodeMachineThread = new RunnableThread($machine);
        $streamingInputThread = new StreamArrayThread($input,$machine->getInput());

        $scheduler = new Scheduler($intCodeMachineThread, $streamingInputThread);
        $scheduler->runUntillAllHaveEnded();

        $actualResult = $intCodeMachineThread->getExitCode();

        $this->assertEquals($expectedResult,$actualResult);
        $this->assertEquals($expectedOutput,$machine->getOutput()->toArray());
    }

    public static function runProvider(): iterable
    {
        yield 'Day2 - example 1' => [
            [1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50],
            [0],
            [],
            3500,
        ];
        yield 'Day2 - example 2' => [
            [1, 1, 1, 4, 99, 5, 6, 0, 99],
            [0],
            [],
            30,
        ];
        yield 'Day 2 - part 1 ' => [
            [1, 12, 2, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 10, 1, 19, 2, 19, 6, 23, 2, 13, 23, 27, 1, 9, 27, 31, 2, 31, 9, 35, 1, 6, 35, 39, 2, 10, 39, 43, 1, 5, 43, 47, 1, 5, 47, 51, 2, 51, 6, 55, 2, 10, 55, 59, 1, 59, 9, 63, 2, 13, 63, 67, 1, 10, 67, 71, 1, 71, 5, 75, 1, 75, 6, 79, 1, 10, 79, 83, 1, 5, 83, 87, 1, 5, 87, 91, 2, 91, 6, 95, 2, 6, 95, 99, 2, 10, 99, 103, 1, 103, 5, 107, 1, 2, 107, 111, 1, 6, 111, 0, 99, 2, 14, 0, 0],
            [0],
            [],
            3_716_293,
        ];
        yield 'Day 2 - part 2 ' => [
            [1, 64, 29, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 10, 1, 19, 2, 19, 6, 23, 2, 13, 23, 27, 1, 9, 27, 31, 2, 31, 9, 35, 1, 6, 35, 39, 2, 10, 39, 43, 1, 5, 43, 47, 1, 5, 47, 51, 2, 51, 6, 55, 2, 10, 55, 59, 1, 59, 9, 63, 2, 13, 63, 67, 1, 10, 67, 71, 1, 71, 5, 75, 1, 75, 6, 79, 1, 10, 79, 83, 1, 5, 83, 87, 1, 5, 87, 91, 2, 91, 6, 95, 2, 6, 95, 99, 2, 10, 99, 103, 1, 103, 5, 107, 1, 2, 107, 111, 1, 6, 111, 0, 99, 2, 14, 0, 0],
            [0],
            [],
            19_690_720,
        ];
        yield 'Day 5 - example 1.a ' => [
            [3, 0, 4, 0, 99],
            [0],
            [0],
            0,
        ];
        yield 'Day 5 - example 1.b' => [
            [3, 0, 4, 0, 99],
            [999],
            [999],
            999,
        ];
        yield 'Day 5 - example 1.c' => [
            [3, 0, 4, 0, 4, 0, 99],
            [999],
            [999, 999],
            999,
        ];
        yield 'Day 5 - example 2' => [
            [1002, 4, 3, 0, 99],
            [0],
            [],
            297,
        ];
        yield 'Day 5 - example 3.a' => [
            [1101, 100, -1, 0, 99],
            [0],
            [],
            99,
        ];
        yield 'Day 5 - example 3.b' => [
            [1102, 100, -1, 0, 99],
            [0],
            [],
            -100,
        ];
        yield 'Day 5 - example 4.a' => [
            [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8],
            [8],
            [1],
            3,
        ];
        yield 'Day 5 - example 4.b' => [
            [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8],
            [7],
            [0],
            3,
        ];
        yield 'Day 5 - example 4.c' => [
            [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8],
            [9],
            [0],
            3,
        ];
        yield 'Day 5 - example 5.a' => [
            [3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8],
            [8],
            [0],
            3,
        ];
        yield 'Day 5 - example 5.b' => [
            [3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8],
            [6],
            [1],
            3,
        ];
        yield 'Day 5 - example 5.c' => [
            [3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8],
            [9],
            [0],
            3,
        ];
        yield 'Day 5 - example 6.a' => [
            [3, 3, 1108, -1, 8, 3, 4, 3, 99],
            [8],
            [1],
            3,
        ];
        yield 'Day 5 - example 6.b' => [
            [3, 3, 1108, -1, 8, 3, 4, 3, 99],
            [6],
            [0],
            3,
        ];
        yield 'Day 5 - example 6.c' => [
            [3, 3, 1108, -1, 8, 3, 4, 3, 99],
            [9],
            [0],
            3,
        ];
        yield 'Day 5 - example 7.a' => [
            [3, 3, 1107, -1, 8, 3, 4, 3, 99],
            [8],
            [0],
            3,
        ];
        yield 'Day 5 - example 7.b' => [
            [3, 3, 1107, -1, 8, 3, 4, 3, 99],
            [6],
            [1],
            3,
        ];
        yield 'Day 5 - example 7.c' => [
            [3, 3, 1107, -1, 8, 3, 4, 3, 99],
            [9],
            [0],
            3,
        ];
        yield 'Day 5 - example 8.a' => [
            [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9],
            [0],
            [0],
            3,
        ];
        yield 'Day 5 - example 8.b' => [
            [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9],
            [1],
            [1],
            3,
        ];
        yield 'Day 5 - example 8.c' => [
            [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9],
            [23],
            [1],
            3,
        ];
        yield 'Day 5 - example 8.d' => [
            [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9],
            [-23],
            [1],
            3,
        ];
        yield 'Day 5 - example 9.a' => [
            [3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1],
            [0],
            [0],
            3,
        ];
        yield 'Day 5 - example 9.b' => [
            [3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1],
            [1],
            [1],
            3,
        ];
        yield 'Day 5 - example 9.c' => [
            [3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1],
            [23],
            [1],
            3,
        ];
        yield 'Day 5 - example 9.d' => [
            [3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1],
            [3],
            [1],
            3,
        ];
        $example10 = [
            3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31,
            1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104,
            999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99,
        ];
        yield 'Day 5 - example 10.a' => [
            $example10,
            [3],
            [999],
            3,
        ];
        yield 'Day 5 - example 10.b' => [
            $example10,
            [9],
            [1001],
            3,
        ];
        yield 'Day 5 - example 10.c' => [
            $example10,
            [8],
            [1000],
            3,
        ];
        yield 'Day 5 - example 10.d' => [
            $example10,
            [-3],
            [999],
            3,
        ];

        yield 'Day 5 - example 1.a - relative-mode' => [
            [3, 0, 204, 0, 99],
            [0],
            [0],
            0,
        ];

        yield 'Day 5 - example 1.b - relative-mode' => [
            [3, 0, 204, 0, 99],
            [999],
            [999],
            999,
        ];
        yield 'Day 5 - example 1.c - relative-mode' => [
            [3, 0, 204, 0, 204, 0, 99],
            [999],
            [999, 999],
            999,
        ];
        yield 'Relative store' => [
            [109, 500, 203, 0, 4, 500, 99],
            [999],
            [999],
            109,
        ];

        yield 'Day 9 - example 1' => [
            [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99],
            [],
            [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99],
            109,
        ];

        yield 'Day 9 - example 2' => [
            [1102, 34915192, 34915192, 7, 4, 7, 99, 0],
            [],
            [1219070632396864],
            1102,
        ];

        yield 'Day 9 - example 3' => [
            [104, 1125899906842624, 99],
            [],
            [1125899906842624],
            104,
        ];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('provideRelativeBaseModification')]
    public function testRelativeBaseModification(array $code, int $initialRelativeBase, int $expectedRelativeBase): void
    {
        $machine = new IntCodeMachine($code,new SignalBuffer(), new SignalBuffer());

        $relativeBase = new \ReflectionProperty(IntCodeMachine::class,'relativeBase');
        $relativeBase->setAccessible(true);
        $relativeBase->setValue($machine,$initialRelativeBase);

        try {
            $machine->run();
        } catch (\Throwable) {
            // do nothing
        }

        $this->assertEquals($expectedRelativeBase, $relativeBase->getValue($machine));
    }

    public static function provideRelativeBaseModification(): iterable
    {
        yield 'example1' => [
            [109, 19],
            2000,
            2019,
        ];
        //        yield 'example2' => [
        //            [109,19,204,-34],
        //            2000,
        //            1985,
        //        ];
    }
}
