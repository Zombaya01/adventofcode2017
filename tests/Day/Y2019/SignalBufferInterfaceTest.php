<?php

declare(strict_types=1);

namespace Tests\Day\Y2019;

use App\Day\Y2019\SignalBuffer;
use App\Day\Y2019\SignalBufferInterface;
use App\Day\Y2019\SignalBufferWithMemory;
use App\Exception\IntCodeMachine\MissingInputException;
use PHPUnit\Framework\TestCase;

class SignalBufferInterfaceTest extends TestCase
{
    public static function provideSignalBuffer(): iterable
    {
        yield [SignalBuffer::class];
        yield [SignalBufferWithMemory::class];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('provideSignalBuffer')]
    public function testSignalBufferIsEmptyOnStart(string $signalBufferClass): void
    {
        /** @var SignalBufferInterface $signalBuffer */
        $signalBuffer = new $signalBufferClass();

        $this->assertSame(0,$signalBuffer->count());
        $this->assertEquals([],$signalBuffer->toArray());
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('provideSignalBuffer')]
    public function testSignalBufferCanBeInitializedStart(string $signalBufferClass): void
    {
        /** @var SignalBufferInterface $signalBuffer */
        $signalBuffer = new $signalBufferClass([1, 2, 3]);

        $this->assertSame(3,$signalBuffer->count());
        $this->assertEquals([1, 2, 3],$signalBuffer->toArray());
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('provideSignalBuffer')]
    public function testSignalBufferIsEmptyAfterPushingAndShifting(string $signalBufferClass): void
    {
        /** @var SignalBufferInterface $signalBuffer */
        $signalBuffer = new $signalBufferClass();

        $signalBuffer->push(999);

        $this->assertSame(1,$signalBuffer->count());

        $this->assertSame(999, $signalBuffer->shift());

        $this->assertSame(0,$signalBuffer->count());
        $this->assertEquals([],$signalBuffer->toArray());
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('provideSignalBuffer')]
    public function testSignalBufferThrowsExceptionWhenInputIsMissing(string $signalBufferClass): void
    {
        /** @var SignalBufferInterface $signalBuffer */
        $signalBuffer = new $signalBufferClass();

        $this->expectException(MissingInputException::class);

        $signalBuffer->shift();
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('provideSignalBuffer')]
    public function testSignalBufferLastReturnsLastValue(string $signalBufferClass): void
    {
        /** @var SignalBufferInterface $signalBuffer */
        $signalBuffer = new $signalBufferClass([1, 2, 3]);

        $this->assertSame(3,$signalBuffer->last());

        /** @var SignalBufferInterface $emptySignalBuffer */
        $emptySignalBuffer = new $signalBufferClass();
        $this->assertNull($emptySignalBuffer->last());
    }

    public function testSignalBufferWithMemoryReturnsAllValues(): void
    {
        $signalBuffer = new SignalBufferWithMemory([1, 2, 3]);

        $signalBuffer->shift();
        $signalBuffer->shift();
        $signalBuffer->push(4);

        $this->assertEquals([1, 2, 3, 4],$signalBuffer->allValues());
    }
}
