<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Day\Y2017;

use App\Day\Y2017\Day9;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day9>
 */
class Day9Test extends DayTestCase
{
    public static function getExamplesPart1(): \Generator
    {
        yield ['{}', 1];
        yield ['{{{}}}', 6];
        yield ['{{},{}}', 5];
        yield ['{{{},{},{{}}}}', 16];
        yield ['{<a>,<a>,<a>,<a>}', 1];
        yield ['{{<ab>},{<ab>},{<ab>},{<ab>}}', 9];
        yield ['{{<!!>},{<!!>},{<!!>},{<!!>}}', 9];
        yield ['{{<a!>},{<a!>},{<a!>},{<ab>}}', 3];
    }

    public static function getExamplesPart2(): \Generator
    {
        yield ['<>', 0];
        yield ['<random characters>', 17];
        yield ['<<<<>', 3];
        yield ['<{!>}>', 2];
        yield ['<!!>', 0];
        yield ['<!!!>>', 0];
        yield ['<{o"i!a,<{i<a>', 10];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('getExamplesPartOfString')]
    public function testRemovePartOfString(string $input,int $offset,int $length, string $output): void
    {
        $this->assertSame($output, self::getDay()->removePartOfString($input,$offset,$length));
    }

    public static function getExamplesPartOfString(): \Generator
    {
        yield ['blabla', 0, 1, 'labla'];
        yield ['blabla', 0, 2, 'abla'];
        yield ['blabla', 1, 1, 'babla'];
        yield ['blabla', 4, 2, 'blab'];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('getExamplesRemoveGarbage')]
    public function testRemoveGarbage(string $input,string $output): void
    {
        $this->assertSame($output, self::getDay()->removeGarbage($input));
    }

    public static function getExamplesRemoveGarbage(): \Generator
    {
        yield ['!a', ''];
        yield ['<>', ''];
        yield ['<random characters>', ''];
        yield ['<<<<>', ''];
        yield ['<{!>}>', ''];
        yield ['<!!>', ''];
        yield ['<!!!>>', ''];
        yield ['<{o"i!a,<{i<a>', ''];
        yield ['{{<!!>},{<!!>},{<!!>},{<!!>}}', '{{},{},{},{}}'];
    }
}
