<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Day\Y2017;

use App\Day\Y2017\Day1;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day1>
 */
class Day1Test extends DayTestCase
{
    public static function getExamplesPart1(): \Generator
    {
        yield ['1122', 3];
        yield ['1111', 4];
        yield ['1234', 0];
        yield ['91212129', 9];
    }

    public static function getExamplesPart2(): \Generator
    {
        yield ['1212', 6];
        yield ['1221', 0];
        yield ['123425', 4];
        yield ['123123', 12];
        yield ['12131415', 4];
    }
}
