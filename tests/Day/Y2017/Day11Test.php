<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Day\Y2017;

use App\Day\Y2017\Day11;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day11>
 */
class Day11Test extends DayTestCase
{
    public static function getExamplesPart1(): \Generator
    {
        yield ['ne,ne,ne', 3];
        yield ['ne,ne,sw,sw', 0];
        yield ['ne,ne,s,s', 2];
        yield ['se,sw,se,sw,sw', 3];
    }

    /**
     * @return \Generator<mixed[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield [self::getDay()->getInput(), 1471];
    }
}
