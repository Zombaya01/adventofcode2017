<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Day\Y2017;

use App\Day\Y2017\Day13;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day13>
 */
class Day13Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield ['"0: 3
                1: 2
                4: 4
                6: 4"', 24];
        // yield[ 'ne,ne,sw,sw', 0];
        // yield[ 'ne,ne,s,s', 2];
        // yield[ 'se,sw,se,sw,sw', 3];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield ['"0: 3
                1: 2
                4: 4
                6: 4"', 10];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('getExamplesIsCaught')]
    public function testIsCaught(int $input, bool $output): void
    {
        if (!method_exists(self::getDay(),'part1')) {
            return;
        }
        if (!method_exists(self::getDay(),'getParsedTestInput')) {
            return;
        }
        $this->assertSame($output, self::getDay()->getParsedTestInput()->isCaught($input));
    }

    public static function getExamplesIsCaught(): \Generator
    {
        yield [0, true];
        yield [1, false];
        yield [2, false];
        yield [3, false];
        yield [4, false];
        yield [5, false];
        yield [6, true];
    }
}
