<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Day\Y2017;

use App\Day\Y2017\Day2;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day2>
 */
class Day2Test extends DayTestCase
{
    public static function getExamplesPart1(): \Generator
    {
        yield ['5  1 9 5
                7  5 3
                2  4 6     8', 18];
        yield ['1 2
                1 3', 3];
        yield ['1 2
                1 2', 2];
        yield ['5 1 9 5', 8];
        yield ['2 4 6 8', 6];
        yield ['7 5 3', 4];
    }

    public static function getExamplesPart2(): \Generator
    {
        yield ['5 9 2 8
                9 4 7 3
                3 8 6 5', 9];
        yield ['5 9 2 8', 4];
        yield ['9 4 7 3', 3];
        yield ['3 8 6 5', 2];
    }
}
