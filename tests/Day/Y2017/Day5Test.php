<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Day\Y2017;

use App\Day\Y2017\Day5;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day5>
 */
class Day5Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): iterable
    {
        yield [' 0
                3
                0
                1
               -3', 5];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): iterable
    {
        yield [' 0
                3
                0
                1
               -3', 10];
    }
}
