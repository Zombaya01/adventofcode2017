<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Day\Y2017;

use App\Day\Y2017\Day10;
use Tests\BaseDayTestCase;
use Tests\TestDay1Trait;

/**
 * @extends BaseDayTestCase<Day10>
 */
class Day10Test extends BaseDayTestCase
{
    use TestDay1Trait;

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield [
            '5 3,4,1,5',
            12,
        ];
    }

    public function testExample(): void
    {
        $day = self::getDay();
        $day->initialize(5);
        $this->assertSame($day->getList(),[0, 1, 2, 3, 4]);
        $day->rotate(3);
        $this->assertSame($day->getList(),[2, 1, 0, 3, 4]);
        $this->assertSame($day->getIndex(),3);
        $day->rotate(4);
        $this->assertSame($day->getList(),[4, 3, 0, 1, 2]);
        $this->assertSame($day->getIndex(),3);
        $day->rotate(1);
        $this->assertSame($day->getList(),[4, 3, 0, 1, 2]);
        $this->assertSame($day->getIndex(),1);
        $day->rotate(5);
        $this->assertSame($day->getList(),[3, 4, 2, 1, 0]);
        $this->assertSame($day->getIndex(),4);
    }
}
