<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Day\Y2017;

use App\Day\Y2017\Day7;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day7>
 */
class Day7Test extends DayTestCase
{
    /**
     * @return \Generator<string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield ['pbga (66)
                xhth (57)
                ebii (61)
                havc (66)
                ktlj (57)
                fwft (72) -> ktlj, cntj, xhth
                qoyq (66)
                padx (45) -> pbga, havc, qoyq
                tknk (41) -> ugml, padx, fwft
                jptl (61)
                ugml (68) -> gyxo, ebii, jptl
                gyxo (61)
                cntj (57)', 'tknk'];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield ['pbga (66)
                xhth (57)
                ebii (61)
                havc (66)
                ktlj (57)
                fwft (72) -> ktlj, cntj, xhth
                qoyq (66)
                padx (45) -> pbga, havc, qoyq
                tknk (41) -> ugml, padx, fwft
                jptl (61)
                ugml (68) -> gyxo, ebii, jptl
                gyxo (61)
                cntj (57)', 60];
    }
}
