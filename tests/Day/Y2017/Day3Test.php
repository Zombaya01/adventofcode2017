<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Day\Y2017;

use App\Day\Y2017\Day3;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day3>
 */
class Day3Test extends DayTestCase
{
    /**
     * @param int[] $output
     */
    #[\PHPUnit\Framework\Attributes\DataProvider('getExamplesCalculatePosition')]
    public function testCalculatePosition(int $input, array $output): void
    {
        $instance = self::getDay();

        $ref    = new \ReflectionClass(Day3::class);
        $method = $ref->getMethod('calculatePosition');
        $method->setAccessible(true);

        $this->assertSame($output, $method->invoke($instance, $input));
    }

    public static function getExamplesCalculatePosition(): \Generator
    {
        yield [1, [0,  0]];
        yield [9, [1,  1]];
        yield [12, [2, -1]];
        yield [15, [0, -2]];
        yield [16, [-1, -2]];
        yield [5, [-1, -1]];
        yield [6, [-1,  0]];
        yield [7, [-1,  1]];
        yield [21, [-2,  2]];
        yield [23, [0,  2]];
        yield [24, [1,  2]];
        yield [25, [2,  2]];
        yield [49, [3,  3]];
    }

    public static function getExamplesPart1(): \Generator
    {
        yield ['1', 0];
        yield ['12', 3];
        yield ['23', 2];
        yield ['1024', 31];
    }

    public static function getExamplesPart2(): \Generator
    {
        yield ['1', 2];
        yield ['2', 4];
        yield ['3', 4];
        yield ['4', 5];
        yield ['5', 10];
        yield ['17', 23];
    }
}
