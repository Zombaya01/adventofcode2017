<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Day\Y2017;

use App\Day\Y2017\Day4;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day4>
 */
class Day4Test extends DayTestCase
{
    public static function getExamplesPart1(): \Generator
    {
        yield ['aa bb cc dd ee', 1];
        yield ['aa bb cc dd aa', 0];
        yield ['aa bb cc dd aaa', 1];
        yield ['aa bb cc dd ee
                aa bb cc dd ff
                aa bb cc dd gg', 3];
    }

    public static function getExamplesPart2(): \Generator
    {
        yield ['aa bb cc dd ee', 1];
        yield ['aa bb cc dd aa', 0];
        yield ['aa bb cc dd aaa', 1];
        yield ['abcde fghij', 1];
        yield ['abcde xyz ecdab', 0];
        yield ['a ab abc abd abf abj', 1];
        yield ['iiii oiii ooii oooi oooo', 1];
        yield ['oiii ioii iioi iiio', 0];
    }
}
