<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day13;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day13>
 */
class Day13Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield 'example' => [<<<EOF
            939
            7,13,x,x,59,x,31,19
            EOF,
            295,
        ];
    }

    public static function getExamplesPart2(): \Generator
    {
        yield 'example1' => [<<<EOF
            939
            7,13,x,x,59,x,31,19
            EOF,
            1_068_781,
        ];
        yield 'example2' => [<<<EOF
            939
            17,x,13,19
            EOF,
            3417,
        ];
        yield 'example3' => [<<<EOF
            939
            67,7,59,61
            EOF,
            754018,
        ];
        yield 'example4' => [<<<EOF
            939
            67,x,7,59,61
            EOF,
            779210,
        ];
        yield 'example5' => [<<<EOF
            939
            67,7,x,59,61
            EOF,
            1_261_476,
        ];
        yield 'example6' => [<<<EOF
            939
            1789,37,47,1889
            EOF,
            1_202_161_486,
        ];
    }

    public function testParseInput(): void
    {
        $input = <<<EOF
            939
            7,13,x,x,59,x,31,19
            EOF;

        $parsed   = Day13::parseInput($input);
        $expected = [939, [7, 13, 59, 31, 19]];

        self::assertEquals($expected,$parsed);
    }

    public function testParseInput2(): void
    {
        $input = <<<EOF
            939
            7,13,x,x,59,x,31,19
            EOF;

        $parsed   = Day13::parseInput2($input);
        $expected = [939, [[4, 59], [6, 31], [7, 19], [1, 13], [0, 7]]];

        self::assertEquals($expected,$parsed);
    }
}
