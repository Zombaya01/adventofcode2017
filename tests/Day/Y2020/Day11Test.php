<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day11;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day11>
 */
class Day11Test extends DayTestCase
{
    public static function getExamplesPart1(): \Generator
    {
        yield 'example' => [<<<EOF
            L.LL.LL.LL
            LLLLLLL.LL
            L.L.L..L..
            LLLL.LL.LL
            L.LL.LL.LL
            L.LLLLL.LL
            ..L.L.....
            LLLLLLLLLL
            L.LLLLLL.L
            L.LLLLL.LL
            EOF,
            37,
        ];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield 'example' => [<<<EOF
            L.LL.LL.LL
            LLLLLLL.LL
            L.L.L..L..
            LLLL.LL.LL
            L.LL.LL.LL
            L.LLLLL.LL
            ..L.L.....
            LLLLLLLLLL
            L.LLLLLL.L
            L.LLLLL.LL
            EOF,
            26,
        ];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('getNextFrameProvider')]
    public function testGetNextFrame(string $input,string $expected): void
    {
        $day11  = new Day11();
        $actual = $day11->toString($day11->calculateNextFrame($day11->parseInput($input)));
        self::assertEquals($expected,$actual);
    }

    public static function getNextFrameProvider(): \Generator
    {
        $frames = [
            <<<EOF
            L.LL.LL.LL
            LLLLLLL.LL
            L.L.L..L..
            LLLL.LL.LL
            L.LL.LL.LL
            L.LLLLL.LL
            ..L.L.....
            LLLLLLLLLL
            L.LLLLLL.L
            L.LLLLL.LL
            EOF,
            <<<EOF
            #.##.##.##
            #######.##
            #.#.#..#..
            ####.##.##
            #.##.##.##
            #.#####.##
            ..#.#.....
            ##########
            #.######.#
            #.#####.##
            EOF,
            <<<EOF
            #.LL.L#.##
            #LLLLLL.L#
            L.L.L..L..
            #LLL.LL.L#
            #.LL.LL.LL
            #.LLLL#.##
            ..L.L.....
            #LLLLLLLL#
            #.LLLLLL.L
            #.#LLLL.##
            EOF,
            <<<EOF
            #.##.L#.##
            #L###LL.L#
            L.#.#..#..
            #L##.##.L#
            #.##.LL.LL
            #.###L#.##
            ..#.#.....
            #L######L#
            #.LL###L.L
            #.#L###.##
            EOF,
            <<<EOF
            #.#L.L#.##
            #LLL#LL.L#
            L.L.L..#..
            #LLL.##.L#
            #.LL.LL.LL
            #.LL#L#.##
            ..L.L.....
            #L#LLLL#L#
            #.LLLLLL.L
            #.#L#L#.##
            EOF,
            <<<EOF
            #.#L.L#.##
            #LLL#LL.L#
            L.#.L..#..
            #L##.##.L#
            #.#L.LL.LL
            #.#L#L#.##
            ..L.L.....
            #L#L##L#L#
            #.LLLLLL.L
            #.#L#L#.##
            EOF,
        ];
        for ($i=1, $iMax = count($frames); $i< $iMax; ++$i) {
            yield [$frames[$i-1], $frames[$i]];
        }
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('getPartTwoNextFrameProvider')]
    public function testPartTwoGetNextFrame(string $input,string $expected): void
    {
        $day11  = new Day11();
        $actual = $day11->toString($day11->calculatePartTwoNextFrame($day11->parseInput($input)));
        self::assertEquals($expected,$actual);
    }

    public static function getPartTwoNextFrameProvider(): \Generator
    {
        $frames = [
            <<<EOF
            L.LL.LL.LL
            LLLLLLL.LL
            L.L.L..L..
            LLLL.LL.LL
            L.LL.LL.LL
            L.LLLLL.LL
            ..L.L.....
            LLLLLLLLLL
            L.LLLLLL.L
            L.LLLLL.LL
            EOF,
            <<<EOF
            #.##.##.##
            #######.##
            #.#.#..#..
            ####.##.##
            #.##.##.##
            #.#####.##
            ..#.#.....
            ##########
            #.######.#
            #.#####.##
            EOF,
            <<<EOF
            #.LL.LL.L#
            #LLLLLL.LL
            L.L.L..L..
            LLLL.LL.LL
            L.LL.LL.LL
            L.LLLLL.LL
            ..L.L.....
            LLLLLLLLL#
            #.LLLLLL.L
            #.LLLLL.L#
            EOF,
            <<<EOF
            #.L#.##.L#
            #L#####.LL
            L.#.#..#..
            ##L#.##.##
            #.##.#L.##
            #.#####.#L
            ..#.#.....
            LLL####LL#
            #.L#####.L
            #.L####.L#
            EOF,
            <<<EOF
            #.L#.L#.L#
            #LLLLLL.LL
            L.L.L..#..
            ##LL.LL.L#
            L.LL.LL.L#
            #.LLLLL.LL
            ..L.L.....
            LLLLLLLLL#
            #.LLLLL#.L
            #.L#LL#.L#
            EOF,
            <<<EOF
            #.L#.L#.L#
            #LLLLLL.LL
            L.L.L..#..
            ##L#.#L.L#
            L.L#.#L.L#
            #.L####.LL
            ..#.#.....
            LLL###LLL#
            #.LLLLL#.L
            #.L#LL#.L#
            EOF,
            <<<EOF
            #.L#.L#.L#
            #LLLLLL.LL
            L.L.L..#..
            ##L#.#L.L#
            L.L#.LL.L#
            #.LLLL#.LL
            ..#.L.....
            LLL###LLL#
            #.LLLLL#.L
            #.L#LL#.L#
            EOF,
        ];
        for ($i=1, $iMax = count($frames); $i< $iMax; ++$i) {
            yield [$frames[$i-1], $frames[$i]];
        }
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('getExamplesPart1')]
    public function testConversion(string $input,int $dummy): void
    {
        $day11     = new Day11();
        $converted = $day11->parseInput($input);
        $actual    = $day11->toString($converted);

        self::assertEquals($input,$actual);
    }
}
