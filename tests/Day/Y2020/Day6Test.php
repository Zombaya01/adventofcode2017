<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day6;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day6>
 */
class Day6Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield [<<<EOF
            abc

            a
            b
            c
            
            ab
            ac
            
            a
            a
            a
            a
            
            b
            EOF,
            11,
        ];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield [<<<EOF
            abc

            a
            b
            c
            
            ab
            ac
            
            a
            a
            a
            a
            
            b
            EOF,
            6,
        ];
    }
}
