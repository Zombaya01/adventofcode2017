<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day12;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day12>
 */
class Day12Test extends DayTestCase
{
    public static function getExamplesPart1(): \Generator
    {
        yield 'example' => [<<<EOF
            F10
            N3
            F7
            R90
            F11
            EOF,
            25,
        ];
        yield [<<<EOF
            L90
            F1
            EOF,
            1,
        ];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield 'example' => [<<<EOF
            F10
            N3
            F7
            R90
            F11
            EOF,
            286,
        ];
    }

    /**
     * @param int[] $expected
     */
    #[\PHPUnit\Framework\Attributes\DataProvider('turnProvider')]
    public function testTurn(int $x,int $y,int $degree,array $expected): void
    {
        $actual = Day12::turn($x,$y,$degree);
        self::assertEquals($expected,$actual);
    }

    public static function turnProvider(): \Generator
    {
        yield [1, 2, 0, [1, 2]];
        yield [10, 4, 90, [4, -10]];
        yield [10, 4, 180, [-10, -4]];
        yield [10, 4, 270, [-4, 10]];
    }
}
