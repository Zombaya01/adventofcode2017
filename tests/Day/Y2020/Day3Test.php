<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day3;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day3>
 */
class Day3Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield [
            <<<EOF
            ..##.......
            #...#...#..
            .#....#..#.
            ..#.#...#.#
            .#...##..#.
            ..#.##.....
            .#.#.#....#
            .#........#
            #.##...#...
            #...##....#
            .#..#...#.#
        EOF
            ,
            7,
        ];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield [
            <<<EOF
            ..##.......
            #...#...#..
            .#....#..#.
            ..#.#...#.#
            .#...##..#.
            ..#.##.....
            .#.#.#....#
            .#........#
            #.##...#...
            #...##....#
            .#..#...#.#
        EOF
            ,
            336,
        ];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('calculateTreesProvider')]
    public function testCalculateTrees(string $input,int $down,int $right,int $expected): void
    {
        self::assertEquals($expected,(new Day3())->calculateTrees($input,$down,$right));
    }

    public static function calculateTreesProvider(): \Generator
    {
        $input = <<<EOF
            ..##.......
            #...#...#..
            .#....#..#.
            ..#.#...#.#
            .#...##..#.
            ..#.##.....
            .#.#.#....#
            .#........#
            #.##...#...
            #...##....#
            .#..#...#.#
        EOF;

        yield [$input, 1, 1, 2];
        yield [$input, 1, 3, 7];
        yield [$input, 1, 5, 3];
        yield [$input, 1, 7, 4];
        yield [$input, 2, 1, 2];
    }
}
