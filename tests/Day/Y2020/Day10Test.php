<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day10;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day10>
 */
class Day10Test extends DayTestCase
{
    private const example1 = <<<EOF
            16
            10
            15
            5
            1
            11
            7
            19
            6
            12
            4
            EOF;
    private const example2 = <<<EOF
            28
            33
            18
            42
            31
            14
            46
            20
            48
            47
            24
            23
            49
            45
            19
            38
            39
            11
            1
            32
            25
            35
            8
            17
            7
            9
            4
            2
            34
            10
            3
            EOF;

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield 'example' => [
            self::example1,
            35,
        ];
        yield 'example2' => [
            self::example2,
            220,
        ];
    }

    public static function getExamplesPart2(): \Generator
    {
        yield [implode("\n",[3, 6, 9, 12]), 1];
        yield [implode("\n",[3, 5, 6, 9, 12]), 2];
        yield [implode("\n",[3, 4, 6, 9, 12]), 2];
        yield [implode("\n",[3, 4, 5, 6, 9, 12]), 4];
        yield [implode("\n",[2, 5, 6, 9, 12]), 1];
        yield [implode("\n",[2, 4, 6, 9, 12]), 1];
        yield [implode("\n",[3, 6, 9, 10, 12]), 2];
        yield [implode("\n",[3, 5, 6, 9, 10, 12]), 4];

        // 0,3,5,6,9,10,12
        //  1 1 1 2 2 2 4
        // 0,3,6,9,10,12
        // 0,3,5,6,9,12
        // 0,3,6,9,12

        yield 'example' => [
            self::example1,
            8,
        ];
        yield 'example2' => [
            self::example2,
            19208,
        ];
    }
}
