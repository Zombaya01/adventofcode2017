<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day16;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day16>
 */
class Day16Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield 'example' => [<<<EOF
            class: 1-3 or 5-7
            row: 6-11 or 33-44
            seat: 13-40 or 45-50
            
            your ticket:
            7,1,14
            
            nearby tickets:
            7,3,47
            40,4,50
            55,2,20
            38,6,12
            EOF,
            71,
        ];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield 'example' => [<<<EOF
            departure: 0-1 or 4-19
            row: 0-5 or 8-19
            seat: 0-13 or 16-19
            
            your ticket:
            11,12,13
            
            nearby tickets:
            3,9,18
            15,1,5
            5,14,9
            EOF,
            12,
        ];
    }

    public function testParseInput(): void
    {
        $input = <<<EOF
            class: 1-3 or 5-7
            row: 6-11 or 33-44
            seat: 13-40 or 45-50
            
            your ticket:
            7,1,14
            
            nearby tickets:
            7,3,47
            40,4,50
            55,2,20
            38,6,12
            EOF;
        $expected = [
            [
                'class' => [1, 3, 5, 7],
                'row'   => [6, 11, 33, 44],
                'seat'  => [13, 40, 45, 50],
            ],
            [7, 1, 14],
            [
                [7, 3, 47],
                [40, 4, 50],
                [55, 2, 20],
                [38, 6, 12],
            ],
        ];
        self::assertEquals($expected,Day16::parseInput($input));
    }

    public function testFilterInvalid(): void
    {
        $rules =  [
            'class' => [1, 3, 5, 7],
            'row'   => [6, 11, 33, 44],
            'seat'  => [13, 40, 45, 50],
        ];
        $tickets = [
            [7, 3, 47],
            [40, 4, 50],
            [55, 2, 20],
            [38, 6, 12],
        ];
        $expected = [
            [7, 3, 47],
        ];
        self::assertEquals($expected,Day16::filterInvalid($rules,$tickets));
    }
}
