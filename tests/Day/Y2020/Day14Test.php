<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day14;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day14>
 */
class Day14Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield 'example' => [<<<EOF
            mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
            mem[8] = 11
            mem[7] = 101
            mem[8] = 0
            EOF,
            165,
        ];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield 'example' => [<<<EOF
            mask = 000000000000000000000000000000X1001X
            mem[42] = 100
            mask = 00000000000000000000000000000000X0XX
            mem[26] = 1
            EOF,
            208,
        ];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('applyMaskProvider')]
    public function testApplyMask(int $input,string $mask,int $expected): void
    {
        $actual = Day14::applyMask($input,$mask);
        self::assertEquals($expected,$actual);
    }

    public static function applyMaskProvider(): \Generator
    {
        yield [11, 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X', 73];
        yield [101, 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X', 101];
        yield [0, 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X', 64];
    }

    /**
     * @param int[] $expected
     */
    #[\PHPUnit\Framework\Attributes\DataProvider('getRegisterAddressesProvider')]
    public function testGetRegisterAddresses(int $input,string $mask,array $expected): void
    {
        $actual = Day14::getRegisterAddresses($input,$mask);
        self::assertEquals($expected,$actual);
    }

    public static function getRegisterAddressesProvider(): \Generator
    {
        yield [42, '000000000000000000000000000000X1001X', [26, 27, 58, 59]];
        yield [26, '00000000000000000000000000000000X0XX', [16, 17, 18, 19, 24, 25, 26, 27]];
    }
}
