<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day2;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day2>
 */
class Day2Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield [
            <<<EOF
            1-3 a: abcde
            1-3 b: cdefg
            2-9 c: ccccccccc
        EOF
            ,
            2,
        ];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield [
            <<<EOF
            1-3 a: abcde
            1-3 b: cdefg
            2-9 c: ccccccccc
        EOF
            ,
            1,
        ];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('passwordValidProvider')]
    public function testPasswordValid(string $input, bool $expected): void
    {
        self::assertEquals($expected,Day2::validPassword($input));
    }

    public static function passwordValidProvider(): array
    {
        return [
            ['1-3 a: abcde', true],
            ['1-3 b: cdefg', false],
            ['2-9 c: ccccccccc', true],
        ];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('secondPasswordValidProvider')]
    public function testSecondPasswordValid(string $input, bool $expected): void
    {
        self::assertEquals($expected,Day2::secondValidPassword($input));
    }

    public static function secondPasswordValidProvider(): array
    {
        return [
            ['1-3 a: abcde', true],
            ['1-3 b: cdefg', false],
            ['2-9 c: ccccccccc', false],
        ];
    }
}
