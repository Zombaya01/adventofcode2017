<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day8;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day8>
 */
class Day8Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield 'example' => [<<<EOF
            nop +0
            acc +1
            jmp +4
            acc +3
            jmp -3
            acc -99
            acc +1
            jmp -4
            acc +6
            EOF,
            5,
        ];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield 'example' => [<<<EOF
            nop +0
            acc +1
            jmp +4
            acc +3
            jmp -3
            acc -99
            acc +1
            jmp -4
            acc +6
            EOF,
            8,
        ];
    }
}
