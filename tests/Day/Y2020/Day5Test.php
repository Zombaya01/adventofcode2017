<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day5;
use Tests\BaseDayTestCase;
use Tests\TestDay1Trait;

/**
 * @extends BaseDayTestCase<Day5>
 */
class Day5Test extends BaseDayTestCase
{
    use TestDay1Trait;

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield [<<<EOF
            BFFFBBFRRR
            FFFBBBFRRR
            BBFFBBFRLL
            EOF,
            820,
        ];
    }

    #[\PHPUnit\Framework\Attributes\DataProvider('convertToIdProvider')]
    public function testConvertToId(string $input, int $expected): void
    {
        self::assertEquals($expected, Day5::convertToId($input));
    }

    public static function convertToIdProvider(): \Generator
    {
        yield ['BFFFBBFRRR', 567];
        yield ['FFFBBBFRRR', 119];
        yield ['BBFFBBFRLL', 820];
    }
}
