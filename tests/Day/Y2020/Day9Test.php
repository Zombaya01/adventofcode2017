<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day9;
use App\Day\Y2020\InvalidEntryException;
use App\Utils\DataConverter;
use Tests\BaseDayTestCase;
use Tests\TestDay1Trait;

/**
 * @extends BaseDayTestCase<Day9>
 */
class Day9Test extends BaseDayTestCase
{
    use TestDay1Trait;

    /**
     * @return \Generator<mixed[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        foreach (self::firstInvalidProvider() as [$inputArray, $windowSize, $expected]) {
            if ($windowSize === 25) {
                yield [implode("\n",$inputArray), $expected];
            }
        }
    }

    /**
     * @param int[] $inputArray
     */
    #[\PHPUnit\Framework\Attributes\DataProvider('firstInvalidProvider')]
    public function testFirstInvalid(array $inputArray,int $windowSize,int $expected): void
    {
        try {
            $isValid = Day9::validateArray($inputArray,$windowSize);
            self::fail('Should not come here');
        } catch (InvalidEntryException $e) {
            $this->assertEquals($expected, $e->getValue());
        }
    }

    public static function firstInvalidProvider(): \Generator
    {
        $firstExample = [20, ...range(1,19), ...range(21,25)];

        yield [
            [...$firstExample, 26, 999], 25, 999,
        ];
        yield [
            [...$firstExample, 45, 65, 999], 25, 65,
        ];
        yield [
            [...$firstExample, 45, 64, 999], 25, 999,
        ];
        yield [
            [...$firstExample, 45, 66, 999], 25, 999,
        ];
        $example = DataConverter::columnOfInt(<<<EOF
            35
            20
            15
            25
            47
            40
            62
            55
            65
            95
            102
            117
            150
            182
            127
            219
            299
            277
            309
            576
            EOF
        );
        yield [$example, 5, 127];
    }

    public function testFindValidRange(): void
    {
        $example = DataConverter::columnOfInt(<<<EOF
            35
            20
            15
            25
            47
            40
            62
            55
            65
            95
            102
            117
            150
            182
            127
            219
            299
            277
            309
            576
            EOF
        );
        try {
            $isValid = Day9::validateArray($example,5);
            self::fail('Should not come here');
        } catch (InvalidEntryException $e) {
            $this->assertEquals(15, $e->getFirstValue());
            $this->assertEquals(47, $e->getLastValue());
        }
    }
}
