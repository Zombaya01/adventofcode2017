<?php

declare(strict_types=1);

namespace Tests\Day\Y2020;

use App\Day\Y2020\Day1;
use Tests\DayTestCase;

/**
 * @extends DayTestCase<Day1>
 */
class Day1Test extends DayTestCase
{
    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart1(): \Generator
    {
        yield [
            <<<EOF
            1721
            299
            979
            366
            
            675
            1456
        EOF
            ,
            514579,
        ];
    }

    /**
     * @return \Generator<int[]|string[]>
     */
    public static function getExamplesPart2(): \Generator
    {
        yield [
            <<<EOF
            1721
            979
            366
            299
            675
            1456
        EOF
            ,
            241_861_950,
        ];
    }
}
