<?php

namespace Tests\Utils;

use App\Utils\DataConverter;
use PHPUnit\Framework\TestCase;

class DataConverterTest extends TestCase
{
    /**
     * @param array<int, mixed[]> $expected */
    #[\PHPUnit\Framework\Attributes\DataProvider('provideColumnOfIntsSplitByBlankLine')]
    public function testColumnOfIntsSplitByBlankLine(string $input, array $expected): void
    {
        $this->assertEquals($expected,DataConverter::columnOfGroupedIntsSplitByBlankLine($input));
    }

    public static function provideColumnOfIntsSplitByBlankLine(): iterable
    {
        yield [
            <<<EOF
            1
            2
            EOF,
            [[1, 2]],
        ];
        yield [
            <<<EOF
            1
            
            2
            EOF,
            [[1], [2]],
        ];
        yield [
            <<<EOF
            1
            
            
            
            2
            EOF,
            [[1], [], [2]],
        ];
    }
}
