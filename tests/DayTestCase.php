<?php

namespace Tests;

use App\Day\AbstractDay;

/**
 * @template Day of AbstractDay
 *
 * @extends BaseDayTestCase<Day>
 */
abstract class DayTestCase extends BaseDayTestCase
{
    use TestDay1Trait;
    use TestDay2Trait;
}
