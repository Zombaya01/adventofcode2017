<?php

declare(strict_types=1);

namespace Tests;

trait TestDay2Trait
{
    /**
     * @return iterable<array{string,int|string|null}>
     */
    abstract public static function getExamplesPart2(): iterable;

    #[\PHPUnit\Framework\Attributes\DataProvider('getExamplesPart2')]
    public function testPart2(string $input, int|string|null $output): void
    {
        if (method_exists(self::getDay(),'part2')) {
            $this->assertSame($output, self::getDay()->part2($input));
        }
    }
}
