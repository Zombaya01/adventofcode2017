<?php

namespace Tests;

use App\Day\AbstractDay;
use PHPUnit\Framework\TestCase;

/**
 * @template Day of AbstractDay
 */
abstract class BaseDayTestCase extends TestCase
{
    /**
     * @return Day
     */
    protected static function getDay(): AbstractDay
    {
        $classname = preg_replace(['/Test$/', '/^Tests/'],['', 'App'],static::class);

        if (!is_subclass_of($classname,AbstractDay::class)) {
            throw new \RuntimeException(sprintf('Invalid classname generated: %s',$classname));
        }

        /** @var Day $class */
        $class = new $classname();

        return $class;
    }

    protected static function getPuzzleInput(): string
    {
        return self::getDay()->getInput();
    }
}
