<?php

declare(strict_types=1);

namespace Tests;

trait TestDay1Trait
{
    /**
     * @return iterable<array{string,int|string|null}>
     */
    abstract public static function getExamplesPart1(): iterable;

    #[\PHPUnit\Framework\Attributes\DataProvider('getExamplesPart1')]
    public function testPart1(string $input, int|string|null $output): void
    {
        if (method_exists(self::getDay(),'part1')) {
            $this->assertSame($output, self::getDay()->part1($input));
        }
    }
}
