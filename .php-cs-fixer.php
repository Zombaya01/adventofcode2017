<?php

declare(strict_types=1);

/** @var PhpCsFixer\Config $config */
$config = require(__DIR__.'/.php-cs-fixer.dist.php');

$config
    ->setParallelConfig(PhpCsFixer\Runner\Parallel\ParallelConfigFactory::detect(filesPerProcess: 10, processTimeout: 300))
;

return $config;