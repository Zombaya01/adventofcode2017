<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__."/src")
    ->in(__DIR__."/tests")
;

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules([
        '@PER-CS' => true,
        '@Symfony' => true,
        'no_trailing_whitespace' => true,
        'array_syntax' => ['syntax' => 'short'],
		'psr_autoloading'  => true,
        'no_leading_import_slash' => true,
        'no_unused_imports' => true,
        // 'no_multiline_whitespace_before_semicolons' => true,
        'phpdoc_indent' => true,
        'method_argument_space' => false,
        // 'method_separation' => true, // methodes moeten met 1 lijn van elkaar gezet zijn
        'yoda_style' => false,
        'binary_operator_spaces' => [
            'default' => 'align',
        ],
    ])
	->setFinder($finder)
    ->setCacheFile(__DIR__.'/var/.php_cs_cache')
;
