<?php

declare(strict_types=1);

namespace App\ArgumentResolver;

use App\Model\Day;
use App\Model\Event;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class EventValueResolver implements ValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        if (Event::class !== $argument->getType()) {
            return false;
        }

        $name = $argument->getName();

        if ($request->attributes->has($name)) {
            $year = $request->attributes->getInt($name);

            // Check for valid range
            if ($year < 2017 || $year > (int) date('Y')) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return iterable<Event>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $year = $request->attributes->getInt($argument->getName()) ?: (int) date('Y');

        yield new Event($year,array_map(fn (int $day): \App\Model\Day => new Day($day,$year), range(1,25)));
    }
}
