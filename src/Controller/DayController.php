<?php

namespace App\Controller;

use App\Model\Event;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DayController extends AbstractController
{
    #[Route('/{event<\d{4}>}', name: 'app_day_year')]
    #[Route('/', name: 'homepage')]
    public function event(Event $event): Response
    {
        return $this->render('day/event.html.twig', [
            'controller_name'  => 'DayController',
            'event'            => $event,
        ]);
    }
}
