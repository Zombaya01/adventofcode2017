<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day15;

/** @phpstan-type Range array<int<0|2>,int> */
readonly class Sensor
{
    private int $Sx;
    private int $Sy;
    private int $Bx;
    private int $By;
    private int $length;

    public function __construct(string $input)
    {
        preg_match('/^Sensor at x=(?<Sx>-?\d+), y=(?<Sy>-?\d+): closest beacon is at x=(?<Bx>-?\d+), y=(?<By>-?\d+)$/', $input, $matches);
        $this->Sx = (int) $matches['Sx'];
        $this->Sy = (int) $matches['Sy'];
        $this->Bx = (int) $matches['Bx'];
        $this->By = (int) $matches['By'];

        $this->length = abs($this->Sx - $this->Bx) + abs($this->Sy - $this->By);
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function getBx(): int
    {
        return $this->Bx;
    }

    public function getBy(): int
    {
        return $this->By;
    }

    public function getSx(): int
    {
        return $this->Sx;
    }

    public function getSy(): int
    {
        return $this->Sy;
    }

    /** @return Range */
    public function getRangeAt(int $y): array
    {
        $left = $this->length - abs($this->Sy - $y);
        if ($left < 0) {
            return [];
        }

        return [$this->Sx - $left, $this->Sx + $left];
    }

    public function isInRange(int $x, int $y): bool
    {
        $distance = abs($this->Sx - $x) + abs($this->Sy - $y);

        return $distance <= $this->length;
    }
}
