<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day12;

class Mountain
{
    private int $elevation;

    /** @var array<Mountain> */
    private array $neighbours;

    /** @var array<Mountain> */
    private array $visibleNeighbours;

    public function __construct(string $elevation, private readonly int $x, private readonly int $y)
    {
        $this->elevation = match ($elevation) {
            'S'     => \ord('a'),
            'E'     => \ord('z'),
            default => \ord($elevation),
        };
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function getElevation(): int
    {
        return $this->elevation;
    }

    /**
     * @param array<Mountain> $neighbours
     *
     * @return $this
     */
    public function setNeighbours(array $neighbours, bool $isRising): static
    {
        $this->neighbours        = $neighbours;
        if ($isRising) {
            $this->visibleNeighbours = \array_filter($this->neighbours, fn (Mountain $neighbour): bool => $neighbour->getElevation() <= $this->elevation + 1);
        } else {
            $this->visibleNeighbours = \array_filter($this->neighbours, fn (Mountain $neighbour): bool => $neighbour->getElevation() >= $this->elevation - 1);
        }

        return $this;
    }

    /** @return array<Mountain> */
    public function getVisitableNeighbours(): array
    {
        return $this->visibleNeighbours;
    }

    public function __toString(): string
    {
        // return chr($this->elevation).count($this->neighbours).count($this->getVisitableNeighbours()).' ';
        return chr($this->elevation);
    }
}
