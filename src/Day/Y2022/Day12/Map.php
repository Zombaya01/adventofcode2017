<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day12;

class Map
{
    /** @var array<int,array<int,Mountain>> */
    private array $map = [];

    private Mountain $start;
    private Mountain $destination;

    public function __construct(string $input, bool $isRising)
    {
        $values = array_map(fn ($row): array => str_split($row), explode("\n", trim($input)));

        for ($y = 0, $yMax = count($values); $y < $yMax; ++$y) {
            $this->map[$y] = [];
            for ($x = 0, $xMax = count($values[0]); $x < $xMax; ++$x) {
                $this->map[$y][$x] = new Mountain($values[$y][$x],$x,$y);

                switch ($values[$y][$x]) {
                    case 'S':
                        if ($isRising) {
                            $this->start = $this->map[$y][$x];
                        }
                        break;
                    case 'E':
                        if ($isRising) {
                            $this->destination = $this->map[$y][$x];
                        } else {
                            $this->start = $this->map[$y][$x];
                        }
                        break;
                }
            }
        }

        for ($y = 0, $yMax = count($values); $y < $yMax; ++$y) {
            for ($x = 0, $xMax = count($values[0]); $x < $xMax; ++$x) {
                $neighbours = [];
                if ($y > 0) {
                    $neighbours[] = $this->map[$y-1][$x];
                }
                if ($y < $yMax - 1) {
                    $neighbours[] = $this->map[$y+1][$x];
                }
                if ($x > 0) {
                    $neighbours[] = $this->map[$y][$x-1];
                }
                if ($x < $xMax - 1) {
                    $neighbours[] = $this->map[$y][$x+1];
                }

                $this->map[$y][$x]->setNeighbours($neighbours,$isRising);
            }
        }
    }

    public function getStart(): Mountain
    {
        return $this->start;
    }

    public function getDestination(): Mountain
    {
        return $this->destination;
    }

    public function __toString(): string
    {
        return implode("\n",
            array_map(fn (array $row): string => implode('', array_map(fn (Mountain $mountain): string => (string) $mountain, $row)), $this->map)
        );
    }

    /** @param callable (Mountain): bool $isDestination */
    public function getShortestPath(int $maxRounds, callable $isDestination): int
    {
        $nextToInvestigate = $this->start->getVisitableNeighbours();
        $distances         = array_fill(0,count($this->map),array_fill(0,count($this->map[0]),null));

        for ($steps = 1; $steps < $maxRounds; ++$steps) {
            $toInvestigate = $nextToInvestigate;

            $nextToInvestigate = [];
            foreach ($toInvestigate as $mountain) {
                if ($isDestination($mountain)) {
                    return $steps;
                }
                if ($distances[$mountain->getY()][$mountain->getX()] === null) {
                    $distances[$mountain->getY()][$mountain->getX()] = $steps;

                    foreach ($mountain->getVisitableNeighbours() as $visitableNeighbour) {
                        if ($distances[$visitableNeighbour->getY()][$visitableNeighbour->getX()] === null) {
                            $nextToInvestigate[] = $visitableNeighbour;
                        }
                    }
                }
            }
            //
            //            $toInvestigateCount = count($toInvestigate);
            //            echo "== step $steps ($toInvestigateCount)==\n";
            //            echo implode("\n",
            //                array_map(fn (array $row) => implode('', array_map(fn ($distance) => $distance !== null ? sprintf('%3d ',$distance) : '    ', $row)), $distances)
            //            );
            //            echo "\n\n";
        }

        throw new \RuntimeException(sprintf('Unable to calculate in %d steps',$steps));
    }
}
