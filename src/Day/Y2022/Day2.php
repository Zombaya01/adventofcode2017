<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day2 extends AbstractDay
{
    private const SCORE_LOST = 0;
    private const SCORE_DRAW = 3;
    private const SCORE_WON  = 6;

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): ?int
    {
        $games      = DataConverter::columnOfStrings($puzzleInput);
        /** @var Game[] $splitGames */
        $splitGames = array_map(
            function ($game): Game {
                $row = explode(' ',$game);

                return new Game(TheirAction::from($row[0]),  OurAction::from($row[1]));
            },
            $games
        );

        return array_sum(array_map([$this, 'calculateGameScore'],$splitGames));
    }

    private function calculateGameScore(Game $game): int
    {
        $scoreBecauseOfSymbolPlayed = match ($game->our) {
            OurAction::ROCK     => 1,
            OurAction::PAPER    => 2,
            OurAction::SCISSORS => 3,
        };
        $scoreBecauseOfWon = match ($game->their) {
            TheirAction::ROCK     => match ($game->our) {
                OurAction::ROCK     => self::SCORE_DRAW,
                OurAction::PAPER    => self::SCORE_WON,
                OurAction::SCISSORS => self::SCORE_LOST,
            },
            TheirAction::PAPER    => match ($game->our) {
                OurAction::ROCK     => self::SCORE_LOST,
                OurAction::PAPER    => self::SCORE_DRAW,
                OurAction::SCISSORS => self::SCORE_WON,
            },
            TheirAction::SCISSORS => match ($game->our) {
                OurAction::ROCK     => self::SCORE_WON,
                OurAction::PAPER    => self::SCORE_LOST,
                OurAction::SCISSORS => self::SCORE_DRAW,
            },
        };

        return $scoreBecauseOfSymbolPlayed + $scoreBecauseOfWon;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): ?int
    {
        $games              = DataConverter::columnOfStrings($puzzleInput);
        /** @var Strategy[] $splitGamesStrategy */
        $splitGamesStrategy = array_map(
            function ($game): Strategy {
                $row = explode(' ',$game);

                return new Strategy(TheirAction::from($row[0]),  DesiredResult::from($row[1]));
            },
            $games
        );

        /** @var Game[] $splitGames */
        $splitGames = array_map(
            [$this, 'calculateActions'],
            $splitGamesStrategy
        );

        return array_sum(array_map([$this, 'calculateGameScore'],$splitGames));
    }

    public function calculateActions(Strategy $strategy): Game
    {
        $ourAction = match ($strategy->desiredResult) {
            DesiredResult::WIN  => match ($strategy->their) {
                TheirAction::ROCK     => OurAction::PAPER,
                TheirAction::PAPER    => OurAction::SCISSORS,
                TheirAction::SCISSORS => OurAction::ROCK,
            },
            DesiredResult::DRAW => match ($strategy->their) {
                TheirAction::ROCK     => OurAction::ROCK,
                TheirAction::PAPER    => OurAction::PAPER,
                TheirAction::SCISSORS => OurAction::SCISSORS,
            },
            DesiredResult::LOSE => match ($strategy->their) {
                TheirAction::ROCK     => OurAction::SCISSORS,
                TheirAction::PAPER    => OurAction::ROCK,
                TheirAction::SCISSORS => OurAction::PAPER,
            },
        };

        return new Game($strategy->their, $ourAction);
    }
}

class Game
{
    public function __construct(public TheirAction $their, public OurAction $our)
    {
    }
}

class Strategy
{
    public function __construct(public TheirAction $their,public DesiredResult $desiredResult)
    {
    }
}

enum TheirAction: string
{
    case ROCK     = 'A';
    case PAPER    = 'B';
    case SCISSORS = 'C';
}

enum OurAction: string
{
    case ROCK     = 'X';
    case PAPER    = 'Y';
    case SCISSORS = 'Z';
}

enum DesiredResult: string
{
    case LOSE = 'X';
    case DRAW = 'Y';
    case WIN  = 'Z';
}
