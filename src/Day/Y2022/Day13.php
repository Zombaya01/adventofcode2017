<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day13 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $sumSuccesfull = 0;

        $input = explode("\n\n",$puzzleInput);
        foreach ($input as $i => $puzzle) {
            $row    = explode("\n",$puzzle);
            $parsed = array_map(fn ($singleRow): mixed => json_decode($singleRow),$row);

            if ($this->isValid($parsed[0],$parsed[1]) === 1) {
                $sumSuccesfull += $i + 1;
            }
        }

        return $sumSuccesfull;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $stripped   = explode("\n",trim(str_replace("\n\n","\n",$puzzleInput)));
        $divider1   = '[[2]]';
        $divider2   = '[[6]]';
        $stripped[] = $divider1;
        $stripped[] = $divider2;
        $stripped   = array_map(fn ($singleRow): mixed => json_decode($singleRow),$stripped);

        usort($stripped,fn ($a,$b): int => -$this->isValid($a,$b));

        $result = array_map(fn ($row) => json_encode($row),$stripped);

        $return = 1;
        for ($i = 0, $iMax = count($result); $i < $iMax; ++$i) {
            if ($result[$i] === $divider1 || $result[$i] === $divider2) {
                $return *= $i+1;
            }
        }

        return $return;
    }

    /**
     * Returns null if equal, bool if known.
     *
     * @template T
     * @template T2
     *
     * @param array<T|int>|int  $a
     * @param array<T2|int>|int $b
     */
    private function isValid(array|int $a,array|int $b): int
    {
        if (is_array($a) && !is_array($b)) {
            $b = [$b];
        }
        if (is_array($b) && !is_array($a)) {
            $a = [$a];
        }

        if (is_int($a) && is_int($b)) {
            if ($a < $b) {
                return 1;
            }

            if ($a > $b) {
                return -1;
            }

            return 0;
        }

        if (is_array($a) && is_array($b)) {
            for ($i = 0; $i < 5000; ++$i) {
                if (!isset($a[$i]) && !isset($b[$i])) {
                    return 0;
                }
                if (!isset($a[$i])) {
                    return 1;
                }
                if (!isset($b[$i])) {
                    return -1;
                }
                if (($subValid = $this->isValid($a[$i], $b[$i])) !== 0) {
                    return $subValid;
                }
            }
        }

        throw new \RuntimeException('Unable to calculate isValid');
    }
}
