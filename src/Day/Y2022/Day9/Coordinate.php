<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day9;

class Coordinate
{
    public function __construct(public int $x = 0, public int $y = 0)
    {
    }

    public function __toString(): string
    {
        return sprintf('%d:%d', $this->y, $this->x);
    }
}
