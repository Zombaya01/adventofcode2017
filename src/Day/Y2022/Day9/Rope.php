<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day9;

class Rope
{
    private Coordinate $head;
    private Knot $tail;
    /** @var array<Knot> */
    private array $knots = [];

    public function __construct(int $numberOfKnots)
    {
        if ($numberOfKnots < 2) {
            throw new \RuntimeException('no fewer than 2 knots!');
        }

        $this->head = new Coordinate();
        $previous   = $this->head;

        for ($i = 1; $i < $numberOfKnots; ++$i) {
            $self          = new Coordinate();
            $this->knots[] = new Knot($previous,$self);
            $previous      = $self;
        }

        $this->tail = $this->knots[array_key_last($this->knots)];
    }

    public function moveHead(string $direction): void
    {
        switch ($direction) {
            case 'R':
                ++$this->head->x;
                break;
            case 'L':
                --$this->head->x;
                break;
            case 'U':
                ++$this->head->y;
                break;
            case 'D':
                --$this->head->y;
                break;
        }
        foreach ($this->knots as $knot) {
            $knot->move();
        }
    }

    public function getHead(): string
    {
        return (string) $this->head;
    }

    public function getTail(): string
    {
        return (string) $this->tail;
    }
}
