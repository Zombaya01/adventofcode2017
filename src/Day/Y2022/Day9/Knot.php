<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day9;

class Knot
{
    public function __construct(private readonly Coordinate $previousKnot, private readonly Coordinate $self)
    {
    }

    public function move(): void
    {
        if (abs($this->previousKnot->x - $this->self->x) > 1 || abs($this->previousKnot->y - $this->self->y) > 1) {
            $this->self->x += $this->previousKnot->x <=> $this->self->x;
            $this->self->y += $this->previousKnot->y <=> $this->self->y;
        }
    }

    public function __toString(): string
    {
        return (string) $this->self;
    }
}
