<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day6 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return $this->calculateHashStart(4, $puzzleInput);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $chars = str_split($puzzleInput);

        return $this->calculateHashStart(14, $puzzleInput);
    }

    private function calculateHashStart(int $strlen, string $puzzleInput): ?int
    {
        $chars = str_split($puzzleInput);
        for ($i = $strlen; $i < strlen($puzzleInput); ++$i) {
            if (count(array_unique(array_slice($chars, $i - $strlen, $strlen))) === $strlen) {
                return $i;
            }
        }

        return null;
    }
}
