<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day8 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $forrest = DataConverter::tableOfIntNumbers($puzzleInput);
        $length  = count($forrest);

        // All are visible by default
        $visible = array_fill(0,$length,array_fill(0,$length,0));

        for ($j = 0; $j < $length; ++$j) {
            // Do left to right
            $max = -1;
            for ($i = 0; $i < $length; ++$i) {
                if ($forrest[$j][$i] > $max) {
                    $max             = $forrest[$j][$i];
                    $visible[$j][$i] = 1;
                }
            }

            // Do right to left
            $max = -1;
            for ($i = $length - 1; $i >= 0; --$i) {
                if ($forrest[$j][$i] > $max) {
                    $max             = $forrest[$j][$i];
                    $visible[$j][$i] = 1;
                }
            }
        }

        for ($i = 0; $i < $length; ++$i) {
            // Do top to bottom
            $max = -1;
            for ($j = 0; $j < $length; ++$j) {
                if ($forrest[$j][$i] > $max) {
                    $max             = $forrest[$j][$i];
                    $visible[$j][$i] = 1;
                }
            }

            // Do right to left
            $max = -1;
            for ($j = $length - 1; $j > 0; --$j) {
                if ($forrest[$j][$i] > $max) {
                    $max             = $forrest[$j][$i];
                    $visible[$j][$i] = 1;
                }
            }
        }

        return array_sum(array_map(fn ($arr): int|float => array_sum($arr),$visible));
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $forrest = DataConverter::tableOfIntNumbers($puzzleInput);
        $length  = count($forrest);
        $scores  = [];

        for ($j = 0; $j < $length; ++$j) {
            for ($i = 0; $i < $length; ++$i) {
                $scores[] = $this->calculateVisibleTreeScore($forrest,$j,$i);
            }
        }

        return max($scores) ?: null;
    }

    /**
     * @param array<int,array<int,int>> $forrest
     */
    public function calculateVisibleTreeScore(array $forrest, int $y, int $x): int
    {
        $visible = [];

        $length        = count($forrest);
        $currentHeight = $forrest[$y][$x];

        // Do from point to right
        $j = $y;
        // Stop after at same height
        for ($i = $x + 1, $seen = 0; $i < $length; ++$i) {
            ++$seen;
            if ($forrest[$j][$i] >= $currentHeight) {
                break;
            }
        }
        $visible[] = $seen;

        // Do point to left
        for ($i = $x - 1, $seen = 0; $i >= 0; --$i) {
            ++$seen;
            if ($forrest[$j][$i] >= $currentHeight) {
                break;
            }
        }
        $visible[] = $seen;

        $i = $x;
        // Do point to bottom
        for ($j = $y + 1, $seen = 0; $j < $length; ++$j) {
            ++$seen;
            if ($forrest[$j][$i] >= $currentHeight) {
                break;
            }
        }
        $visible[] = $seen;

        // Do point to top
        for ($j = $y - 1, $seen = 0; $j >= 0; --$j) {
            ++$seen;
            if ($forrest[$j][$i] >= $currentHeight) {
                break;
            }
        }
        $visible[] = $seen;

        return (int) array_product($visible);
    }
}
