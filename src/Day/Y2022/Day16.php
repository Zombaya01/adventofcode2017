<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day16 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $valves = $this->parseInput($puzzleInput);

        $valvesToOpen = array_filter($valves, fn (Valve $valve): bool => $valve->flowRate !== 0);

        $history = [];

        return $this->calculateMaxOutput($valvesToOpen,30,0,0,0,$valves['AA'],[],$history);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return null;
    }

    /** @return array<string,Valve> */
    public function parseInput(string $input): array
    {
        $valves = [];
        $regex  = '/^Valve (?<name>\w+) has flow rate=(?<flow>\d+); tunnels? leads? to valves? (?<tunnels>.*)$/';

        $rows = explode("\n",trim($input));
        // Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
        foreach ($rows as $row) {
            preg_match($regex,$row,$matches);
            $valve                = new Valve($matches['name'], (int) $matches['flow']);
            $valves[$valve->name] = $valve;
        }

        // Add neighbours
        foreach ($rows as $row) {
            preg_match($regex,$row,$matches);
            foreach (explode(', ',$matches['tunnels']) as $neighbour) {
                $valves[$matches['name']]->addValve($valves[$neighbour]);
            }
        }

        /** @var callable(array<Valve>): bool $someDistancesAreNotKnownYetFn */
        $someDistancesAreNotKnownYetFn = static function (array $valves): bool {
            $valveCount = count($valves);

            return array_reduce($valves,static fn (bool $carry, Valve $valve): bool => $carry || $valve->getNumberOfKnownDistances() < $valveCount - 1, false);
        };

        // Add distances to other valves
        $valveCount = count($valves);
        $i          = 0;
        do {
            foreach ($valves as $valve) {
                foreach ($valves as $otherValve) {
                    if (($distance = $valve->getDistance($otherValve)) !== null) {
                        foreach ($valve->getNeighbouringValves() as $neighbouringValve) {
                            $neighbouringValve->setDistance($otherValve,$distance + 1);
                            $otherValve->setDistance($neighbouringValve,$distance + 1);
                        }
                    }
                }
            }
            ++$i;
        } while ($someDistancesAreNotKnownYetFn && $i <= $valveCount);

        return $valves;
    }

    /** @param array<Valve> $valves */
    public function toUml(array $valves): string
    {
        $output = [];

        $output[] = '@startuml';

        foreach ($valves as $valve) {
            $output[] = sprintf('object %s', $valve->name);
            $output[] = sprintf('%s : flowRate %d', $valve->name, $valve->flowRate);
            foreach ($valves as $other) {
                if ($other !== $valve) {
                    $output[] = sprintf('%s : to%s %d', $valve->name, $other->name, $valve->getDistance($other));
                }
            }
        }
        $output[] = '';
        foreach ($valves as $valve) {
            foreach ($valve->getNeighbouringValves() as $neighbouringValve) {
                if ($valve->name < $neighbouringValve->name) {
                    $output[] = sprintf('%s -- %s',$valve->name, $neighbouringValve->name);
                }
            }
        }
        $output[] = '@enduml';

        return implode("\n",$output);
    }

    /**
     * @param array<Valve> $valvesToOpen
     */
    public function calculateMaxOutput(array $valvesToOpen, int $parentSteps, int $parentMax, int $parentOutput, int $parentFlowRate, Valve $currentLocation, array $history, array &$maxHistory): int
    {
        $currentMax = 0;
        foreach ($valvesToOpen as $valve) {
            $currentFlowRate = $parentFlowRate;
            $currentOutput   = $parentOutput;
            $steps           = $parentSteps;
            $currentHistory  = array_merge($history,[$valve]);

            // If it takes longer to get to the valve and open it than we have steps left, skip
            if ($currentLocation->getDistance($valve) + 1 > $steps) {
                continue;
            }

            // Move to valve
            $steps -= $currentLocation->getDistance($valve);

            // Open the valve
            --$steps;

            $remainingValves = array_filter($valvesToOpen,static fn (Valve $valveToOpen): bool => $valve !== $valveToOpen);

            $newMax = $this->calculateMaxOutput($remainingValves, $steps, $currentMax,$currentOutput, $valve->flowRate, $valve, $currentHistory, $maxHistory);

            if ($newMax > $currentMax) {
                $currentMax = $newMax;
                $maxHistory = $currentHistory;
            }
        }

        return $parentSteps * $parentFlowRate + $parentOutput + $currentMax;
    }
}
