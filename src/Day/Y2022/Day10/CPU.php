<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day10;

class CPU
{
    private int $tickCounter = 0;
    private int $step        = 0;
    /** @var array<string,int> */
    private array $register = [];
    /** @var array<string,int> */
    private array $newValues = [];

    /** @var array<callable> */
    private array $substeps = [];

    /** @param array<string> $instructions */
    public function __construct(private readonly array $instructions)
    {
        $this->register['x'] = 1;
    }

    public function getRegister(string $name): int
    {
        return $this->register[$name] ?? 0;
    }

    public function ticks(int $number): void
    {
        for ($i = 0; $i < $number; ++$i) {
            $this->tick();
        }
    }

    public function tick(): void
    {
        // If nothing more to do, load next step
        if ($this->substeps === []) {
            $nextInstruction = trim($this->instructions[$this->step] ??  'noop');
            //            echo $nextInstruction."\n";
            switch (1) {
                case preg_match('/^noop$/', $nextInstruction, $matches):
                    $this->substeps[] = $this->noop();
                    break;
                case preg_match('/^add(?<register>\w) (?<number>-?\d+)$/', $nextInstruction, $matches):
                    $this->substeps[] = $this->noop();
                    $this->substeps[] = $this->add($matches['register'], (int) $matches['number']);
                    break;
                default:
                    throw new \RuntimeException(sprintf('Invalid next step: "%s"',$nextInstruction));
            }
            ++$this->step;
        }

        // Do the next action
        $nextAction = array_shift($this->substeps);
        $nextAction();

        // Write values to register
        foreach ($this->newValues as $key => $value) {
            $this->register[$key] = $value;
        }
        $this->newValues = [];

        ++$this->tickCounter;

        //        echo sprintf("%2d: %d\n",$this->tickCounter, $this->register['x']);
    }

    private function noop(): callable
    {
        return static fn (): true => true;
    }

    private function add(string $register, int $value): callable
    {
        return function () use ($register, $value): true {
            $this->newValues[$register] = $this->register[$register] + $value;

            return true;
        };
    }
}
