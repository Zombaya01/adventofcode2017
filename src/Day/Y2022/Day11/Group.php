<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day11;

use Doctrine\Common\Collections\ArrayCollection;

class Group
{
    private int $rounds = 0;

    /** @var ArrayCollection<int, Monkey> */
    private ArrayCollection $monkeys;

    public function __construct(string $input, callable $worryReducer)
    {
        $this->monkeys = new ArrayCollection([]);
        $monkeyInputs  = explode("\n\n", $input);
        foreach ($monkeyInputs as $monkeyInput) {
            new Monkey($this->monkeys, $worryReducer, $monkeyInput);
        }
    }

    public function rounds(int $number = 1): void
    {
        $this->rounds += $number;

        for ($i = 0; $i < $number; ++$i) {
            foreach ($this->monkeys as $monkey) {
                $monkey->act();
            }
        }
    }

    public function __toString(): string
    {
        return implode("\n",$this->monkeys->map(static fn (Monkey $monkey): string => (string) $monkey)->toArray());
    }

    public function showInspections(): string
    {
        return sprintf(
            "== After round %d ==\n%s\n\n",
            $this->rounds,
            implode("\n",$this->monkeys->map(static fn (Monkey $monkey): string => $monkey->printInspections())->toArray())
        );
    }

    /** @return array<int> */
    public function getTopNumberOfInspections(int $number): array
    {
        $scores = $this->monkeys->map(fn (Monkey $monkey): int => $monkey->getNumberOfInspections())->toArray();
        rsort($scores);

        return array_slice($scores, 0, $number);
    }

    public function getLcmDividers(): string
    {
        if (array_diff([23, 19, 13, 17],$this->monkeys->map(fn (Monkey $monkey): int => $monkey->getDivisible())->toArray()) === []) {
            return '96577';
        }
        if (array_diff([13, 19, 5, 2, 17, 11, 7, 3],$this->monkeys->map(fn (Monkey $monkey): int => $monkey->getDivisible())->toArray()) === []) {
            return '9699690';
        }
        throw new \InvalidArgumentException(sprintf('Unable to calculcate LCM of dividers for array %s',implode(' ',$this->monkeys->map(fn (Monkey $monkey): int => $monkey->getDivisible())->toArray())));
    }
}
