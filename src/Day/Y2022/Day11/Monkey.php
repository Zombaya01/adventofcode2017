<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day11;

use Doctrine\Common\Collections\Collection;
use Laminas\Math\BigInteger\Adapter\AdapterInterface;
use Laminas\Math\BigInteger\BigInteger;

// /**
// * @property Collection<Monkey> $group
// * @property callable (string): string $worryReducer
// */
class Monkey
{
    private int $number;

    private int $divisible;

    private int $numberOfInspections = 0;

    /** @var callable (string): string */
    private $operation;
    /** @var callable (string): Monkey */
    private $throwDecider;

    /** @var array<string> */
    private array $items;

    private AdapterInterface $bigInt;

    /**
     * @param Collection<int,Monkey>    $group
     * @param callable (string): string $worryReducer
     */
    public function __construct(
        private readonly Collection $group,
        private $worryReducer,
        string $input)
    {
        $this->bigInt = BigInteger::factory();

        $regex = [
            'Monkey (?<number>\d+):',
            'Starting items: (?<items>(?>\d+,? ?)+)',
            'Operation: new = old (?<operand>[\*\+]) (?<value>\w+)',
            'Test: divisible by (?<divisible>\d+)',
            'If true: throw to monkey (?<true>\d+)',
            'If false: throw to monkey (?<false>\d+)',
        ];

        $regex = '/'.implode('\s+', $regex).'/m';

        preg_match($regex, $input, $matches);

        $this->number = (int) $matches['number'];
        $this->items  = explode(', ', $matches['items']);

        $operand = $matches['operand'];
        $value   = $matches['value'];

        $this->operation = function (string $item) use ($value, $operand): string {
            if ($value === 'old') {
                $value = $item;
            }

            if ($operand === '*') {
                return $this->bigInt->mul($item,$value);
            }

            if ($operand === '+') {
                return $this->bigInt->add($item,$value);
            }

            throw new \InvalidArgumentException(sprintf('Invalid operand "%s"', $operand));
        };

        $divisible       = $matches['divisible'];
        $this->divisible = (int) $divisible;

        $true      = (int) $matches['true'];
        $false     = (int) $matches['false'];

        $this->throwDecider = fn (string $item): Monkey => $this->bigInt->mod($item,$divisible) === '0' ? $this->group->get($true) : $this->group->get($false);

        // Add self to group
        $this->group->set($this->number, $this);
    }

    public function getDivisible(): int
    {
        return $this->divisible;
    }

    public function act(): void
    {
        while (($item = array_shift($this->items)) !== null) {
            ++$this->numberOfInspections;

            $newValue = ($this->operation)($item);
            $newValue = ($this->worryReducer)($newValue);

            ($this->throwDecider)($newValue)->receive($newValue);
        }
    }

    public function receive(string $item): void
    {
        $this->items[] = $item;
    }

    public function __toString(): string
    {
        return sprintf('Monkey %d: %s', $this->number, implode(', ', $this->items));
    }

    public function printInspections(): string
    {
        return sprintf('Monkey %d inspected items %d times.', $this->number, $this->numberOfInspections);
    }

    public function getNumberOfInspections(): int
    {
        return $this->numberOfInspections;
    }
}
