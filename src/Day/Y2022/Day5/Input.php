<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day5;

use App\Utils\DataConverter;

/**
 * @phpstan-type StackArray array<int,string[]>
 */
class Input
{
    /** @var StackArray */
    public array $stacks;
    /** @var array<Move> */
    public array $moves;

    public function __construct(string $input)
    {
        [$unparsedStacks, $unparsedMoves] = explode("\n\n",$input);
        $explodedUnparsedStacks           = explode("\n",$unparsedStacks);

        $unconvertedStackNames = array_pop($explodedUnparsedStacks);
        $stackNames            = DataConverter::rowOfInt($unconvertedStackNames,' ');

        $reversedUnparsedStacks = array_reverse($explodedUnparsedStacks);

        $values = [];
        foreach ($reversedUnparsedStacks as $row) {
            $parsedRow = array_map(fn ($cell): string => trim($cell,'[] '),str_split($row,4));
            foreach ($parsedRow as $key => $value) {
                if ($value !== '') {
                    $values[$key][] = $value;
                }
            }
        }

        $this->stacks = array_combine($stackNames,$values);
        $this->moves  = array_map(static fn (string $raw): \App\Day\Y2022\Day5\Move => new Move($raw), DataConverter::columnOfStrings($unparsedMoves));
    }
}
