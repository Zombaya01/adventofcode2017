<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day5;

class Move
{
    public int $amount;
    public int $from;
    public int $to;

    public function __construct(string $input)
    {
        preg_match('/^move (?<amount>\d+) from (?<from>\d+) to (?<to>\d+)$/', $input, $matches);

        $this->amount = (int) $matches['amount'];
        $this->from   = (int) $matches['from'];
        $this->to     = (int) $matches['to'];
    }
}
