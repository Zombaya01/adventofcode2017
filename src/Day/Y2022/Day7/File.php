<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day7;

class File implements FileSystemEntryInterface
{
    private Directory $parent;

    public function __construct(private readonly string $name, private readonly int $size)
    {
    }

    public function getParent(): Directory
    {
        return $this->parent;
    }

    public function setParent(Directory $parent): FileSystemEntryInterface
    {
        $this->parent = $parent;

        return $this;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
