<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day7;

interface FileSystemEntryInterface
{
    public function getName(): string;

    public function getSize(): int;

    public function setParent(Directory $parent): self;
}
