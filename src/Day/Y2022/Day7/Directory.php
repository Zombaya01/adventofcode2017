<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day7;

class Directory implements FileSystemEntryInterface
{
    /** @var array<string, FileSystemEntryInterface> */
    private array $children        = [];
    private ?Directory $parent     = null;

    public function __construct(private readonly string $name)
    {
    }

    public function addChild(FileSystemEntryInterface $child): static
    {
        $this->children[$child->getName()] = $child;
        $child->setParent($this);

        return $this;
    }

    public function getSubdirectory(string $name): Directory
    {
        if (!isset($this->children[$name]) || !$this->children[$name] instanceof Directory) {
            throw new \RuntimeException(sprintf('Child %s not found', $name));
        }

        return $this->children[$name];
    }

    public function getSize(): int
    {
        return array_sum(array_map(static fn (FileSystemEntryInterface $child): int => $child->getSize(), $this->children));
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setParent(Directory $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getParent(): ?Directory
    {
        return $this->parent;
    }
}
