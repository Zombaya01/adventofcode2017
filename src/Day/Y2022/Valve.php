<?php

declare(strict_types=1);

namespace App\Day\Y2022;

class Valve
{
    /** @var array<Valve> */
    private array $neighbouringValves;

    /** @var array<string,int> */
    private array $distances;

    public function __construct(readonly public string $name, readonly public int $flowRate)
    {
    }

    public function addValve(Valve $valve): void
    {
        $this->neighbouringValves[]    = $valve;
        $this->distances[$valve->name] = 1;
    }

    /** @return array<Valve> */
    public function getNeighbouringValves(): array
    {
        return $this->neighbouringValves;
    }

    public function setDistance(Valve $other, int $distance): void
    {
        if ($other === $this) {
            return;
        }

        if (isset($this->distances[$other->name]) && $this->distances[$other->name] <= $distance) {
            return;
        }

        $this->distances[$other->name] = $distance;
    }

    public function getDistance(Valve $other): ?int
    {
        return $this->distances[$other->name] ?? null;
    }

    public function getNumberOfKnownDistances(): int
    {
        return count($this->distances);
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
