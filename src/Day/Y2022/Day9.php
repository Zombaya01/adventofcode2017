<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Day\Y2022\Day9\Rope;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day9 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return $this->countTailPositions($puzzleInput, 2);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return $this->countTailPositions($puzzleInput, 10);
    }

    private function countTailPositions(string $puzzleInput, int $amountOfKnots): int
    {
        $input = array_map(
            static function (string $row): array {
                sscanf($row, '%c %d', $direction, $number);

                return ['direction' => (string) $direction, 'number' => (int) $number];
            },
            explode("\n", $puzzleInput)
        );

        $rope    = new Rope($amountOfKnots);
        $tailLog = [];
        foreach ($input as $row) {
            for ($i = 0; $i < $row['number']; ++$i) {
                $rope->moveHead($row['direction']);
                $tailLog[] = $rope->getTail();
            }
        }

        return count(array_unique($tailLog));
    }
}
