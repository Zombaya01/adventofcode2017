<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day7 extends AbstractDay
{
    /** @var array<Day7\Directory> */
    private array $allDirectories = [];
    private Day7\Directory $root;

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $this->parseInput($puzzleInput);

        return array_sum(
            array_map(
                fn (Day7\Directory $dir): int => $dir->getSize(),
                array_filter($this->allDirectories,fn (Day7\Directory $dir): bool => $dir->getSize() <= 100000)
            )
        );
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $this->parseInput($puzzleInput);

        $sizeNeeded = $this->root->getSize() - 70000000 + 30000000;

        $possible = array_map(
            fn (Day7\Directory $dir): int => $dir->getSize(),
            array_filter(
                $this->allDirectories,
                fn (Day7\Directory $directory): bool => $directory->getSize() >= $sizeNeeded
            )
        );

        sort($possible);

        return array_shift($possible);
    }

    private function parseInput(string $input): void
    {
        $this->root = new Day7\Directory('/');
        $currentDir = $this->root;

        $lines = DataConverter::columnOfStrings($input);
        foreach ($lines as $line) {
            switch (1) {
                case preg_match('/^\$ cd (?<dir>[\w\.\/]+)$/',$line,$matches):
                    $currentDir = match ($matches['dir']) {
                        '/'     => $this->root,
                        '..'    => $currentDir->getParent(),
                        // no break
                        default => $currentDir->getSubdirectory($matches['dir']),
                    };
                    break;

                case preg_match('/^\$ ls$/',$line,$matches):
                    // do nothing
                    break;

                case preg_match('/^dir (?<name>[\w]+)$/',$line,$matches):
                    $newDir                 = new Day7\Directory($matches['name']);
                    $this->allDirectories[] = $newDir;
                    $currentDir->addChild($newDir);
                    break;

                case preg_match('/^(?<size>\d+) (?<name>[\w\.]+)$/',$line,$matches):
                    $currentDir->addChild(new Day7\File($matches['name'],(int) $matches['size']));
                    break;

                default:
                    throw new \RuntimeException(sprintf('Invalid line "%s"',$line));
            }
        }
    }
}
