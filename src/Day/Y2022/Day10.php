<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Day\Y2022\Day10\CPU;
use App\Utils\DataConverter;
use App\Utils\OCR;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day10 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $cpu = new CPU(DataConverter::columnOfStrings($puzzleInput));

        $cpu->ticks(19);

        $scores = [];
        for ($i=20; $i < 220; $i += 40) {
            $scores[] = $i * $cpu->getRegister('x');
            $cpu->ticks(40);
        }
        $scores[] = $i * $cpu->getRegister('x');

        return array_sum($scores);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $cpu = new CPU(DataConverter::columnOfStrings($puzzleInput));

        $output = '';
        for ($j=0; $j < 6; ++$j) {
            for ($i=0; $i < 40; ++$i) {
                $x = $cpu->getRegister('x');
                $output .= abs($x - $i) <= 1 ? '#' : '.';
                $cpu->tick();
            }
            $output .= "\n";
        }

        try {
            return (new OCR())->scan($output);
        } catch (\Throwable $e) {
            if ($consoleOutput instanceof OutputInterface) {
                $consoleOutput->writeln($e->getMessage());
            }

            return trim($output);
        }
    }
}
