<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Day\Y2022\Day14\Cave;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day14 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $cave = new Cave($puzzleInput);
        $i    = 0;
        do {
            $intoTheAbyss = $cave->dropSandKernel(500);
            ++$i;
        } while (!$intoTheAbyss);

        return $i - 1;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $originalCave = new Cave($puzzleInput);
        $maxY         = $originalCave->getMaxY() + 2;
        $minX         = $originalCave->getMinX() - $maxY - 2;
        $maxX         = $originalCave->getMaxX() + $maxY + 2;
        $floor        = sprintf('%d,%d -> %d,%d',$minX,$maxY,$maxX,$maxY);

        return $this->part1($puzzleInput."\n".$floor,$consoleInput,$consoleOutput);
    }
}
