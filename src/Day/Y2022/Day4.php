<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day4 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): ?int
    {
        $parsedInput = $this->parseInput($puzzleInput);

        $counter = 0;
        /** @var array{0: Instruction, 1: Instruction} $row */
        foreach ($parsedInput as $row) {
            [$a,$b] = $row;

            if ($this->fullyContain($a,$b) || $this->fullyContain($b,$a)) {
                ++$counter;
            }
        }

        return $counter;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): ?int
    {
        $parsedInput = $this->parseInput($puzzleInput);

        $counter = 0;
        /** @var array{0: Instruction, 1: Instruction} $row */
        foreach ($parsedInput as $row) {
            [$a,$b] = $row;

            if ($this->overlap($a,$b) || $this->overlap($b,$a)) {
                ++$counter;
            }
        }

        return $counter;
    }

    /**
     * @return array{array{Instruction, Instruction}}
     */
    private function parseInput(string $puzzleInput): array
    {
        $input = DataConverter::columnOfStrings($puzzleInput);

        /** @var array{array{Instruction, Instruction}} $result */
        $result = array_map(
            function (string $row): array {
                /** @var array<int,string> $elfInstructions */
                $elfInstructions = explode(',', $row);

                if (count($elfInstructions) > 2) {
                    throw new \InvalidArgumentException('input invalid');
                }

                // Parsed instructions
                return array_map(fn ($instruction): \App\Day\Y2022\Instruction => new Instruction($instruction), $elfInstructions);
            },
            $input
        );

        return $result;
    }

    private function fullyContain(Instruction $a, Instruction $b): bool
    {
        return $a->start <= $b->start && $a->end >= $b->end;
    }

    private function overlap(Instruction $a, Instruction $b): bool
    {
        // Starts before
        if ($a->start <= $b->start && $a->end >= $b->start) {
            return true;
        }

        // Ends after
        return $a->start <= $b->end && $a->start >= $b->start;
    }
}

class Instruction
{
    public int $start;
    public int $end;

    public function __construct(string $instruction)
    {
        [$this->start,$this->end] = array_map('intval',explode('-',$instruction));
    }
}
