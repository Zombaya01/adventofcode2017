<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Day\Y2022\Day11\Group;
use Laminas\Math\BigInteger\BigInteger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day11 extends AbstractDay
{
    /**
     * @return callable (string): string
     */
    public static function part1WorryReducer(): callable
    {
        $bigInt = BigInteger::factory();

        return static fn (string $input) => $bigInt->div($input, '3');
    }

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $group = new Group($puzzleInput, self::part1WorryReducer());

        $group->rounds(20);

        return (int) array_product($group->getTopNumberOfInspections(2));
    }

    /**
     * @return callable (string): string
     */
    public static function part2WorryReducer(string $puzzleInput): callable
    {
        $group = new Group($puzzleInput, fn (string $input): string => $input);

        $lcm = $group->getLcmDividers();

        $bigInt = BigInteger::factory();

        return static fn (string $input) => $bigInt->mod($input, $lcm);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $group = new Group($puzzleInput, self::part2WorryReducer($puzzleInput));

        $group->rounds(10000);

        return (int) array_product($group->getTopNumberOfInspections(2));
    }
}
