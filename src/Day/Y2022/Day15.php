<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Day\Y2022\Day15\Sensor;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/** @phpstan-import-type Range from \App\Day\Y2022\Day15\Sensor*/
class Day15 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return $this->calculatePart1($puzzleInput,2000000);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return $this->calculatePart2($puzzleInput,4000000);
    }

    public function calculatePart1(string $puzzleInput, int $y): int
    {
        $sensors = $this->toSensorArray($puzzleInput);

        $ranges = array_map(
            static fn (Sensor $sensor): array => $sensor->getRangeAt($y),
            $sensors,
        );

        $ranges = $this->mergeRanges($ranges);

        $numberOfPlacesWhereNoBeaconsShouldExist = array_sum(
            array_map(
                fn (array $range): int => $range[1] - $range[0] + 1,
                $ranges
            )
        );
        $numberOfBeaconsInRange = $this->calculateNumberOfBeaconsInRange($ranges, $sensors, $y);

        return $numberOfPlacesWhereNoBeaconsShouldExist - $numberOfBeaconsInRange;
    }

    public function calculatePart2(string $puzzleInput, int $max): int
    {
        $sensors = $this->toSensorArray($puzzleInput);

        usort($sensors, static fn (Sensor $a, Sensor $b): int => $a->getLength() <=> $b->getLength());

        foreach ($sensors as $root) {
            $otherSensors = array_filter($sensors, fn (Sensor $a): bool => $a !== $root);
            for ($yDiff = -$root->getLength() - 1; $yDiff < $root->getLength() + 1; ++$yDiff) {
                $xDiff = $root->getLength() + 1 - abs($yDiff);
                $y     = $root->getSy()     + $yDiff;
                $x1    = $root->getSx() - $xDiff;
                $x2    = $root->getSx() + $xDiff;
                if ($y < 0) {
                    continue;
                }
                if ($y > $max) {
                    continue;
                }
                if (($x1 < 0 || $x1 > $max) && ($x2 < 0 || $x2 > $max)) {
                    continue;
                }

                if ($x1 >= 0 && $x1 <= $max && $this->isNeverInRange($otherSensors,$x1,$y)) {
                    $x = $x1;
                    break 2;
                }
                if ($x1 !== $x2 && $x2 >= 0 && $x2 <= $max && $this->isNeverInRange($otherSensors,$x2,$y)) {
                    $x = $x2;
                    break 2;
                }
            }
        }

        if (!isset($x,$y)) {
            throw new \RuntimeException('solution not found');
        }

        return ($x * 4000000) + $y;
    }

    /**
     * @param array<Sensor> $sensors
     */
    public function isNeverInRange(array $sensors, int $x, int $y): bool
    {
        foreach ($sensors as $sensor) {
            if ($sensor->isInRange($x, $y)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array<Range> $ranges
     *
     * @return array<Range>
     */
    public function mergeRanges(array $ranges): array
    {
        $ranges = array_values(array_filter($ranges,fn (array $ar): bool => $ar !== []));
        usort($ranges,fn (array $a, array $b): int => $a[0] <=> $b[0]);

        for ($i=0, $iMax = count($ranges); $i < $iMax; ++$i) {
            if (!isset($ranges[$i])) {
                continue;
            }
            for ($j = $i+1; $j < $iMax; ++$j) {
                if (!isset($ranges[$j])) {
                    continue;
                }
                if ($ranges[$j][0] <= $ranges[$i][1] + 1) {
                    $ranges[$i][1] = max($ranges[$i][1],$ranges[$j][1]);
                    unset($ranges[$j]);
                }
            }
        }

        return array_values($ranges);
    }

    /**
     * @param array<Sensor> $sensors
     * @param array<Range>  $ranges
     */
    private function calculateNumberOfBeaconsInRange(array $ranges, array $sensors, int $y): int
    {
        $sensorsAtY = array_filter($sensors, static fn (Sensor $sensor): bool => $sensor->getBy() === $y);

        // Filter out duplicate beacons
        $keys    = array_map(fn (Sensor $sensor): string => sprintf('%d-%d',$sensor->getBx(),$sensor->getBy()),$sensorsAtY);
        $beacons = array_combine($keys,$sensorsAtY);

        $sum = 0;
        foreach ($ranges as $range) {
            foreach ($beacons as $sensor) {
                if ($sensor->getBx() >= $range[0] && $sensor->getBx() <= $range[1]) {
                    ++$sum;
                }
            }
        }

        return $sum;
    }

    /** @return array<Sensor> */
    public function toSensorArray(string $puzzleInput): array
    {
        $rows = explode("\n", trim($puzzleInput));

        return array_map(
            static fn (string $row): \App\Day\Y2022\Day15\Sensor => new Sensor($row),
            $rows
        );
    }
}
