<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day3 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): ?int
    {
        $rows = DataConverter::ColumnOfStrings($puzzleInput);

        return array_sum(
            array_map(
                function (string $row): int {
                    /*
                     * @phpstan-ignore-next-line
                     */
                    $splitString = str_split($row, strlen($row) / 2);

                    return $this->getPriorityOfRowSet($splitString);
                },
                $rows
            )
        );
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): ?int
    {
        $rows = DataConverter::ColumnOfStrings($puzzleInput);

        $setsOf3Strings = array_chunk($rows, 3);
        $priorityOfSets = array_map(
            [$this, 'getPriorityOfRowSet'],
            $setsOf3Strings
        );

        return array_sum(
            $priorityOfSets
        );
    }

    private function toPriority(string $letter): int
    {
        $integerValue = ord($letter);
        if ($integerValue < ord('a')) {
            return $integerValue - ord('A') + 27;
        }

        return $integerValue - ord('a') + 1;
    }

    /** @param string[] $strings */
    private function getCommon(array $strings): string
    {
        /*
         * @phpstan-ignore-next-line
         */
        $setsOfLetterArrays = array_map(static fn (string $string): array => str_split($string), $strings);
        $duplicateLetters   = array_intersect(...$setsOfLetterArrays);

        return array_values(array_unique($duplicateLetters))[0];
    }

    /** @param string[] $rowSet */
    private function getPriorityOfRowSet(array $rowSet): int
    {
        return $this->toPriority($this->getCommon($rowSet));
    }
}
