<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @phpstan-import-type StackArray from Day5\Input
 */
class Day5 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input  = new Day5\Input($puzzleInput);
        $stacks = $input->stacks;

        foreach ($input->moves as $move) {
            if ($move->amount > count($stacks[$move->from])) {
                throw new \RuntimeException('trying to move too much');
            }
            for ($i=0; $i<$move->amount; ++$i) {
                $stacks[$move->to][] = array_pop($stacks[$move->from]);
            }
        }

        return $this->stacksToResult($stacks);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input  = new Day5\Input($puzzleInput);
        $stacks = $input->stacks;

        foreach ($input->moves as $move) {
            if ($move->amount > count($stacks[$move->from])) {
                throw new \RuntimeException('trying to move too much');
            }
            $moved = [];
            for ($i=0; $i<$move->amount; ++$i) {
                $moved[] = array_pop($stacks[$move->from]);
            }
            $stacks[$move->to] = array_merge($stacks[$move->to], array_reverse($moved));
        }

        return $this->stacksToResult($stacks);
    }

    /**
     * @param StackArray $stacks
     */
    private function stacksToResult(array $stacks): string
    {
        return implode('', array_map(fn (array $stack): mixed => array_pop($stack), $stacks));
    }
}
