<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day14;

class Cave
{
    /** @var array<int,array<int,Type>> */
    private array $map;

    private int $xOffset;
    private int $maxY;
    private int $maxX;

    private int $debug = 0;
    private int $debugSteps;

    public function __construct(string $input)
    {
        /** @var array<array<Coordinate>> $parsedRows */
        $parsedRows = array_map(
            function (string $row): array {
                $regex = '/(?<coor>\d+,\d+)/';
                preg_match_all($regex,$row,$matches);

                return array_map(
                    fn (string $rawCoordinates): \App\Day\Y2022\Day14\Coordinate => new Coordinate($rawCoordinates),
                    $matches['coor']
                );
            },
            array_unique(explode("\n",$input))
        );

        /** @var non-empty-array<Coordinate> $allCoordinates */
        $allCoordinates = array_merge(...$parsedRows);

        $this->xOffset = min(array_map(fn (Coordinate $co): int => $co->getX(),$allCoordinates));

        $this->maxX = max(array_map(fn (Coordinate $co): int => $co->getX(), $allCoordinates)) - $this->xOffset;
        $this->maxY = max(array_map(fn (Coordinate $co): int => $co->getY(), $allCoordinates));

        $this->map = array_fill(
            0,
            $this->maxY + 1,
            array_fill(
                0,
                $this->maxX + 1,
                Type::Empty
            )
        );

        foreach ($parsedRows as $rowKey => $row) {
            $this->verboseDebug(fn (): string => "=== Row $rowKey  ===");
            for ($i = 1, $iMax = count($row); $i < $iMax; ++$i) {
                $this->verboseDebug(fn (): string => "==  Line $i  ==");
                $previous = $row[$i-1];
                $current  = $row[$i];

                $xDiff = $previous->getx() <= $current->getx() ? 1 : -1;
                $yDiff = $previous->getY() <= $current->getY() ? 1 : -1;

                for ($x = $previous->getX(); $xDiff === 1 ? $x <= $current->getX() : $x >= $current->getX(); $x += $xDiff) {
                    for ($y = $previous->getY(); $yDiff === 1 ? $y <= $current->getY() : $y >= $current->getY(); $y += $yDiff) {
                        $this->map[$y][$x-$this->xOffset] = Type::Rock;

                        $this->verboseDebug(fn (): string => $this."\n");
                    }
                }
            }
        }
    }

    public function getNumberOfStepsToDropKernel(): int
    {
        return $this->debugSteps;
    }

    public function getMaxY(): int
    {
        return $this->maxY;
    }

    public function getMinX(): int
    {
        return $this->xOffset;
    }

    public function getMaxX(): int
    {
        return $this->maxX + $this->xOffset;
    }

    /**
     * @return $this
     */
    public function setDebug(int $debug): static
    {
        $this->debug = $debug;

        return $this;
    }

    private function getValue(int $x, int $y): Type
    {
        return $this->map[$y][$x-$this->xOffset] ?? Type::OutOfBounds;
    }

    private function setValue(int $x, int $y, Type $type): self
    {
        $this->map[$y][$x-$this->xOffset] = $type;

        return $this;
    }

    /**
     * Return true when sand falls in the abyss or is blocked on top.
     */
    public function dropSandKernel(int $dropOnX, int $amount = 1): bool
    {
        $this->debugSteps = 0;
        for ($i = 0; $i < $amount; ++$i) {
            $x        = $dropOnX;
            $previous = null;

            if ($this->getValue($dropOnX,0) !== Type::Empty) {
                return true;
            }

            for ($y = 0; $y <= $this->maxY+1; ++$y) {
                $possible = ($y === 0 ? [$x] : [$x, $x-1, $x+1]);

                $isAbleToDrop = false;
                foreach ($possible as $possibleX) {
                    ++$this->debugSteps;
                    switch ($this->getValue($possibleX,$y)) {
                        case Type::OutOfBounds:
                            if ($previous instanceof Coordinate) {
                                $this->setValue($previous->getX(),$previous->getY(),Type::Empty);
                            }

                            $this->debug(fn (): string => (string) $this."\n");

                            return true;
                        case Type::Sand:
                        case Type::Rock:
                            continue 2; // Do not store here
                        case Type::Empty:
                            $isAbleToDrop = true;
                            $x            = $possibleX;
                            break 2; // break out of foreach
                        default:
                            throw new \RuntimeException('Ai ai ai');
                    }
                }

                if ($isAbleToDrop) {
                    $this->setValue($x,$y,Type::Sand);
                    if ($previous instanceof Coordinate) {
                        $this->setValue($previous->getX(),$previous->getY(),Type::Empty);
                    }

                    $previous = new Coordinate("$x,$y");
                    $this->verboseDebug(fn (): string => (string) $this."\n");
                } else {
                    continue 2;
                }
            }
        }

        $this->debug(fn (): string => (string) $this."\n");

        return false;
    }

    /** @param callable ():string $fn */
    private function debug(callable $fn): void
    {
        if ($this->debug > 0) {
            echo $fn()."\n";
        }
    }

    private function verboseDebug(callable $fn): void
    {
        if ($this->debug > 1) {
            echo $fn()."\n";
        }
    }

    public function __toString(): string
    {
        return implode(
            "\n",
            array_map(
                fn (array $row): string => implode('', array_map(fn (Type $type) => $type->value,$row)),
                $this->map
            )
        );
    }
}
