<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day14;

class Coordinate
{
    private int $x;
    private int $y;

    public function __construct(string $raw)
    {
        $split   = explode(',', $raw);
        $this->x = (int) $split[0];
        $this->y = (int) $split[1];
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }
}
