<?php

declare(strict_types=1);

namespace App\Day\Y2022\Day14;

enum Type: string
{
    case Rock        = '#';
    case Sand        = 'o';
    case Start       = '+';
    case Empty       = '.';
    case OutOfBounds = '%';
}
