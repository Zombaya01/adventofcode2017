<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Day\Y2022\Day12\Map;
use App\Day\Y2022\Day12\Mountain;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day12 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $map         = new Map($puzzleInput, true);
        $destination = $map->getDestination();

        return $map->getShortestPath(1000, fn (Mountain $mountain): bool => $mountain === $destination);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return (new Map($puzzleInput, false))->getShortestPath(500,fn (Mountain $mountain): bool => in_array((string) $mountain,['a', 'S'],true));
    }
}
