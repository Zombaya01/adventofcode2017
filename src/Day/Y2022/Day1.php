<?php

declare(strict_types=1);

namespace App\Day\Y2022;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day1 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfGroupedIntsSplitByBlankLine($puzzleInput);

        $sumsPerImp = array_map('array_sum',$parsedInput);

        return max(...$sumsPerImp);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfGroupedIntsSplitByBlankLine($puzzleInput);

        $sumsPerImp = array_map('array_sum',$parsedInput);

        rsort($sumsPerImp);

        return array_sum(array_slice($sumsPerImp,0,3));
    }
}
