<?php

namespace App\Day\Y2018;

use App\Day\AbstractDay;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day2 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $splittedInput = mb_strpos((string) $puzzleInput,"\n") !== false ? explode("\n",(string) $puzzleInput) : explode(' ',(string) $puzzleInput);

        $two   = 0;
        $three = 0;
        foreach ($splittedInput as $id) {
            $sums = [];
            foreach (mb_str_split($id) as $letter) {
                $sums[$letter] = ($sums[$letter] ?? 0) + 1;
            }

            $addTwo   = false;
            $addThree = false;
            foreach ($sums as $lettersum) {
                if ($lettersum === 2) {
                    $addTwo = true;
                }
                if ($lettersum === 3) {
                    $addThree = true;
                }
            }
            if ($addTwo) {
                ++$two;
            }
            if ($addThree) {
                ++$three;
            }
        }

        return $two * $three;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): string
    {
        $splittedInput = str_contains($puzzleInput, "\n") ? explode("\n", $puzzleInput) : explode(' ', $puzzleInput);

        for ($i = 0,$iMax = count($splittedInput); $i < $iMax; ++$i) {
            for ($j = $i + 1; $j < $iMax; ++$j) {
                $word1 = mb_str_split($splittedInput[$i]);
                $word2 = mb_str_split($splittedInput[$j]);

                $differentCount = 0;
                $newWord        = '';
                foreach ($word1 as $k => $singleWord1) {
                    if ($singleWord1 !== $word2[$k]) {
                        ++$differentCount;
                    } else {
                        $newWord .= $singleWord1;
                    }
                }
                if ($differentCount === 1) {
                    return $newWord;
                }
            }
        }

        throw new \RuntimeException('aaii');
    }
}
