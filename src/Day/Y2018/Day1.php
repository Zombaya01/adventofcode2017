<?php

namespace App\Day\Y2018;

use App\Day\AbstractDay;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day1 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $splittedInput = mb_strpos((string) $puzzleInput,"\n") !== false ? explode("\n",(string) $puzzleInput) : explode(', ',(string) $puzzleInput);

        $total = 0;
        foreach ($splittedInput as $i => $value) {
            $total += (int) $value;
        }

        return $total;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $splittedInput = mb_strpos((string) $puzzleInput,"\n") !== false ? explode("\n",(string) $puzzleInput) : explode(', ',(string) $puzzleInput);

        $total = 0;
        $seen  = [0];
        for ($i = 0; $i < 1000; ++$i) {
            foreach ($splittedInput as $value) {
                $total += (int) $value;

                if (in_array($total,$seen,true)) {
                    return $total;
                }

                $seen[] = $total;
            }
        }

        return -1;
    }
}
