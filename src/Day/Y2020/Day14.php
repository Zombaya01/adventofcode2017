<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day14 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfStrings($puzzleInput);

        $buffer = [];
        $mask   = '';

        foreach ($parsedInput as $line) {
            if (preg_match('/^mask = (?<mask>[X01]{36})$/',$line,$matches)) {
                $mask = $matches['mask'];
            } elseif (preg_match('/^mem\[(?<reg>\d+)\] = (?<value>\d+)$/',$line,$matches)) {
                $buffer[$matches['reg']] = self::applyMask((int) $matches['value'],$mask);
            } else {
                throw new \RuntimeException(sprintf('Line "%s" not matched',$line));
            }
        }

        return array_sum($buffer);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfStrings($puzzleInput);

        $buffer = [];
        $mask   = '';

        foreach ($parsedInput as $line) {
            if (preg_match('/^mask = (?<mask>[X01]{36})$/',$line,$matches)) {
                $mask = $matches['mask'];
            } elseif (preg_match('/^mem\[(?<reg>\d+)\] = (?<value>\d+)$/',$line,$matches)) {
                $addresses = self::getRegisterAddresses((int) $matches['reg'],$mask);
                foreach ($addresses as $reg) {
                    $buffer[$reg] = (int) $matches['value'];
                }
            } else {
                throw new \RuntimeException(sprintf('Line "%s" not matched',$line));
            }
        }

        return array_sum($buffer);
    }

    public static function applyMask(int $input, string $mask): int
    {
        $input &= bindec(str_replace('X','1',$mask));
        $input |= bindec(str_replace('X','0',$mask));

        return $input | bindec(str_replace('X','0',$mask));
    }

    /**
     * @return float[]|int[]
     */
    public static function getRegisterAddresses(int $input, string $mask): array
    {
        $convertedInput = str_pad(decbin($input),mb_strlen($mask),'0',STR_PAD_LEFT);

        $xIndexes = [];
        for ($i=0, $iMax = mb_strlen($mask); $i < $iMax; ++$i) {
            if ($mask[$i] === 'X') {
                $convertedInput[$i] = 'X';
                $xIndexes[]         = $i;
            } elseif ($mask[$i] === '1') {
                $convertedInput[$i] = '1';
            }
        }

        $total = mb_substr_count($convertedInput,'X');

        $result = [];
        for ($i = 0, $iMax = 1 << $total; $i < $iMax; ++$i) {
            $maskValue = str_pad(decbin($i),$total,'0',STR_PAD_LEFT);

            $tempValue = $convertedInput;
            for ($j = 0, $jMax = $total; $j < $jMax; ++$j) {
                $tempValue[$xIndexes[$j]] = $maskValue[$j];
            }
            $result[] = bindec($tempValue);
        }

        return $result;
    }
}
