<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Exception\NoSolutionFoundException;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day16 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        [$rules,$yours,$theirs] = self::parseInput($puzzleInput);
        $rules                  = array_values($rules);

        $mismatches   = [];
        foreach ($theirs as $ticket) {
            foreach ($ticket as $index => $value) {
                foreach ($rules as $rule) {
                    if ($value >= $rule[0] && $value <= $rule[1]) {
                        continue 2;
                    }
                    if ($value < $rule[2]) {
                        continue;
                    }
                    if ($value > $rule[3]) {
                        continue;
                    }
                    continue 2;
                }
                $mismatches[] = $value;
            }
        }

        return array_sum($mismatches);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        [$rules,$yours,$theirs] = self::parseInput($puzzleInput);
        $theirs                 = self::filterInvalid($rules, $theirs);

        $numberOfColumns = count($rules);
        $names           = array_keys($rules);
        $rulesByIndex    = array_values($rules);

        $matchedIndexes = [];
        $hasFound       = false;

        $validTickets = [$yours, ...$theirs];

        $possibleMatches = [];

        foreach ($rulesByIndex as $index => $rule) {
            for ($i=0; $i<$numberOfColumns; ++$i) {
                foreach ($validTickets as $validTicket) {
                    $value = $validTicket[$i];
                    if ($value >= $rule[0] && $value <= $rule[1]) {
                        continue;
                    }
                    if ($value >= $rule[2] && $value <= $rule[3]) {
                        continue;
                    }
                    continue 2;
                }
                $possibleMatches[$names[$index]][] = $i;
            }
        }

        do {
            $foundSomething = false;
            foreach ($possibleMatches as $name => $possibleMatchOptions) {
                if (count($possibleMatchOptions) === 1) {
                    $foundSomething        = true;
                    $matchedIndexes[$name] = array_shift($possibleMatchOptions);

                    // Remove itself from possible matches
                    unset($possibleMatches[$name]);

                    // Strip from other possibleMatches
                    foreach ($possibleMatches as $index => $possibleMatch) {
                        $possibleMatches[$index] = array_filter($possibleMatch, static fn ($val): bool => $val !== $matchedIndexes[$name]);
                    }
                }
            }

            if (!$foundSomething) {
                throw new NoSolutionFoundException();
            }
        } while (count($matchedIndexes) !== $numberOfColumns);

        $columnsToBeUsed = array_filter($matchedIndexes,static fn ($key): bool => str_contains((string) $key,'departure'), ARRAY_FILTER_USE_KEY);

        $product = 1;
        foreach ($columnsToBeUsed as $column) {
            $product *= $yours[$column];
        }

        return $product;
    }

    /**
     * @return array<int, array>
     */
    public static function parseInput(string $input): array
    {
        $inputPass1 = DataConverter::columnOfStringsSplitByBlankLine($input);

        $rules = [];
        foreach (DataConverter::columnOfStrings($inputPass1[0]) as $rule) {
            preg_match('/^(?<name>[\w ]+): (?<n1>\d+)-(?<n2>\d+) or (?<n3>\d+)-(?<n4>\d+)$/',$rule,$matches);
            $rules[$matches['name']] = [$matches['n1'], $matches['n2'], $matches['n3'], $matches['n4']];
        }

        $yours = array_map('intval',explode(',',DataConverter::columnOfStrings($inputPass1[1])[1]));

        $inputPass2 = DataConverter::columnOfStrings($inputPass1[2]);
        array_shift($inputPass2);

        $theirs = array_map(static fn ($row): array => array_map('intval',explode(',',$row)),$inputPass2);

        return [$rules, $yours, $theirs];
    }

    /**
     * @return mixed[]
     */
    public static function filterInvalid(array $rules, array $tickets): array
    {
        return array_filter($tickets,static fn ($ticket): bool => self::isValid($rules,$ticket));
    }

    private static function isValid(array $rules, array $ticket): bool
    {
        $rules = array_values($rules);
        foreach ($ticket as $value) {
            foreach ($rules as $rule) {
                if ($value >= $rule[0] && $value <= $rule[1]) {
                    continue 2;
                }
                if ($value < $rule[2]) {
                    continue;
                }
                if ($value > $rule[3]) {
                    continue;
                }
                continue 2;
            }

            return false;
        }

        return true;
    }
}
