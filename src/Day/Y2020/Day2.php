<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day2 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfStrings($puzzleInput);

        return array_reduce($parsedInput, fn ($carry, $row): float|int => $carry + (self::validPassword($row) ? 1 : 0),0);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfStrings($puzzleInput);

        return array_reduce($parsedInput, fn ($carry, $row): float|int => $carry + (self::secondValidPassword($row) ? 1 : 0),0);
    }

    public static function validPassword(string $password): bool
    {
        preg_match('/(?<min>\d+)-(?<max>\d+) (?<letter>\w): (?<input>\w+)/',$password,$matches);
        $count = mb_substr_count($matches['input'],$matches['letter']);

        return $count >= (int) $matches['min'] && $count <= (int) $matches['max'];
    }

    public static function secondValidPassword(string $password): bool
    {
        preg_match('/(?<min>\d+)-(?<max>\d+) (?<letter>\w): (?<input>\w+)/',$password,$matches);

        $firstLetter  = $matches['input'][(int) $matches['min'] - 1];
        $secondLetter = $matches['input'][(int) $matches['max'] - 1];

        return ($firstLetter === $matches['letter']) xor ($secondLetter === $matches['letter']);
    }
}
