<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day10 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = [0, ...DataConverter::columnOfInt($puzzleInput)];
        sort($parsedInput);

        $diff1 = 0;
        $diff3 = 1;
        for ($i=1,$iMax = count($parsedInput); $i < $iMax; ++$i) {
            switch ($parsedInput[$i] - $parsedInput[$i-1]) {
                case 1:
                    $diff1++;
                    break;
                case 3:
                    $diff3++;
                    break;
            }
        }

        return $diff1 * $diff3;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = [-10, -10, 0, ...DataConverter::columnOfInt($puzzleInput)];
        sort($parsedInput);

        $paths = array_fill(0,count($parsedInput),1);
        $count = 0;
        for ($i=3,$iMax = count($parsedInput); $i < $iMax; ++$i) {
            $count = 0;
            for ($j=1; $j <= 3; ++$j) {
                if ($parsedInput[$i] - $parsedInput[$i-$j] <= 3) {
                    $count += $paths[$i-$j];
                }
            }
            $paths[$i] = $count;
        }

        return $count;
    }
}
