<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day1 extends AbstractDay
{
    private const REFERENCE = 2020;

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfInt($puzzleInput);

        $count = count($parsedInput);

        for ($i = 0; $i < $count; ++$i) {
            for ($j = $i + 1; $j < $count; ++$j) {
                if ($parsedInput[$i] + $parsedInput[$j] === self::REFERENCE) {
                    return $parsedInput[$i] * $parsedInput[$j];
                }
            }
        }

        return 0;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfInt($puzzleInput);
        $count       = count($parsedInput);

        for ($i = 0; $i < $count; ++$i) {
            for ($j = $i + 1; $j < $count; ++$j) {
                for ($k = $j + 1; $k < $count; ++$k) {
                    if ($parsedInput[$i] + $parsedInput[$j] + $parsedInput[$k] === self::REFERENCE) {
                        return $parsedInput[$i] * $parsedInput[$j] * $parsedInput[$k];
                    }
                }
            }
        }

        return 0;
    }
}
