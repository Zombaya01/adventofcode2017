<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day8 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $codes = $this->parseInput($puzzleInput);

        try {
            return $this->executeCode($codes);
        } catch (LoopDetectionException $e) {
            return $e->getAcc();
        }
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $codes = $this->parseInput($puzzleInput);

        foreach ($codes as $i => $code) {
            $clone = $codes;
            try {
                if ($code['op'] === 'nop') {
                    $clone[$i]['op'] = 'jmp';
                } elseif ($code['op'] === 'jmp') {
                    $clone[$i]['op'] = 'nop';
                } else {
                    continue;
                }

                return $this->executeCode($clone);
            } catch (LoopDetectionException) {
                // as expected
            }
        }

        throw new \RuntimeException('No solution found');
    }

    private function parseInput(string $input): array
    {
        $partiallyParsedInput = DataConverter::columnOfStrings($input);

        return array_map(static function ($line): array {
            preg_match('/^(?<op>\w+) (?<number>[+-]\d+)$/',$line,$matches);

            return [
                'op'     => $matches['op'],
                'number' => (int) $matches['number'],
            ];
        },$partiallyParsedInput);
    }

    private function executeCode(array $codes): int
    {
        $acc        = 0;
        $index      = 0;
        $visited    = [];
        $totalSteps = count($codes);

        for ($stepCounter=0; $stepCounter < $totalSteps; ++$stepCounter) {
            if ($index === $totalSteps) {
                return $acc;
            }
            if (!array_key_exists($index,$codes)) {
                throw new \RuntimeException(sprintf('Step %d in program of %d steps could not be found',$index,$totalSteps));
            }
            if (array_key_exists($index,$visited)) {
                throw new LoopDetectionException($index,$totalSteps,$acc);
            }

            $visited[$index] = true;

            switch ($codes[$index]['op']) {
                case 'nop':
                    $index++;
                    break;
                case 'acc':
                    $acc += $codes[$index]['number'];
                    ++$index;
                    break;
                case 'jmp':
                    $index += $codes[$index]['number'];
                    break;
            }
        }

        throw new \RuntimeException('got outside of loop');
    }
}

class LoopDetectionException extends \Exception
{
    public function __construct(int $step, int $totalSteps, private readonly int $acc)
    {
        $message   = sprintf('Loop detected at step %d in program of %d steps',$step,$totalSteps);
        parent::__construct($message);
    }

    public function getAcc(): int
    {
        return $this->acc;
    }
}
