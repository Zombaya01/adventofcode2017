<?php

namespace App\Day\Y2020\Day7;

class Child
{
    public function __construct(private readonly Bag $bag, private readonly int $amount)
    {
    }

    public function getBag(): Bag
    {
        return $this->bag;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getCost(): int|float
    {
        return $this->amount * $this->bag->getChildrenCost();
    }
}
