<?php

namespace App\Day\Y2020\Day7;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Bag
{
    /** @var Collection<Bag> */
    private readonly Collection $parents;

    /** @var Collection<Bag> */
    private readonly Collection $children;

    public function __construct(public readonly string $color)
    {
        $this->parents  = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    public function addChild(Bag $child, int $number): void
    {
        $this->children->add(new Child($child,$number));
        $child->addParent($this);
    }

    public function addParent(Bag $parent): void
    {
        $this->parents->add($parent);
    }

    public function getChildrenCost(): int
    {
        return 1 + array_sum($this->children->map(fn (Child $child): int => $child->getCost())->toArray());
    }

    /**
     * @return ArrayCollection<int|string,Bag>
     */
    public function getAllParents(): ArrayCollection
    {
        $allParents = new ArrayCollection();

        foreach ($this->parents as $parent) {
            $allParents->add($parent);
            foreach ($parent->getAllParents() as $grandParent) {
                if (!$allParents->contains($grandParent)) {
                    $allParents->add($grandParent);
                }
            }
        }

        return $allParents;
    }
}
