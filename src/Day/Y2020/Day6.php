<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day6 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsed = DataConverter::columnOfStringsSplitByBlankLine($puzzleInput);

        $scores = [];
        foreach ($parsed as $group) {
            $score = 0;
            for ($l = 'a'; $l !== 'aa'; ++$l) {
                if (mb_strpos($group,$l) !== false) {
                    ++$score;
                }
            }
            $scores[] = $score;
        }

        return array_sum($scores);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsed = DataConverter::columnOfStringsSplitByBlankLine($puzzleInput);

        $scores = [];
        foreach ($parsed as $group) {
            $score      = 0;
            $answers    = explode("\n",$group);
            $groupCount = count($answers);
            for ($l = 'a'; $l !== 'aa'; ++$l) {
                $answerCount = array_reduce($answers,static fn ($carry,$answer): float|int => $carry + (mb_strpos((string) $answer,$l) !== false ? 1 : 0), 0);
                if ($answerCount === $groupCount) {
                    ++$score;
                }
            }
            $scores[] = $score;
        }

        return array_sum($scores);
    }
}
