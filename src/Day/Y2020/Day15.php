<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day15 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::rowOfInt($puzzleInput,',');

        return $this->calculate($parsedInput, 2020);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::rowOfInt($puzzleInput,',');

        return $this->calculate($parsedInput, 30_000_000);
    }

    public static function getNextNumber(array $input): int
    {
        $last = $input[array_key_last($input)];
        for ($i=0,$iMax = count($input)-2; $i<=$iMax; ++$i) {
            if ($input[$iMax - $i] === $last) {
                return $i + 1;
            }
        }

        return 0;
    }

    /**
     * @return array<int|string, int|null>
     */
    public static function toIndexes(array $input, int $size): array
    {
        $indexes = array_fill(0,$size,null);

        for ($i = 0, $iMax = count($input) - 1; $i<$iMax; ++$i) {
            $indexes[$input[$i]] = $i;
        }

        return $indexes;
    }

    /**
     * @param int[] $parsedInput
     */
    private function calculate(array $parsedInput, int $place): int
    {
        $indexes = self::toIndexes($parsedInput, $place);

        $nextValue = $parsedInput[array_key_last($parsedInput)];
        for ($i = count($parsedInput); $i < $place; ++$i) {
            $lastValue           = $nextValue;
            $currentIndex        = $i                                            - 1;
            $nextValue           = $indexes[$lastValue] !== null ? $currentIndex - $indexes[$lastValue] : 0;
            $indexes[$lastValue] = $currentIndex;
        }

        return $nextValue;
    }
}
