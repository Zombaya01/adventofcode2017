<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\ConsoleSectionOutput;
use Symfony\Component\Console\Output\OutputInterface;

class Day11 extends AbstractDay
{
    private const SLEEP = 300;

    private const EMPTY    = 'L';
    private const FLOOR    = '.';
    private const OCCUPIED = '#';

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = $this->parseInput($puzzleInput);

        $nextHash  = '';
        $nextFrame = $parsedInput;
        $i         = 0;

        do {
            $previousFrame = $nextFrame;
            $previousHash  = $nextHash;
            $nextFrame     = $this->calculateNextFrame($previousFrame);
            $nextHash      = $this->hashFrame($nextFrame);

            if ($i % 2 === 0 && $consoleOutput instanceof ConsoleOutput && $consoleOutput->isVerbose()) {
                $section = $consoleOutput->section();
                $this->writeFrame($section,$nextFrame);
            }
            ++$i;
        } while ($previousHash !== $nextHash);

        return mb_substr_count($this->toString($nextFrame),self::OCCUPIED);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = $this->parseInput($puzzleInput);
        $nextHash    = '';
        $nextFrame   = $parsedInput;
        $i           = 0;

        do {
            $previousFrame = $nextFrame;
            $previousHash  = $nextHash;
            $nextFrame     = $this->calculatePartTwoNextFrame($previousFrame);
            $nextHash      = $this->hashFrame($nextFrame);

            if ($i % 2 === 0 && $consoleOutput instanceof ConsoleOutput && $consoleOutput->isVerbose()) {
                $section = $consoleOutput->section();
                $this->writeFrame($section,$nextFrame);
            }
            ++$i;
        } while ($previousHash !== $nextHash);

        return mb_substr_count($this->toString($nextFrame),self::OCCUPIED);
    }

    private function writeBuffer(ConsoleSectionOutput $output, array $bufferHistory): void
    {
        foreach ($bufferHistory as $i => $singleBufferHistory) {
            $this->writeFrame($output,$singleBufferHistory);
        }
        $output->clear();
    }

    private function writeFrame(ConsoleSectionOutput $output, array $frame): void
    {
        foreach ($frame as $line) {
            $combinedLine = implode('', $line);
            $outputLine   = str_replace(
                [self::FLOOR, self::EMPTY, self::OCCUPIED],
                [' ', '<fg=green>█</>', '<fg=red>█</>'],
                $combinedLine
            );

            $output->writeln($outputLine);
        }
        $output->writeln(sprintf('Hash: %s',$this->hashFrame($frame)));
        usleep(self::SLEEP * 1000);
        $output->clear();
    }

    /**
     * @return mixed[]
     */
    public function calculateNextFrame(array $previousFrame): array
    {
        $nextFrame = $previousFrame;
        for ($i=0, $iMax = count($previousFrame)-2; $i<$iMax; ++$i) {
            for ($j=0, $jMax = (is_countable($previousFrame[0]) ? count($previousFrame[0]) : 0)-2; $j<$jMax; ++$j) {
                $occupiedNeighbours = 0;
                $tests              = 0;
                for ($k = -1; $k <= 1; ++$k) {
                    for ($l = -1; $l <= 1; ++$l) {
                        if (!($k === 0 && $l === 0) && $previousFrame[$i + $k][$j + $l] === self::OCCUPIED) {
                            ++$occupiedNeighbours;
                        }
                    }
                }

                if ($previousFrame[$i][$j] === self::EMPTY && $occupiedNeighbours === 0) {
                    $nextFrame[$i][$j] = self::OCCUPIED;
                } elseif ($previousFrame[$i][$j] === self::OCCUPIED && $occupiedNeighbours >= 4) {
                    $nextFrame[$i][$j] = self::EMPTY;
                } else {
                    $nextFrame[$i][$j] = $previousFrame[$i][$j];
                }
            }
        }

        return $nextFrame;
    }

    /**
     * @return mixed[]
     */
    public function calculatePartTwoNextFrame(array $previousFrame): array
    {
        $nextFrame = $previousFrame;
        for ($i=0, $iMax = count($previousFrame)-2; $i<$iMax; ++$i) {
            for ($j=0, $jMax = (is_countable($previousFrame[0]) ? count($previousFrame[0]) : 0)-2,$zMax=max($iMax,$jMax); $j<$jMax; ++$j) {
                $occupiedNeighbours = 0;
                for ($k = -1; $k <= 1; ++$k) {
                    for ($l = -1; $l <= 1; ++$l) {
                        if ($k === 0 && $l === 0) {
                            continue;
                        }

                        for ($z=1; $z < $zMax; ++$z) {
                            $tx = $i + $z*$k;
                            $ty = $j + $z*$l;
                            if ($tx < 0) {
                                continue 2;
                            }
                            if ($tx >= $iMax) {
                                continue 2;
                            }
                            if ($ty < 0) {
                                continue 2;
                            }
                            if ($ty >= $jMax) {
                                continue 2;
                            }

                            if ($previousFrame[$tx][$ty] === self::OCCUPIED) {
                                ++$occupiedNeighbours;
                                continue 2;
                            }
                            if ($previousFrame[$tx][$ty] === self::EMPTY) {
                                continue 2;
                            }
                        }
                    }
                }

                if ($previousFrame[$i][$j] === self::EMPTY && $occupiedNeighbours === 0) {
                    $nextFrame[$i][$j] = self::OCCUPIED;
                } elseif ($previousFrame[$i][$j] === self::OCCUPIED && $occupiedNeighbours >= 5) {
                    $nextFrame[$i][$j] = self::EMPTY;
                } else {
                    $nextFrame[$i][$j] = $previousFrame[$i][$j];
                }
            }
        }

        return $nextFrame;
    }

    public function toString(array $frame): string
    {
        return implode("\n",array_slice(array_map(static fn ($row): string => mb_substr(implode('',$row),1,-1),$frame),1,-1));
    }

    private function hashFrame(array $frame): string
    {
        return sha1($this->toString($frame));
    }

    public function parseInput(string $input): array
    {
        $result      = [];
        $parsedInput = DataConverter::tableOfStrings($input,'');
        $x           = count($parsedInput);
        $y           = count($parsedInput[0]);

        for ($i=-1; $i <= $x; ++$i) {
            for ($j=-1; $j <= $y; ++$j) {
                $result[$i][$j] = $parsedInput[$i][$j] ?? self::FLOOR;
            }
        }

        return $result;
    }
}
