<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day12 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfStrings($puzzleInput);

        $x   = 0;
        $y   = 0;
        $dir = 0;

        $movements = [
            [1, 0],
            [0, -1],
            [-1, 0],
            [0, 1],
        ];

        foreach ($parsedInput as $line) {
            preg_match('/^(?<letter>\w)(?<number>\d+)$/',$line,$matches);
            ['letter'=> $letter,'number'=> $number] = $matches;
            $number                                 = (int) $number;

            switch ($letter) {
                case 'N':
                    $y += $number;
                    break;
                case 'S':
                    $y -= $number;
                    break;
                case 'E':
                    $x += $number;
                    break;
                case 'W':
                    $x -= $number;
                    break;
                case 'L':
                    $dir = ($dir - $number/90 + 4) % 4;
                    break;
                case 'R':
                    $dir = ($dir + $number/90) % 4;
                    break;
                case 'F':
                    $x += $movements[$dir][0]*$number;
                    $y += $movements[$dir][1]*$number;
                    break;
            }
        }

        return abs($x)+abs($y);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfStrings($puzzleInput);

        $wx  = 10;
        $wy  = 1;
        $x   = 0;
        $y   = 0;
        $dir = 0;

        $movements = [
            [1, 0],
            [0, -1],
            [-1, 0],
            [0, 1],
        ];

        foreach ($parsedInput as $line) {
            preg_match('/^(?<letter>\w)(?<number>\d+)$/',$line,$matches);
            ['letter'=> $letter,'number'=>$number] = $matches;
            $number                                = (int) $number;

            switch ($letter) {
                case 'N':
                    $wy += $number;
                    break;
                case 'S':
                    $wy -= $number;
                    break;
                case 'E':
                    $wx += $number;
                    break;
                case 'W':
                    $wx -= $number;
                    break;
                case 'L':
                    [$wx,$wy] = self::turn($wx,$wy,-$number);
                    break;
                case 'R':
                    [$wx,$wy] = self::turn($wx,$wy,$number);
                    break;
                case 'F':
                    $x += $number*$wx;
                    $y += $number*$wy;
                    break;
            }
        }

        return abs($x)+abs($y);
    }

    private function distanceToWaypoint(int $x, int $wx): int
    {
        return abs($wx - $x);
    }

    public static function turn(int $x, int $y, int $degrees): array
    {
        $twist = ($degrees/90 + 4) % 4;

        return match ($twist) {
            1       => [$y, -$x],
            2       => [-$x, -$y],
            3       => [-$y, $x],
            default => [$x, $y],
        };
    }
}
