<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day3 extends AbstractDay
{
    final public const TREE = '#';
    final public const SNOW = '.';

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return $this->calculateTrees($puzzleInput,1,3);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return
            $this->calculateTrees($puzzleInput, 1, 1)
            * $this->calculateTrees($puzzleInput, 1, 3)
            * $this->calculateTrees($puzzleInput, 1, 5)
            * $this->calculateTrees($puzzleInput, 1, 7)
            * $this->calculateTrees($puzzleInput, 2, 1);
    }

    public function calculateTrees(string $input, int $down, int $right): int
    {
        $parsedInput = DataConverter::tableOfStrings($input,'');

        $count    = 0;
        $rowCount = count($parsedInput[0]);
        for ($i = 0, $x = 0, $xMax = count($parsedInput); $x < $xMax; $i++, $x = $i*$down) {
            $y = ($i*$right) % $rowCount;
            if ($parsedInput[$x][$y] === self::TREE) {
                ++$count;
            }
        }

        return $count;
    }
}
