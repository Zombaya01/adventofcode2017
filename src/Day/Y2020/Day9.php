<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Exception\NoSolutionFoundException;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day9 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $inputArray = DataConverter::columnOfInt($puzzleInput);

        try {
            $isValid = Day9::validateArray($inputArray,25);
        } catch (InvalidEntryException $e) {
            return $e->getValue();
        }

        throw new NoSolutionFoundException();
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $inputArray = DataConverter::columnOfInt($puzzleInput);

        try {
            $isValid = Day9::validateArray($inputArray,25);
        } catch (InvalidEntryException $e) {
            return $e->getFirstValue() + $e->getLastValue();
        }

        throw new NoSolutionFoundException();
    }

    public static function validateArray(array $input, int $windowSize): bool
    {
        for ($i=$windowSize,$iMax = count($input); $i < $iMax; ++$i) {
            for ($j=$i-$windowSize; $j < $i; ++$j) {
                for ($k = $j+1; $k < $i; ++$k) {
                    if ($input[$k] + $input[$j] === $input[$i]) {
                        continue 3;
                    }
                }
            }

            for ($j=0; $j < $i; ++$j) {
                for ($k = 1; $k < $i-$j; ++$k) {
                    $slice = array_slice($input,$j,$k);
                    if (array_sum($slice) === $input[$i]) {
                        throw new InvalidEntryException($i, $input[$i],min($slice),max($slice));
                    }
                }
            }

            throw new InvalidEntryException($i, $input[$i],null,null);
        }

        return true;
    }
}

class InvalidEntryException extends \Exception
{
    public function __construct(private readonly int $index, private readonly int $value, private readonly ?int $firstValue, private readonly ?int $lastValue)
    {
        parent::__construct(sprintf('Invalid value %d found at index %d',$value,$index));
    }

    public function getIndex(): int
    {
        return $this->index;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getFirstValue(): ?int
    {
        return $this->firstValue;
    }

    public function getLastValue(): ?int
    {
        return $this->lastValue;
    }
}
