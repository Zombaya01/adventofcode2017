<?php

declare(strict_types=1);

namespace App\Day\Y2020\Day4;

class Passport
{
    private ?int $birthYear      = null;
    private ?int $issueYear      = null;
    private ?int $expirationYear = null;
    private ?Height $height      = null;
    private ?string $hairColor   = null;
    private ?string $eyeColor    = null;
    private ?string $passportId  = null;
    private ?int $countryId      = null;

    public function __construct(string $input)
    {
        $exploded = array_filter(explode(' ',str_replace(["\n", "\r"],' ',$input)), static fn ($val): bool => $val !== '');
        $parsed   = [];
        foreach ($exploded as $keyvalue) {
            [$key,$value] = explode(':',$keyvalue);
            $parsed[$key] = $value;
        }

        if (isset($parsed['byr'])) {
            $this->birthYear = (int) $parsed['byr'];
        }
        if (isset($parsed['iyr'])) {
            $this->issueYear = (int) $parsed['iyr'];
        }
        if (isset($parsed['eyr'])) {
            $this->expirationYear = (int) $parsed['eyr'];
        }
        if (isset($parsed['hgt'])) {
            $this->height = new Height($parsed['hgt']);
        }
        if (isset($parsed['hcl'])) {
            $this->hairColor =  $parsed['hcl'];
        }
        if (isset($parsed['ecl'])) {
            $this->eyeColor = $parsed['ecl'];
        }
        if (isset($parsed['pid'])) {
            $this->passportId = $parsed['pid'];
        }
        if (isset($parsed['cid'])) {
            $this->countryId = (int) $parsed['cid'];
        }
    }

    public function isValid(): bool
    {
        return $this->birthYear      !== null
            && $this->issueYear      !== null
            && $this->expirationYear !== null
            && $this->height instanceof Height
            && $this->hairColor      !== null
            && $this->eyeColor       !== null
            && $this->passportId     !== null
            && $this->countryId      !== null;
    }

    public function isValidWithoutCountry(): bool
    {
        return $this->birthYear      !== null
            && $this->issueYear      !== null
            && $this->expirationYear !== null
            && $this->height instanceof Height
            && $this->hairColor      !== null
            && $this->eyeColor       !== null
            && $this->passportId     !== null;
    }

    public function isValidPartTwo(): bool
    {
        return
            $this->isValidWithoutCountry()
            && $this->birthYear      >= 1920 && $this->birthYear      <= 2002
            && $this->issueYear      >= 2010 && $this->issueYear      <= 2020
            && $this->expirationYear >= 2020 && $this->expirationYear <= 2030
            && (($this->height->getType() === 'cm' && $this->height->getAmount() >= 150 && $this->height->getAmount() <= 193)
             || ($this->height->getType() === 'in' && $this->height->getAmount() >= 59  && $this->height->getAmount() <= 76)
            )
            && preg_match('/^#[0-9a-f]{6}$/',$this->hairColor)
            && in_array($this->eyeColor,['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'],true)
            && preg_match('/^\d{9}$/',$this->passportId);
    }
}

class Height
{
    private readonly int $amount;
    private readonly string $type;

    public function __construct(string $input)
    {
        preg_match('/(?<amount>\d+)(?<type>\w+)/',$input,$matches);
        $this->amount = (int) $matches['amount'];
        $this->type   = $matches['type'];
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}
