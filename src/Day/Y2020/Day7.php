<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Day\Y2020\Day7\Bag;
use App\Utils\DataConverter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day7 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $collection = self::parseInput($puzzleInput);

        return $collection->get('shiny gold')->getAllParents()->count();
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $collection = self::parseInput($puzzleInput);

        return $collection->get('shiny gold')->getChildrenCost() - 1;
    }

    /** @return Collection<Bag> */
    public static function parseInput(string $input): Collection
    {
        $lines = DataConverter::columnOfStrings($input);
        /** @var Collection<Bag> $collection */
        $collection = new ArrayCollection();
        foreach ($lines as $line) {
            self::parseLine($collection,$line);
        }

        return $collection;
    }

    public static function parseLine(Collection $collection, string $line): void
    {
        $line = rtrim($line,'.');
        // clear gray bags contain 3 mirrored olive bags, 3 clear crimson bags, 5 dark orange bags, 2 dim gold bags.
        [$parentString,$childrenString] = explode(' contain ',$line);

        $parentColor = self::stripColor($parentString);

        self::addColorToCollection($collection,$parentColor);

        if ($childrenString === 'no other bags') {
            return;
        }

        foreach (explode(', ',$childrenString) as $child) {
            preg_match('/^(?<number>\d+) (?<color>\w+ \w+) bags?/',$child,$matches);
            $color  = $matches['color'];
            $amount = $matches['number'];

            self::addColorToCollection($collection,$color);
            $collection->get($parentColor)->addChild($collection->get($color),(int) $amount);
        }
    }

    public static function addColorToCollection(Collection $collection, string $color): void
    {
        if (!$collection->containsKey($color)) {
            $collection->set($color,new Bag($color));
        }
    }

    private static function stripColor(string $input): string
    {
        return trim(preg_replace('/bags?$/','',trim($input)));
    }
}
