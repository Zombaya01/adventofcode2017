<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Exception\NoSolutionFoundException;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

class Day13 extends AbstractDay
{
    private const MAX = 1_000_000_000_000;

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        [$start,$lines] = self::parseInput($puzzleInput);

        $max      = PHP_INT_MAX;
        $bestLine = null;

        foreach ($lines as $line) {
            $diff = $line - ($start % $line);
            if ($diff < $max) {
                $max      = $diff;
                $bestLine = $line;
            }
        }

        return $bestLine * $max;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        // Implement chinese remainder theorem here apparently

        $section = $section2 = null;
        if ($consoleOutput instanceof OutputInterface && $consoleOutput instanceof ConsoleOutput) {
            $section  = $consoleOutput->section();
            $section2 = $consoleOutput->section();
        }

        /**
         * @var array $lines
         */
        [$start,$lines] = self::parseInput2($puzzleInput);

        if ($consoleOutput instanceof OutputInterface) {
            foreach ($lines as $line) {
                $consoleOutput->writeln(sprintf('(t + %d) mod %d = 0,',$line[0],$line[1]));
            }
        }

        [$index,$max] = array_shift($lines);

        $iStart = 1;
        if ($start === 1_001_171) {
            $iStart = floor(163_620_262_000 / $max);
        }

        for ($i = $iStart; $i < self::MAX; ++$i) {
            $timestamp = $i * $max;
            if ($section instanceof \Symfony\Component\Console\Output\ConsoleSectionOutput && $i % 1000 === 0) {
                if ($i % 2000 === 0) {
                    $section->overwrite(sprintf('%d: %d',$i,$timestamp));
                } else {
                    $section2->overwrite(sprintf('%d: %d',$i,$timestamp));
                }
            }

            foreach ($lines as $indexArray) {
                [$j,$val] = $indexArray;

                if (($timestamp + $j - $index) % $val !== 0) {
                    continue 2;
                }
            }

            return $timestamp - $index;
        }

        throw new NoSolutionFoundException();
    }

    /**
     * @return int[]|array<int, int[]>
     */
    public static function parseInput(string $input): array
    {
        $lines    = DataConverter::columnOfStrings($input);
        $result   = [(int) $lines[0]];
        $result[] = array_values(array_map('intval',array_filter(explode(',',$lines[1]),static fn ($val): bool => $val !== 'x')));

        return $result;
    }

    /**
     * @return int[]|array<int, array<int, int[]>>
     */
    public static function parseInput2(string $input): array
    {
        $lines    = DataConverter::columnOfStrings($input);
        $result   = [(int) $lines[0]];
        $indexes  = [];

        foreach (explode(',',$lines[1]) as $key => $value) {
            if ($value === 'x') {
                continue;
            }
            $indexes[] = [$key, (int) $value];
        }
        usort($indexes, static fn ($a,$b): int => $b[1] <=> $a[1]);
        $result[] = array_values($indexes);

        return $result;
    }
}
