<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Exception\NoSolutionFoundException;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day5 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return array_reduce(array_map($this->convertToId(...), DataConverter::columnOfStrings($puzzleInput)),fn ($carry, $number): mixed => max($carry,$number),0);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $filledSeats = array_map($this->convertToId(...), DataConverter::columnOfStrings($puzzleInput));
        sort($filledSeats);

        for ($i=1, $iMax=max($filledSeats)-1; $i<$iMax; ++$i) {
            if (in_array($i,$filledSeats,true)) {
                continue;
            }
            if (!in_array($i-1,$filledSeats,true)) {
                continue;
            }
            if (!in_array($i+1,$filledSeats,true)) {
                continue;
            }

            return $i;
        }

        throw new NoSolutionFoundException();
    }

    public static function convertToId(string $input): int
    {
        [$x,$y] = mb_str_split($input,7);

        $xi = $yi = 0;
        for ($i=0; $i<7; ++$i) {
            if ($x[$i] === 'B') {
                $xi |= 1 << (6-$i);
            }
        }

        for ($i=0; $i<3; ++$i) {
            if ($y[$i] === 'R') {
                $yi |= 1 << (2-$i);
            }
        }

        return ($xi * 8) + $yi;
    }
}
