<?php

declare(strict_types=1);

namespace App\Day\Y2020;

use App\Day\AbstractDay;
use App\Day\Y2020\Day4\Passport;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day4 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $passpords = $this->parseInput($puzzleInput);

        return array_reduce($passpords,fn ($carry,Passport $passport): float|int => $carry + ($passport->isValidWithoutCountry() ? 1 : 0),0);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $passpords = $this->parseInput($puzzleInput);

        return array_reduce($passpords,fn ($carry,Passport $passport): float|int => $carry + ($passport->isValidPartTwo() ? 1 : 0),0);
    }

    /**
     * @return Passport[]
     */
    private function parseInput(string $input): array
    {
        $lines  = explode("\n\n",$input);
        $result = [];
        foreach ($lines as $line) {
            $result[] = new Passport($line);
        }

        return $result;
    }
}
