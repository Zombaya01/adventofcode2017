<?php

declare(strict_types=1);

namespace App\Day\Y2024;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day1 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input = DataConverter::tableOfInt($puzzleInput);

        $firstColumn  = array_column($input, 0);
        $secondColumn = array_column($input, 1);

        sort($firstColumn);
        sort($secondColumn);

        $totalDistance = 0;
        foreach ($firstColumn as $key => $value) {
            $totalDistance += abs($value - $secondColumn[$key]);
        }

        return $totalDistance;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input = DataConverter::tableOfInt($puzzleInput);

        $firstColumn  = array_column($input, 0);
        $secondColumn = array_column($input, 1);

        $totalSimilarity = 0;
        foreach ($firstColumn as $key => $value) {
            $totalSimilarity += $value * count(array_filter($secondColumn, static fn ($v) => $v === $value));
        }

        return $totalSimilarity;
    }
}
