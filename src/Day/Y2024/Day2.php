<?php

declare(strict_types=1);

namespace App\Day\Y2024;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day2 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input = DataConverter::tableOfInt($puzzleInput);

        return count(array_filter(
            $input,
            $this->isSafeLevel1(...)
        ));
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input = DataConverter::tableOfInt($puzzleInput);

        return count(array_filter(
            $input,
            $this->isSafeLevel2(...)
        ));
    }

    /**
     * @param list<int> $row
     */
    private function isSafeLevel1(array $row): bool
    {
        if ($row[0] < $row[1]) {
            $minDiff = 1;
            $maxDiff = 3;
        } else {
            $minDiff = -3;
            $maxDiff = -1;
        }
        for ($i=0, $iMax = count($row) -1; $i < $iMax; ++$i) {
            $diff = $row[$i + 1] - $row[$i];
            if ($diff > $maxDiff || $diff < $minDiff) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param list<int> $row
     */
    private function isSafeLevel2(array $row): bool
    {
        if ($this->isSafeLevel1($row)) {
            return true;
        }

        for ($i=0, $iMax = count($row); $i<$iMax; ++$i) {
            $newRow = $row;
            unset($newRow[$i]);
            if ($this->isSafeLevel1(array_values($newRow))) {
                return true;
            }
        }

        return false;
    }
}
