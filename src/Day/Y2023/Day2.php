<?php

declare(strict_types=1);

namespace App\Day\Y2023;

use App\Day\AbstractDay;
use App\Utils\CollectionHelper;
use App\Utils\DataCollectionConverter;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day2 extends AbstractDay
{
    // 12 red cubes, 13 green cubes, and 14 blue cubes
    private const SET = [
        'red'   => 12,
        'green' => 13,
        'blue'  => 14,
    ];

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input = DataCollectionConverter::columnOfStrings($puzzleInput);

        // Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        // Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
        // Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
        // Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
        // Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green

        return $this->mapInput($input)
            ->filter(
                static function (array $row): bool {
                    foreach ($row['sets'] as $set) {
                        if ($set['red'] > self::SET['red']) {
                            return false;
                        }
                        if ($set['green'] > self::SET['green']) {
                            return false;
                        }
                        if ($set['blue'] > self::SET['blue']) {
                            return false;
                        }
                    }

                    return true;
                }
            )
            ->map(
                static function (array $row): int {
                    return $row['game'];
                }
            )
            ->reduce(CollectionHelper::sum(...))
        ;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input = DataCollectionConverter::columnOfStrings($puzzleInput);

        // Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        // Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
        // Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
        // Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
        // Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green

        return $this->mapInput($input)
            ->map(
                function (array $row): int {
                    return max(...array_column($row['sets'],'red'))
                        * max(...array_column($row['sets'],'green'))
                        * max(...array_column($row['sets'],'blue'))
                    ;
                }
            )
            ->reduce(
                CollectionHelper::sum(...),
                0
            );
    }

    /**
     * @param Collection<int,string> $input
     *
     * @return Collection<int,array{game: int, sets: array<int,array{red: int, green: int, blue: int}>}>
     */
    protected function mapInput(
        Collection $input,
    ): Collection {
        return $input
            ->map(
                static function (string $row): array {
                    $regex = '/^Game (?<game>\d+): (?<rest>.*)$/';
                    preg_match($regex, $row, $matches);
                    $game = (int) ($matches['game'] ?? 0);

                    return [
                        'game' => $game,
                        'sets' => array_map(
                            static function (string $set): array {
                                $result = [];
                                preg_match('/(?<number>\d+) red/', $set, $matches);
                                $result['red'] = (int) ($matches['number'] ?? 0);
                                preg_match('/(?<number>\d+) green/', $set, $matches);
                                $result['green'] = (int) ($matches['number'] ?? 0);
                                preg_match('/(?<number>\d+) blue/', $set, $matches);
                                $result['blue'] = (int) ($matches['number'] ?? 0);

                                return $result;
                            },
                            isset($matches['rest']) ? explode(';', $matches['rest']) : []
                        ),
                    ];
                }
            );
    }
}
