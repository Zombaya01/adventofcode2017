<?php

declare(strict_types=1);

namespace App\Day\Y2023\Day5;

use App\Utils\DataConverter;

class Day5Converter
{
    /**
     * @var list<array<int<0, 2>,positive-int>>
     */
    private array $ranges;

    public function __construct(string $input)
    {
        $rows = DataConverter::columnOfStrings($input);

        foreach ($rows as $row) {
            if (!is_numeric($row[0])) {
                continue;
            }

            // @phpstan-ignore assign.propertyType (assume it is valid)
            $this->ranges[] = sscanf($row, '%d %d %d');
        }
    }

    public function transform(int $value): int
    {
        foreach ($this->ranges as $range) {
            [$destinationStart, $sourceStart, $length] = $range;

            if ($value >= $sourceStart && $value < $sourceStart + $length) {
                return $destinationStart + ($value - $sourceStart);
            }
        }

        return $value;
    }

    /**
     * @param list<array{0:int, 1:int}> $inputRanges
     *
     * @return list<array{0:int, 1:int}>
     */
    public function transformRanges(array $inputRanges): array
    {
        $output = [];

        while ($inputRanges !== []) {
            $inputRange = array_shift($inputRanges);

            [$inputStart,$inputLength] = $inputRange;

            foreach ($this->ranges as $range) {
                [$destinationStart, $sourceStart, $length] = $range;

                // input is before given range
                if ($inputStart < $sourceStart) {
                    continue;
                }

                // Input is after given range
                if ($inputStart > $sourceStart + $length) {
                    continue;
                }

                // Input is completely in range
                if ($inputStart + $inputLength <= $sourceStart + $length) {
                    $output[] = [$destinationStart + ($inputStart - $sourceStart), $inputLength];
                    continue 2;
                }

                // Input is partly in range
                if ($inputStart < $sourceStart + $length) {
                    $lengthOfPart  = ($sourceStart + $length) - $inputStart;
                    $output[]      = [$destinationStart + ($inputStart - $sourceStart), $lengthOfPart];
                    $inputRanges[] = [$inputStart + $lengthOfPart, $inputLength - $lengthOfPart];
                    continue 2;
                }
            }

            $output[] = $inputRange;
        }

        return $output;
    }
}
