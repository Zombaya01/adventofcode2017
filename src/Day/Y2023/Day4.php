<?php

declare(strict_types=1);

namespace App\Day\Y2023;

use App\Day\AbstractDay;
use App\Utils\CollectionHelper;
use App\Utils\DataCollectionConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day4 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return DataCollectionConverter::columnOfStrings($puzzleInput)
            ->map(self::toClass(...))
            ->map(fn (object $row): int => $row->getDay1Score())
            ->reduce(CollectionHelper::sum(...))
        ;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $startCollection = DataCollectionConverter::columnOfStrings($puzzleInput)
            ->map(self::toClass(...))
        ;

        $total           = 0;
        $duplicateBuffer = [];

        foreach ($startCollection as $row) {
            $currentBufferAdd = array_shift($duplicateBuffer) ?? 0;
            $duplicateBuffer  = array_values($duplicateBuffer); // Make it a proper list again

            // Add this card-count to the total plus the additional
            $thisCardCount = $currentBufferAdd + 1;

            $total += $thisCardCount;

            // Calculate the buffer
            $numberOfCards = $row->getDay2Score();
            for ($i=0; $i < $numberOfCards; ++$i) {
                if (!isset($duplicateBuffer[$i])) {
                    $duplicateBuffer[$i] = $thisCardCount;
                } else {
                    $duplicateBuffer[$i] += $thisCardCount;
                }
            }
        }

        return $total;
    }

    public static function toClass(string $row): Day4Card
    {
        return new Day4Card($row);
    }
}

class Day4Card
{
    /** @var list<int> */
    private array $winning;
    /** @var list<int> */
    private array $available;

    public function __construct(string $input)
    {
        preg_match('/^Card +(?<id>\d+): (?<winning>.*?) \| (?<available>.*)$/', $input, $matches);

        if (!isset($matches['available'],$matches['winning'])) {
            throw new \InvalidArgumentException();
        }

        $this->available = array_values(array_filter(array_map(intval(...),explode(' ',trim($matches['available'])))));
        $this->winning   = array_values(array_filter(array_map(intval(...),explode(' ',trim($matches['winning'])))));
    }

    public function getDay1Score(): int
    {
        $total = 0;
        foreach ($this->winning as $win) {
            foreach ($this->available as $a) {
                if ($win === $a) {
                    if ($total === 0) {
                        ++$total;
                    } else {
                        $total *= 2;
                    }
                }
            }
        }

        return $total;
    }

    public function getDay2Score(): int
    {
        $total = 0;
        foreach ($this->winning as $win) {
            foreach ($this->available as $a) {
                if ($win === $a) {
                    ++$total;
                }
            }
        }

        return $total;
    }
}
