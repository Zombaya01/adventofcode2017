<?php

declare(strict_types=1);

namespace App\Day\Y2023;

use App\Day\AbstractDay;
use App\Day\Y2023\Day5\Day5Converter;
use App\Utils\CollectionHelper;
use App\Utils\DataCollectionConverter;
use App\Utils\DataConverter;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day5 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parts = DataConverter::columnOfStrings($puzzleInput, "\n\n");

        $rawSeeds   = array_shift($parts);
        $converters = (new ArrayCollection($parts))
            ->map(fn (string $row) => new Day5Converter($row))
        ;

        preg_match('/^seeds: (?<values>.*)$/i', $rawSeeds, $matches);
        if (!isset($matches['values'])) {
            throw new \InvalidArgumentException();
        }
        $seedValues = DataCollectionConverter::rowOfInt(trim($matches['values']));

        return $seedValues
            ->map(
                fn (int $seedValue) => $converters->reduce(
                    fn (int $carry, Day5Converter $value): int => $value->transform($carry),
                    $seedValue
                )
            )
            ->reduce(CollectionHelper::min(...))
        ;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parts = DataConverter::columnOfStrings($puzzleInput, "\n\n");

        $rawSeeds   = array_shift($parts);
        $converters = (new ArrayCollection($parts))
            ->map(fn (string $row) => new Day5Converter($row))
        ;

        preg_match('/^seeds: (?<values>.*)$/i', $rawSeeds, $matches);
        if (!isset($matches['values'])) {
            throw new \InvalidArgumentException();
        }
        /** @var ArrayCollection<int,array{int,int}> $seedValueRanges */
        $seedValueRanges = new ArrayCollection(array_chunk(
            DataConverter::rowOfInt(trim($matches['values'])),
            2
        ));

        $allRanges = $seedValueRanges
            ->map(
                fn (array $range) => $converters->reduce(
                    fn (array $carry, Day5Converter $c) => $c->transformRanges($carry),
                    [$range]
                )
            )
            ->toArray();

        $allRangesMerged = array_merge(...$allRanges);
        $startValues     = array_column($allRangesMerged, 0);

        return min(...$startValues);
    }
}
