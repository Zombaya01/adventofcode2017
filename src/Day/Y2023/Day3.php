<?php

declare(strict_types=1);

namespace App\Day\Y2023;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day3 extends AbstractDay
{
    private int $xLen = 0;
    private int $yLen = 0;

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        // $input[$y][$x]
        $input = DataConverter::tableOfStrings($puzzleInput, '');

        $this->yLen = count($input);
        $this->xLen = count($input[0]);

        $numbers = [];

        for ($j = 0; $j < $this->yLen; ++$j) {
            $xMin = null;
            $xMax = null;
            for ($i = 0; $i < $this->xLen; ++$i) {
                $char = $input[$j][$i];
                if (is_numeric($char)) {
                    if ($xMin === null) {
                        $xMin = $i;
                    }
                } else {
                    if ($xMin !== null) {
                        $xMax = $i - 1;
                        if ($this->doesNumberTouch($input, $j, $xMin, $xMax)) {
                            $numbers[] = (int) substr(implode('',$input[$j]), $xMin, $xMax - $xMin + 1);
                        }
                        $xMax = null;
                        $xMin = null;
                    }
                }
            }

            if ($xMin !== null) {
                $xMax = $this->xLen;
                if ($this->doesNumberTouch($input, $j, $xMin, $xMax)) {
                    $numbers[] = (int) substr(implode('',$input[$j]), $xMin, $xMax - $xMin + 1);
                }
            }
        }

        return array_sum($numbers);
    }

    /**
     * @param array<int,array<int,string>> $input
     */
    public function doesNumberTouch(array $input, int $y, int $minX, int $maxX): bool
    {
        for ($j = max(0, $y-1); $j < min($this->yLen, $y+2); ++$j) {
            for ($i = max(0,$minX-1); $i < min($this->xLen, $maxX + 2); ++$i) {
                $char = $input[$j][$i];

                if (!is_numeric($char) && $char !== '.') {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param array<int,array<int,string>> $input
     */
    public function getTouchingGear(array $input, int $y, int $minX, int $maxX): ?string
    {
        for ($j = max(0, $y-1); $j < min($this->yLen, $y+2); ++$j) {
            for ($i = max(0,$minX-1); $i < min($this->xLen, $maxX + 2); ++$i) {
                $char = $input[$j][$i];

                if (!is_numeric($char) && $char !== '.') {
                    return "{$j}-{$i}";
                }
            }
        }

        return null;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        // $input[$y][$x]
        $input = DataConverter::tableOfStrings($puzzleInput, '');

        $this->yLen = count($input);
        $this->xLen = count($input[0]);

        $touchingGears = [];

        for ($j = 0; $j < $this->yLen; ++$j) {
            $xMin = null;
            $xMax = null;
            for ($i = 0; $i < $this->xLen; ++$i) {
                $char = $input[$j][$i];
                if (is_numeric($char)) {
                    if ($xMin === null) {
                        $xMin = $i;
                    }
                } else {
                    if ($xMin !== null) {
                        $xMax = $i - 1;

                        $key = $this->getTouchingGear($input, $j, $xMin, $xMax);
                        if ($key !== null) {
                            $touchingGears[$key][] = (int) substr(implode('',$input[$j]), $xMin, $xMax - $xMin + 1);
                        }
                        $xMax = null;
                        $xMin = null;
                    }
                }
            }

            if ($xMin !== null) {
                $xMax = $this->xLen;

                $key = $this->getTouchingGear($input, $j, $xMin, $xMax);
                if ($key !== null) {
                    $touchingGears[$key][] = (int) substr(implode('',$input[$j]), $xMin, $xMax - $xMin + 1);
                }
            }
        }

        $sums = 0;
        foreach ($touchingGears as $numbers) {
            if (count($numbers) === 2) {
                $sums+= $numbers[0] * $numbers[1];
            }
        }

        return $sums;
    }
}
