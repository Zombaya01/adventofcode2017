<?php

declare(strict_types=1);

namespace App\Day\Y2023;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day1 extends AbstractDay
{
    private const day2_map = [
        '1'     => 1,
        '2'     => 2,
        '3'     => 3,
        '4'     => 4,
        '5'     => 5,
        '6'     => 6,
        '7'     => 7,
        '8'     => 8,
        '9'     => 9,
        'one'   => 1,
        'two'   => 2,
        'three' => 3,
        'four'  => 4,
        'five'  => 5,
        'six'   => 6,
        'seven' => 7,
        'eight' => 8,
        'nine'  => 9,
    ];

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return array_sum(
            array_map(
                static function (string $row): int {
                    $strlen = strlen($row);
                    for ($i = 0; $i < $strlen; ++$i) {
                        if (is_numeric($row[$i])) {
                            $first = $row[$i];
                            break;
                        }
                    }
                    for ($i = $strlen - 1; $i >= 0; --$i) {
                        if (is_numeric($row[$i])) {
                            $last = $row[$i];
                            break;
                        }
                    }

                    if (!isset($first, $last)) {
                        throw new \RuntimeException('Unable to calculate');
                    }

                    return (int) ($first.$last);
                },
                DataConverter::columnOfStrings($puzzleInput),
            )
        );
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        return array_sum(
            array_map(
                static function (string $row): int {
                    $row1 = $row;
                    while (!empty($row1)) {
                        foreach (self::day2_map as $needle => $value) {
                            if (str_starts_with($row1, (string) $needle)) {
                                $first = $value;
                                break 2;
                            }
                        }
                        $row1 = substr($row1, 1);
                    }

                    $row2 = $row;
                    while (!empty($row2)) {
                        foreach (self::day2_map as $needle => $value) {
                            if (str_ends_with($row2, (string) $needle)) {
                                $last = $value;
                                break 2;
                            }
                        }
                        $row2 = substr($row2,0, -1);
                    }

                    if (!isset($first, $last)) {
                        throw new \RuntimeException('Unable to calculate');
                    }

                    return (int) ($first.$last);
                },
                DataConverter::columnOfStrings($puzzleInput),
            )
        );
    }
}
