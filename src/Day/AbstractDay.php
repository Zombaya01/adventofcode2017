<?php

namespace App\Day;

use App\Exception\NotImplementedException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

abstract class AbstractDay
{
    /**
     * @throws NotImplementedException
     */
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        throw new NotImplementedException();
    }

    /**
     * @throws NotImplementedException
     */
    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        throw new NotImplementedException();
    }

    public function getInput(): string
    {
        $classname = $this::class;
        $classname = mb_strtolower(preg_replace('-App\\\\Day\\\\-','',$classname));

        [$path,$filename] = explode('\\',$classname);

        // If we have a input-file, return that file
        $finder = new Finder();
        $finder->in(__DIR__.'/../../input')->path($path)->name($filename)->files();
        $iterator = $finder->getIterator();

        if (iterator_count($iterator) > 0) {
            $iterator->rewind();

            return $iterator->current()->getContents();
        }

        // Default return empty shizzle
        return '';
    }

    public function part1command(?InputInterface $input, ?OutputInterface $output): int|string|null
    {
        $questionInput = $this->getInput();

        try {
            return $this->part1($questionInput,$input,$output);
        } catch (NotImplementedException) {
            return null;
        }
    }

    public function part2command(?InputInterface $input, ?OutputInterface $output): int|string|null
    {
        $questionInput = $this->getInput();

        try {
            return $this->part2($questionInput,$input,$output);
        } catch (NotImplementedException) {
            return null;
        }
    }
}
