<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day2 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $sum  = 0;
        $data = DataConverter::tableOfInt($puzzleInput);
        foreach ($data as $row) {
            $sum += (max($row) - min($row));
        }

        return $sum;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $sum  = 0;
        $data = DataConverter::tableOfInt($puzzleInput);
        foreach ($data as $row) {
            sort($row);
            $len = count($row);
            for ($i = 0; $i < $len - 1; ++$i) {
                for ($j = $i + 1; $j < $len; ++$j) {
                    if ($row[$j] % $row[$i] == 0) {
                        $sum += $row[$j] / $row[$i];
                        break 2;
                    }
                }
            }
        }

        return $sum;
    }
}
