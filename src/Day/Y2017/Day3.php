<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day3 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        [$x, $y] = $this->calculatePosition((int) $puzzleInput);

        return abs($x) + abs($y);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $total     = 100;
        $offset    = $total / 2;
        $arr       = array_fill_keys(range(-$offset,$offset),array_fill_keys(range(-$offset,$offset),0));
        $arr[0][0] = 1;
        $sum       = 1;
        for ($i = 2; $i < 372; ++$i) {
            [$x, $y]     = $this->calculatePosition($i);
            $sum         = $this->calculateSum($arr,$x,$y);
            $arr[$x][$y] = $sum;
            if ($sum > $puzzleInput) {
                return $sum;
            }
        }

        return $sum;
    }

    /**
     * @return array{0: int, 1:int}
     */
    private function calculatePosition(int $index): array
    {
        $maxindex = 10000;
        $n        = -1;
        $new      = 1;
        // calculate correct n
        do {
            ++$n;
            $corner = $new;
            $new    = 4 * $n ** 2 + 4 * $n + 1;
        } while ($new <= $index && $n < $maxindex);

        // Too large
        if ($n == $maxindex) {
            throw new \RuntimeException('too large');
        }
        --$n;
        if ($index == $corner) {
            return [$n, $n];
        }
        if ($index <= $corner + 2 * ($n + 1)) {
            // right side
            return [$n + 1, $n + 1 - ($index - $corner)];
        }
        if ($index <= $corner + 4 * ($n + 1)) {
            // top side
            return [$n + 1 - ($index - $corner - 2 * $n - 2), -$n - 1];
        }

        if ($index <= $corner + 6 * ($n + 1)) {
            // Left side
            return [-$n - 1, -$n - 1 + ($index - $corner - 4 * $n - 4)];
        } else { // Bottom side
            return [-$n - 1 + ($index - $corner - 6 * $n - 6), $n + 1];
        }
    }

    /**
     * @param array<int,array<int,int|float>> $array
     */
    private function calculateSum(array $array, int $x, int $y): int|float
    {
        $sum = 0;
        $sum += $array[$x - 1][$y - 1]; // dump("↖:$sum");
        $sum += $array[$x][$y - 1]; // dump("↑:$sum");
        $sum += $array[$x + 1][$y - 1]; // dump("↗:$sum");
        $sum += $array[$x - 1][$y]; // dump("←:$sum");
        $sum += $array[$x + 1][$y]; // dump("→:$sum");
        $sum += $array[$x - 1][$y + 1]; // dump("↙:$sum");
        $sum += $array[$x][$y + 1]; // dump("↓:$sum");
        $sum += $array[$x + 1][$y + 1]; // dump("↘:$sum");

        return $sum;
    }
}
