<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day7 extends AbstractDay
{
    private array $found           = []; // name => object
    private array $missingChildren = []; // childname => parent

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $rows = DataConverter::columnOfStrings($puzzleInput);
        foreach ($rows as $row) {
            $existingChildren = [];

            preg_match_all('/^(?\'name\'\w+) \((?\'weight\'\d+)\)(?> -> (?\'children\'.*))?/',$row,$matches);
            array_walk($matches,function (&$el): void {$el = $el[0] ?? ''; });
            // $matches = $matches[1];
            // dump($row,$matches);
            $name             = trim((string) $matches['name']);
            $weight           = (int) $matches['weight'];
            $existingChildren = [];
            if (isset($matches['children']) && $matches['children'] !== []) {
                $children = explode(',',(string) $matches['children']);
                array_walk($children,function (&$el): void {$el = trim($el); });
                foreach ($children as $child) {
                    // dump($child);
                    if (isset($this->found[$child])) {
                        $existingChildren[] = $this->found[$child];
                    } else {
                        $this->missingChildren[$child] = $name;
                    }
                }
            }
            $program            = new Program($name,$weight,$existingChildren);
            $this->found[$name] = $program;
            foreach ($existingChildren as $child) {
                $child->setParent($program);
            }
            if (isset($this->missingChildren[$name])) {
                $parent = $this->getProgram($this->missingChildren[$name]);
                if (!$parent instanceof Program) {
                    dump('could not find:',$this->missingChildren[$name]);
                }
                $parent->addChild($program);
                $program->setParent($parent);
            }
        }
        $possibleParents = array_filter($this->found,fn ($el) => $el->hasNoParent());
        if (count((array) $possibleParents) != 1) {
            throw new \RuntimeException('Err: wrong number of parents: '.count((array) $possibleParents));
        }

        return array_pop($possibleParents)->getName();
    }

    private function getProgram($name): ?Program
    {
        return $this->found[$name] ?? null;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parentname = $this->part1($puzzleInput);
        $parent     = $this->getProgram($parentname);

        return $parent->getCorrectedBalancedWeight();
    }
}

class Program
{
    private string $name;
    private array $children  = [];
    private ?Program $parent = null;

    public function __construct(string $name,private readonly int $weight,$children)
    {
        $this->name   = trim($name);
        foreach ($children as $child) {
            $this->addChild($child);
        }
    }

    public function setParent(self $parent): void
    {
        $this->parent = $parent;
    }

    public function hasNoParent(): bool
    {
        return $this->parent === null;
    }

    public function addChild(self $child): void
    {
        $this->children[] = $child;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getTotalWeight(): float|int
    {
        $total = $this->weight;
        foreach ($this->children as $child) {
            $total += $child->getTotalWeight();
        }

        return $total;
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    public function getTree()
    {
        if ($this->children === []) {
            return $this->getName();
        }
        $output = [];
        foreach ($this->children as $child) {
            if (!empty($child->getChildren())) {
                $output[] = $child->getName();
            } else {
                $output[$child->getName()] = $child->getTree();
            }
        }

        return $output;
    }

    public function getCorrectedBalancedWeight()
    {
        $correctWeight = null;
        $wrongWeight   = null;
        $weights       = [];
        $wrong         = null;
        foreach ($this->children as $child) {
            $weights[$child->getTotalWeight()][] = $child;
        }
        foreach ($weights as $weight => $childs) {
            if (count($childs) === 1) {
                $wrong       = $childs[0];
                $wrongWeight = $weight;
            } else {
                $correctWeight = $weight;
            }
        }
        if ($wrong === null) {
            return 0;
        }

        $childDifference = $wrong->getcorrectedBalancedWeight();
        if ($childDifference == 0) {
            return $wrong->getWeight() + $correctWeight - $wrongWeight;
        }

        return $childDifference;
    }
}
