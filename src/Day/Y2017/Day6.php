<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day6 extends AbstractDay
{
    final public const MAXSTEPS = 10000;

    private array $history      = [];

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $row    = DataConverter::rowOfInt($puzzleInput);
        $length = count($row);
        $steps  = 0;

        while (!$this->inHistory($row) && $steps < self::MAXSTEPS) {
            $this->addToHistory($row);
            $start = $this->getStart($row);
            for ($i = $row[$start], $index = $start,$row[$start] = 0; $i > 0; --$i) {
                $index = ($index + 1) % $length;
                ++$row[$index];
            }

            ++$steps;
        }
        $this->addToHistory($row);

        return $steps;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $steps = $this->part1($puzzleInput);
        $last  = $this->history[count($this->history) - 1];

        return $steps - array_search($last,$this->history);
    }

    private function getStart(array $row): int|string|false
    {
        return array_search(max($row),$row);
    }

    private function inHistory(array $row): bool
    {
        return in_array($this->hashRow($row), $this->history);
    }

    private function addToHistory(array $row): void
    {
        $this->history[]      = $this->hashRow($row);
    }

    private function hashRow($row): string
    {
        return md5(json_encode($row, JSON_THROW_ON_ERROR));
    }
}
