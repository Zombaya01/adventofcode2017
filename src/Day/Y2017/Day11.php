<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day11 extends AbstractDay
{
    /**
     * @var array<string,int>
     */
    private array $counts = ['n' => 0, 's' => 0, 'nw' => 0, 'sw' => 0, 'ne' => 0, 'se' => 0];
    /**
     * @var string[]|null
     */
    private ?array $steps = null;

    private const duplicates = [
        ['n', 's'],
        ['nw', 'se'],
        ['sw', 'ne'],
        ['n', 'se', 'sw'],
        ['ne', 's', 'nw'],
        ['se', 'sw', 'n'],
        ['s', 'nw', 'ne'],
    ];
    private const replaceables = [
        'se' => ['ne', 's'],
        'sw' => ['nw', 's'],
        'ne' => ['se', 'n'],
        'nw' => ['sw', 'n'],
        'n'  => ['ne', 'nw'],
        's'  => ['se', 'sw'],
    ];

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $steps        = explode(',',trim((string) $puzzleInput));
        $this->steps  = $steps;
        $this->counts = $this->toCounts($this->steps);

        return $this->calculatePart1();
    }

    private function calculatePart1(): int
    {
        $this->removeUnnecesarry();
        $this->reduceSteps();

        return array_sum($this->counts);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $allsteps = explode(',',trim((string) $puzzleInput));
        $max      = 0;
        foreach ($allsteps as $step) {
            ++$this->counts[$step];
            $max = max($max,$this->calculatePart1());
        }

        return $max;
    }

    public function removeUnnecesarry(): void
    {
        foreach (self::duplicates as $duplicates) {
            $toRemove = self::getMin($duplicates);
            foreach ($duplicates as $key) {
                $this->counts[$key] -= $toRemove;
            }
        }
    }

    public function reduceSteps(): void
    {
        foreach (self::replaceables as $result => $input) {
            $toReplace = self::getMin($input);
            $this->counts[$result] += $toReplace;
            foreach ($input as $key) {
                $this->counts[$key] -= $toReplace;
            }
        }
    }

    /**
     * @param array<int,string> $steps
     *
     * @return array<string, int>
     */
    private function toCounts(array $steps): array
    {
        return [
            'n'  => $this->array_count_value($steps,'n'),
            's'  => $this->array_count_value($steps,'s'),
            'ne' => $this->array_count_value($steps,'ne'),
            'se' => $this->array_count_value($steps,'se'),
            'nw' => $this->array_count_value($steps,'nw'),
            'sw' => $this->array_count_value($steps,'sw'),
        ];
    }

    /**
     * @param array<string> $keys
     */
    private function getMin(array $keys): int
    {
        $min = PHP_INT_MAX;
        foreach ($keys as $key) {
            $min = min($min,$this->counts[$key]);
        }

        return $min;
    }

    /**
     * @param array<int,string> $array
     */
    private function array_count_value(array $array,string $value): int
    {
        return count(array_keys($array,$value));
    }
}
