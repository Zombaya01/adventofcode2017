<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day5 extends AbstractDay
{
    final public const MAXSTEPS = 100_000_000;

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $row   = DataConverter::columnOfInt($puzzleInput);
        $steps = 0;
        $index = 0;
        $len   = count($row);
        do {
            $nextindex = $index + $row[$index];
            ++$row[$index];
            $index = $nextindex;
            ++$steps;
        } while ($index >= 0 && $index < $len && $steps < self::MAXSTEPS);

        if ($steps == self::MAXSTEPS) {
            throw new \RuntimeException('Err: To many rounds');
        }

        return $steps;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $row   = DataConverter::columnOfInt($puzzleInput);
        $steps = 0;
        $index = 0;
        $len   = count($row);
        do {
            $nextindex = $index + $row[$index];
            if ($row[$index] >= 3) {
                --$row[$index];
            } else {
                ++$row[$index];
            }
            $index = $nextindex;
            ++$steps;
        } while ($index >= 0 && $index < $len && $steps < self::MAXSTEPS);

        if ($steps == self::MAXSTEPS) {
            throw new \RuntimeException('ERR: too big');
        }

        return $steps;
    }
}
