<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day8 extends AbstractDay
{
    /** @var array<string,int> */
    private array $registers              = [];
    private int $maxEver                  = 0;
    final public const REGEX              = "/^(?<name>\w+) (?<operation>inc|dec) (?<amount>-?\d+) if (?<name2>\w+) (?<test>[<>=\!]+) (?<amount2>-?\d+)$/";

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $rows = DataConverter::columnOfStrings($puzzleInput);
        foreach ($rows as $row) {
            preg_match_all(self::REGEX,$row,$matches);
            array_walk($matches,function (&$el): void {$el = $el[0] ?? ''; });
            foreach (array_keys($matches) as $key) {
                if (is_numeric($key)) {
                    unset($matches[$key]);
                }
            }

            [
                'amount'    => $amount,
                'name'      => $name,
                'operation' => $operation,
                'amount2'   => $amount2,
                'test'      => $test,
                'name2'     => $name2,
            ] = $matches;

            try {
                if ($this->doesTestPass($name2,$test,$amount2)) {
                    $this->incRegister($name,$operation,$amount);
                }
            } catch (\Exception $e) {
                dump($row);
                dump($matches);
                throw $e;
            }

            // dump($row,$this->registers);
        }

        return max($this->registers);
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $this->part1($puzzleInput);

        return $this->maxEver;
    }

    private function getRegister(string $name): int
    {
        if (!isset($this->registers[$name])) {
            $this->registers[$name] = 0;
        }

        return $this->registers[$name];
    }

    private function incRegister(string $name, string $operation, int $amount): void
    {
        if ($operation === 'dec') {
            $amount = -$amount;
        }
        $this->getRegister($name); //  To initialize
        $this->registers[$name] += $amount;
        if ($this->registers[$name] > $this->maxEver) {
            $this->maxEver = $this->registers[$name];
        }
    }

    private function doesTestPass(string $name, string $operation, int $testvalue)
    {
        $value = $this->getRegister($name);

        return match ($operation) {
            '<'     => $value < $testvalue,
            '>'     => $value > $testvalue,
            '<='    => $value <= $testvalue,
            '>='    => $value >= $testvalue,
            '=', '==' => $value === $testvalue,
            '!='    => $value !== $testvalue,
            default => throw new \Exception("Could not match operation '$operation'"),
        };
    }
}
