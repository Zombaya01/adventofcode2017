<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day9 extends AbstractDay
{
    private int $removedGarbage = 0;

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $sum    = 0;
        $val    = 0;
        $string = $this->removeGarbage($puzzleInput);
        for ($i = 0; $i < mb_strlen((string) $string); ++$i) {
            if ($string[$i] == '{') {
                ++$val;
                $sum += $val;
            } elseif ($string[$i] == '}') {
                --$val;
            }
        }

        return $sum;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $this->part1($puzzleInput);

        return $this->removedGarbage;
    }

    public function removeGarbage($string)
    {
        while (($pos = mb_strpos((string) $string,'!')) !== false) {
            $string = $this->removePartOfString($string,$pos,2);
        }
        do {
            $garbageFound = false;
            $start        = mb_strpos((string) $string,'<');
            if ($start !== false) {
                $end = mb_strpos((string) $string,'>',$start);
                if ($end !== false) {
                    $garbageFound = true;
                    $this->removedGarbage += $end - $start - 1;
                    $string = $this->removePartOfString($string,$start,$end - $start + 1);
                }
            }
        } while ($garbageFound);

        return $string;
    }

    public function removePartOfString($string,$offset,$length): string
    {
        return mb_substr((string) $string,0,$offset).mb_substr((string) $string,$offset + $length);
    }
}
