<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day4 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $sum  = 0;
        $data = DataConverter::tableOfStrings($puzzleInput);
        foreach ($data as $row) {
            $sum += $this->isValidPassphrase($row) ? 1 : 0;
        }

        return $sum;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $sum  = 0;
        $data = DataConverter::tableOfStrings($puzzleInput);
        foreach ($data as $row) {
            foreach ($row as $key => $word) {
                $word = mb_str_split($word);
                sort($word);
                $row[$key] = implode('',$word);
            }

            $sum += $this->isValidPassphrase($row) ? 1 : 0;
        }

        return $sum;
    }

    protected function isValidPassphrase($phrase): bool
    {
        return max(array_count_values($phrase)) == 1;
    }
}
