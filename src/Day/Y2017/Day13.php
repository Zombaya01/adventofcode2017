<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day13 extends AbstractDay
{
    private array $firewall        = [];
    final public const MAXDELAY    = 100_000_000;

    public function getParsedTestInput(): static
    {
        $this->parseInput('0: 3
                1: 2
                4: 4
                6: 4');

        return $this;
    }

    /**
     * @return mixed[]
     */
    public function parseInput(string $input): array
    {
        $firewall = [];
        $input    = DataConverter::columnOfStrings($input);
        foreach ($input as $row) {
            [$step, $length]             = explode(': ',$row);
            $this->firewall[(int) $step] = (int) $length;
        }

        return $this->firewall;
    }

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $this->parseInput($puzzleInput);
        $max      = max(array_keys($this->firewall));
        $severity = 0;
        for ($i = 0; $i <= $max; ++$i) {
            if ($this->isCaught($i)) {
                $severity += $this->getSeverity($i);
            }
        }

        return $severity;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $this->parseInput($puzzleInput);
        for ($delay = 0; $delay < self::MAXDELAY; ++$delay) {
            $max = max(array_keys($this->firewall));
            for ($i = 0; $i <= $max; ++$i) {
                if ($this->isCaught($i,$delay)) {
                    continue 2;
                }
            }

            return $delay;
        }

        throw new \RuntimeException('Err: To many rounds');
    }

    public function isCaught($move,$delay = 0): bool
    {
        if (!isset($this->firewall[$move])) {
            return false;
        }

        return (($move + $delay) % (2 * ($this->firewall[$move] - 1))) == 0;
    }

    public function getSeverity($move): int|float
    {
        return $move * $this->firewall[$move];
    }
}
