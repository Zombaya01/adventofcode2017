<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day1 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input = $puzzleInput.$puzzleInput[0];
        $sum   = 0;

        $len = mb_strlen($input) - 1;
        for ($i = 0; $i < $len; ++$i) {
            if ($input[$i] === $input[$i + 1]) {
                $sum += (int) $input[$i];
            }
        }

        return $sum;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $sum   = 0;

        $len = mb_strlen($puzzleInput) / 2;
        for ($i = 0; $i < $len; ++$i) {
            if ($puzzleInput[$i] === $puzzleInput[$i + $len]) {
                $sum += 2 * (int) $puzzleInput[$i];
            }
        }

        return $sum;
    }
}
