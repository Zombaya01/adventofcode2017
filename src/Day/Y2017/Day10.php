<?php

namespace App\Day\Y2017;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day10 extends AbstractDay
{
    private ?int $index = null;

    /** @var array<int,int>|null */
    private ?array $list = null;
    private ?int $skip   = null;
    private int $length;

    public function initialize(int $length): void
    {
        $this->index  = 0;
        $this->skip   = 0;
        $this->list   = range(0, $length - 1);
        $this->length = $length;
    }

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::rowOfStrings($puzzleInput);

        $this->initialize((int) $parsedInput[0]);

        foreach (explode(',',$parsedInput[1]) as $value) {
            $this->rotate((int) $value);
        }

        return $this->list[0] * $this->list[1];
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $this->initialize((int) $puzzleInput[0]);

        foreach (explode(',',(string) $puzzleInput[1]) as $value) {
            $this->rotate(ord($value),64);
        }

        return $this->list[0] * $this->list[1];
    }

    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * @return float[]|int[]|mixed[]
     */
    public function getList(): array
    {
        return $this->list;
    }

    public function rotate(int $length,int $rounds = 1): void
    {
        for ($round = 0; $round < $rounds; ++$round) {
            if ($length > 1) {
                $offset = (int) floor($length / 2);
                for ($i = 1; $i <= $offset; ++$i) {
                    if ($length % 2 == 0) {
                        $this->switchValues($offset - 1 + $i,$offset - $i);
                    } else {
                        $this->switchValues($offset + $i,$offset - $i);
                    }
                }
            }
        }
        $this->index = ($this->index + $this->skip + $length) % $this->length;
        ++$this->skip;
    }

    private function getValue(int $offset): int
    {
        return $this->list[($this->index + $offset) % $this->length];
    }

    private function setValue(int $offset,int $value): void
    {
        $this->list[($this->index + $offset) % $this->length] = $value;
    }

    private function switchValues(int $index1, int $index2): void
    {
        $a = $this->getValue($index1);
        $b = $this->getValue($index2);
        $this->setValue($index1,$b);
        $this->setValue($index2,$a);
    }
}
