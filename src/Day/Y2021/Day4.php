<?php

declare(strict_types=1);

namespace App\Day\Y2021;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day4 extends AbstractDay
{
    private function parse(string $input): ParsedInput
    {
        $semiparsed = DataConverter::columnOfStringsSplitByBlankLine($input);
        $numbers    = array_shift($semiparsed);

        return new ParsedInput(
            DataConverter::rowOfInt($numbers,','),
            array_map(static fn ($row): \App\Day\Y2021\Board => new Board(DataConverter::tableOfInt($row)),$semiparsed)
        );
    }

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = $this->parse($puzzleInput);

        foreach ($parsedInput->numbers as $number) {
            foreach ($parsedInput->boards as $board) {
                $board->check($number);
                if ($board->hasWon()) {
                    return $number*$board->getScore();
                }
            }
        }

        return null;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput               = $this->parse($puzzleInput);
        $numbersOfBoardsWhoHaveWon = 0;

        foreach ($parsedInput->numbers as $number) {
            foreach ($parsedInput->boards as $board) {
                if ($board->hasWon()) {
                    continue;
                }

                $board->check($number);
                if ($board->hasWon()) {
                    ++$numbersOfBoardsWhoHaveWon;

                    if ($numbersOfBoardsWhoHaveWon === count($parsedInput->boards)) {
                        return $number*$board->getScore();
                    }
                }
            }
        }

        return null;
    }
}

class ParsedInput
{
    /**
     * @param array|int[]   $numbers
     * @param array|Board[] $boards
     */
    public function __construct(public array $numbers, public array $boards)
    {
    }
}

class Board
{
    private array $allValues;
    private array $allRows;
    private bool $hasWon = false;

    public function __construct(array $values)
    {
        $this->allValues = array_merge(...$values);

        for ($i = 0; $i < 5; ++$i) {
            $this->allRows[] = array_column($values,$i);
            $this->allRows[] = $values[$i];
        }
    }

    public function check(int $number): void
    {
        if (in_array($number,$this->allValues)) {
            $this->allValues = array_filter($this->allValues,static fn ($val): bool => $val !== $number);
            foreach ($this->allRows as $key => $row) {
                if (in_array($number,$row)) {
                    $this->allRows[$key] = array_filter($this->allRows[$key], static fn ($val): bool => $val !== $number);

                    if ((array) $this->allRows[$key] === []) {
                        $this->hasWon = true;
                    }
                }
            }
        }
    }

    public function getScore(): int
    {
        return array_sum($this->allValues);
    }

    public function hasWon(): bool
    {
        return $this->hasWon;
    }
}
