<?php

declare(strict_types=1);

namespace App\Day\Y2021;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day3 extends AbstractDay
{
    /**
     * @return array[]
     */
    private function parse(string $input): array
    {
        $rows = DataConverter::columnOfStrings($input);

        return array_map(static fn ($row): array => array_map('intval',mb_str_split((string) $row,1)),$rows);
    }

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = $this->parse($puzzleInput);

        $numberOfColumns = count($parsedInput[0]);
        $columnLength    = count($parsedInput);
        $halfway         = ((float) $columnLength) / 2;

        $epsilon = 0;
        $gamma   = 0;
        for ($i=0, $j=$numberOfColumns -1; $i<$numberOfColumns; $i++,$j--) {
            $values = array_column($parsedInput,$j);
            $sum    = array_sum($values);
            if ($sum > $halfway) {
                $epsilon += 2 ** $i;
            } else {
                $gamma += 2 ** $i;
            }
        }

        return $epsilon*$gamma;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = $this->parse($puzzleInput);

        $numberOfColumns = count($parsedInput[0]);

        for ($i=0, $j=$numberOfColumns -1; $i<$numberOfColumns; $i++,$j--) {
            $prettyValues = array_map(fn ($val): string => implode('',$val), $parsedInput);

            $halfway         = ((float) count($parsedInput)) / 2;
            $values          = array_column($parsedInput,$i);
            $sum             = array_sum($values);
            if ($sum >= $halfway) {
                $parsedInput = array_filter($parsedInput,static fn ($row): bool => $row[$i] === 1);
            } else {
                $parsedInput = array_filter($parsedInput,static fn ($row): bool => $row[$i] === 0);
            }

            if (count($parsedInput) === 1) {
                break;
            }
        }
        $oxigenValue = bindec(implode('',array_values($parsedInput)[0]));

        $parsedInput = $this->parse($puzzleInput);

        for ($i=0, $j=$numberOfColumns -1; $i<$numberOfColumns; $i++,$j--) {
            $prettyValues = array_map(fn ($val): string => implode('',$val), $parsedInput);

            $halfway         = ((float) count($parsedInput)) / 2;
            $values          = array_column($parsedInput,$i);
            $sum             = array_sum($values);
            if ($sum >= $halfway) {
                $parsedInput = array_filter($parsedInput,static fn ($row): bool => $row[$i] === 0);
            } else {
                $parsedInput = array_filter($parsedInput,static fn ($row): bool => $row[$i] === 1);
            }

            if (count($parsedInput) === 1) {
                break;
            }
        }
        $co2value = bindec(implode('',array_values($parsedInput)[0]));

        return $co2value*$oxigenValue;
    }
}
