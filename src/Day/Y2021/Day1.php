<?php

declare(strict_types=1);

namespace App\Day\Y2021;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day1 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfInt($puzzleInput);

        $count            = 0;
        $parsedInputCount = count($parsedInput);
        for ($i = 1; $i < $parsedInputCount; ++$i) {
            if ($parsedInput[$i] > $parsedInput[$i-1]) {
                ++$count;
            }
        }

        return $count;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::columnOfInt($puzzleInput);

        $count            = 0;
        $parsedInputCount = count($parsedInput);
        for ($i = 3; $i < $parsedInputCount; ++$i) {
            $sum1 = array_sum(array_slice($parsedInput,$i-3,3));
            $sum2 = array_sum(array_slice($parsedInput,$i-2,3));
            if ($sum2 > $sum1) {
                ++$count;
            }
        }

        return $count;
    }
}
