<?php

declare(strict_types=1);

namespace App\Day\Y2021;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day2 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = $this->parse($puzzleInput);
        $x           = 0;
        $y           = 0;

        foreach ($parsedInput as $command) {
            switch ($command->direction) {
                case Direction::UP:
                    $y -= $command->amount;
                    break;
                case Direction::DOWN:
                    $y += $command->amount;
                    break;
                case Direction::FORWARD:
                    $x += $command->amount;
                    break;
            }
        }

        return $x*$y;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = $this->parse($puzzleInput);
        $x           = 0;
        $y           = 0;
        $aim         = 0;

        foreach ($parsedInput as $command) {
            switch ($command->direction) {
                case Direction::UP:
                    $aim -= $command->amount;
                    break;
                case Direction::DOWN:
                    $aim += $command->amount;
                    break;
                case Direction::FORWARD:
                    $x += $command->amount;
                    $y += $command->amount * $aim;
                    break;
            }
        }

        return $x*$y;
    }

    /**
     * @return array|Command[]
     */
    private function parse(string $input): array
    {
        $parsedInput = DataConverter::tableOfStrings($input);

        return array_map(static fn ($row): \App\Day\Y2021\Command => new Command($row),$parsedInput);
    }
}

enum Direction
{
    case UP;
    case DOWN;
    case FORWARD;

    public static function make(string $input): self
    {
        return match ($input) {
            'up'      => self::UP,
            'down'    => self::DOWN,
            'forward' => self::FORWARD,
            default   => throw new \RuntimeException('invalid value'),
        };
    }
}

class Command
{
    public Direction $direction;
    public int $amount;

    public function __construct(array $row)
    {
        $this->direction = Direction::make($row[0]);
        $this->amount    = (int) $row[1];
    }
}
