<?php

namespace App\Day\Y2019;

use App\Day\Y2019\Day11\Thread\RunnableInterface;

class IntCodeMachine implements RunnableInterface
{
    private int $i            = 0;
    private int $relativeBase = 0;

    /** @param array<int,int> $code */
    public function __construct(private array $code, private readonly SignalBufferInterface $input, private readonly SignalBufferInterface $output)
    {
    }

    public function getInput(): SignalBufferInterface
    {
        return $this->input;
    }

    public function getOutput(): SignalBufferInterface
    {
        return $this->output;
    }

    private function read(Mode $mode, int $position): int
    {
        return $this->code[$this->getPosition($mode,$position)] ?? 0;
    }

    private function getPosition(Mode $mode, int $position): int
    {
        return match ($mode) {
            Mode::Immediate => $position,
            Mode::Position  => $this->code[$position] ?? 0,
            Mode::Relative  => $this->relativeBase + $this->code[$position],
        };
    }

    public function hasInputToParse(): bool
    {
        return $this->input->count() > 0;
    }

    /**
     * @throws \App\Exception\IntCodeMachine\MissingInputException
     */
    public function run(): int
    {
        while (array_key_exists($this->i,$this->code)) {
            $instruction = new Instruction($this->code[$this->i]);
            $a           = null;
            $b           = null;

            switch ($instruction->opCode) {
                case OpCode::Add:
                case OpCode::Multiply:
                case OpCode::JumpIfTrue:
                case OpCode::JumpIfFalse:
                case OpCode::LessThan:
                case OpCode::Equals:
                    $a = $this->read($instruction->modeA, $this->i + 1);
                    $b = $this->read($instruction->modeB, $this->i + 2);
                    break;
                case OpCode::RelativeBaseOffset:
                case OpCode::Output:
                    $a = $this->read($instruction->modeA, $this->i + 1);
                    break;
                default:
                    // nothing needs to be done
            }

            switch ($instruction->opCode) {
                case OpCode::Add:
                    $this->code[$this->getPosition($instruction->modeC,$this->i + 3)] = $a + $b;
                    $this->i += 4;
                    break;
                case OpCode::Multiply:
                    $this->code[$this->getPosition($instruction->modeC,$this->i + 3)] = $a * $b;
                    $this->i += 4;
                    break;
                case OpCode::Store:
                    $this->code[$this->getPosition($instruction->modeA, $this->i + 1)] = $this->input->shift();

                    $this->i += 2;
                    break;
                case OpCode::Output:
                    $this->output->push($a);
                    $this->i += 2;
                    break;
                case OpCode::JumpIfTrue:
                    if ($a !== 0) {
                        $this->i = $b;
                        break;
                    }
                    $this->i += 3;
                    break;
                case OpCode::JumpIfFalse:
                    if ($a === 0) {
                        $this->i = $b;
                        break;
                    }
                    $this->i += 3;
                    break;
                case OpCode::LessThan:
                    $this->code[$this->getPosition($instruction->modeC,$this->i + 3)] = $a < $b ? 1 : 0;
                    $this->i += 4;
                    break;
                case OpCode::Equals:
                    $this->code[$this->getPosition($instruction->modeC,$this->i + 3)] = $a === $b ? 1 : 0;
                    $this->i += 4;
                    break;
                case OpCode::Return:
                    return $this->code[0];
                case OpCode::RelativeBaseOffset:
                    $this->relativeBase += $a;
                    $this->i            += 2;
                    break;
                default:
                    error_log(sprintf('Index: %d.  Value: %d', $this->i, $this->code[$this->i]));
                    dump($this->code);

                    throw new \Exception('ai ai ai');
            }
        }

        throw new \Exception('Program ran out of code to run!');
    }
}
