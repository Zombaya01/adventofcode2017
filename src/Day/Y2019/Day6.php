<?php

namespace App\Day\Y2019;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day6 extends AbstractDay
{
    /** @var Star[] */
    private array $map = [];

    public function __construct()
    {
        $input = DataConverter::columnOfStrings($this->getInput());
        //        $input = DataConverter::columnOfStrings(<<<EOF
        // COM)B
        // B)C
        // C)D
        // D)E
        // E)J
        // J)K
        // K)L
        // E)F
        // B)G
        // G)H
        // D)I
        // EOF);

        $missingParent = [];

        $this->map['COM'] = new Star('COM',null);

        foreach ($input as $line) {
            [$parent, $child] = explode(')',$line);

            if (!array_key_exists($parent,$this->map)) {
                $star                     = new Star($child,null);
                $missingParent[$parent][] = $star;
            } else {
                $star = new Star($child,$this->map[$parent]);
            }
            $this->map[$child] = $star;

            if (array_key_exists($child,$missingParent)) {
                /** @var Star $childStar */
                foreach ($missingParent[$child] as $childStar) {
                    $childStar->setParent($star);
                    $star->addChild($childStar);
                }
                unset($missingParent[$child]);
            }
        }

        if ($missingParent !== []) {
            throw new \RuntimeException('Too many stars missing a parent');
        }

        $this->map['COM']->setDistance(0);
    }

    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $total = 0;
        foreach ($this->map as $star) {
            $total += $star->getDistance();
        }

        return $total;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $a = 'T2J';
        $b = 'FZ8';

        $aParents = $this->map[$a]->getParents();
        $bParents = $this->map[$b]->getParents();

        foreach ($aParents as $aIndex => $aParent) {
            if (in_array($aParent,$bParents)) {
                return $aIndex + array_search($aParent,$bParents) + 2;
            }
        }

        return -1;
    }
}

class Star
{
    private array $children = [];
    private int $distance   = 0;

    public function __construct(private readonly string $name, private ?Star $parent)
    {
        if ($parent instanceof Star) {
            $parent->addChild($this);
        }
    }

    public function addChild(Star $child): Star
    {
        $this->children[] = $child;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getParent(): ?Star
    {
        return $this->parent;
    }

    public function setParent(Star $parent): Star
    {
        $this->parent = $parent;

        return $this;
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    public function getDistance(): int
    {
        return $this->distance;
    }

    /**
     * @return $this
     */
    public function setDistance(int $distance): self
    {
        $this->distance = $distance;

        foreach ($this->children as $child) {
            $child->setDistance($distance + 1);
        }

        return $this;
    }

    public function getParents(): array
    {
        $parents = [];
        $star    = $this;
        for ($i = 0; $i < 99999; ++$i) {
            if (!$star->getParent() instanceof Star) {
                return $parents;
            }
            $parents[] = $star->getParent();
            $star      = $star->getParent();
        }
        throw new \RuntimeException('Should have found a node without parent by now');
    }
}
