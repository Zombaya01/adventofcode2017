<?php

namespace App\Day\Y2019;

use App\Exception\IntCodeMachine as IntCodeMachineException;

class SignalBuffer implements SignalBufferInterface
{
    /**
     * @param array<int,int> $buffer
     */
    public function __construct(private array $buffer = [])
    {
        foreach ($buffer as $item) {
            if (!is_int($item)) {
                throw new \InvalidArgumentException('Input invalid!'.var_export($buffer, true));
            }
        }
    }

    public function push(int $value): void
    {
        $this->buffer[] = $value;
    }

    public function shift(): int
    {
        if ($this->buffer === []) {
            throw new IntCodeMachineException\MissingInputException('Ran out of input');
        }

        return array_shift($this->buffer);
    }

    public function last(): ?int
    {
        return $this->buffer[array_key_last($this->buffer)] ?? null;
    }

    /** @return array<int,int> */
    public function toArray(): array
    {
        return $this->buffer;
    }

    public function count(): int
    {
        return count($this->buffer);
    }
}
