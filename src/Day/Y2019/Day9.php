<?php

declare(strict_types=1);

namespace App\Day\Y2019;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day9 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): ?int
    {
        $input          = DataConverter::rowOfInt($puzzleInput,',');
        $intCodeMachine = new IntCodeMachine($input,new SignalBuffer([1]),new SignalBuffer());

        $intCodeMachine->run();

        $output = $intCodeMachine->getOutput();

        if ($output->count() > 1 && $consoleOutput instanceof OutputInterface) {
            $consoleOutput->writeln('Failed to correctly parse program');
            $consoleOutput->writeln('');
            $consoleOutput->writeln('Failed opcodes:');
            $i = 1;
            $consoleOutput->writeln(array_map(fn ($row): string => sprintf('%d: %d',$i++,$row),$output->toArray()));
        }

        return $output->shift();
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): ?int
    {
        $input          = DataConverter::rowOfInt($puzzleInput,',');
        $intCodeMachine = new IntCodeMachine($input,new SignalBuffer([2]),new SignalBuffer());

        $intCodeMachine->run();

        $output = $intCodeMachine->getOutput();

        if ($output->count() > 1 && $consoleOutput instanceof OutputInterface) {
            $consoleOutput->writeln('Failed to correctly parse program');
            $consoleOutput->writeln('');
            $consoleOutput->writeln('Failed opcodes:');
            $i = 1;
            $consoleOutput->writeln(array_map(fn ($row): string => sprintf('%d: %d',$i++,$row),$output->toArray()));
        }

        return $output->shift();
    }
}
