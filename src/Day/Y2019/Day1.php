<?php

namespace App\Day\Y2019;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day1 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input = DataConverter::columnOfInt($puzzleInput);

        return array_sum(array_map([self::class, 'calculateFuelCost'],$input));
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input = DataConverter::columnOfInt($puzzleInput);

        return array_sum(array_map([self::class, 'calculateFuelCostRecursiveFuelCost'],$input));
    }

    private static function calculateFuelCost($in): float
    {
        return floor($in / 3) - 2;
    }

    private function calculateFuelCostRecursiveFuelCost($in): int|float
    {
        $total = 0;
        $part  = self::calculateFuelCost($in);
        do {
            $total += $part;
            $part = self::calculateFuelCost($part);
        } while ($part >= 0);

        return $total;
    }
}
