<?php

namespace App\Day\Y2019;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day3 extends AbstractDay
{
    public function part1(string $input, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input   = DataConverter::tableOfStrings($input,',');
        $storage = [];

        foreach ($input as $wirenumber => $wire) {
            $x         = 0;
            $y         = 0;
            $locations = [];
            foreach ($wire as $section) {
                $direction = $section[0];
                $amount    = (int) mb_substr((string) $section,1);
                $xShift    = 0;
                $yShift    = 0;

                switch ($direction) {
                    case 'D':
                        $yShift = -1;
                        break;
                    case 'U':
                        $yShift = 1;
                        break;
                    case 'L':
                        $xShift = -1;
                        break;
                    case 'R':
                        $xShift = 1;
                }
                for ($i = 0; $i < $amount; ++$i) {
                    $x += $xShift;
                    $y += $yShift;

                    $label = sprintf('%d %d',$x,$y);

                    $locations[$label] = [$x, $y];
                }
            }
            $storage[$wirenumber] = $locations;
        }

        $intersections = array_intersect_key($storage[0],$storage[1]);
        $closestCross  = null;

        foreach ($intersections as $intersection) {
            $newDistance = $this->manhattanDistance($intersection[0], $intersection[1]);
            if ($closestCross === null || $newDistance < $closestCross) {
                $closestCross = $newDistance;
            }
        }

        return $closestCross;
    }

    public function part2(string $input, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input   = DataConverter::tableOfStrings($input,',');
        $storage = [];

        foreach ($input as $wirenumber => $wire) {
            $x         = 0;
            $y         = 0;
            $length    = 0;
            $locations = [];
            foreach ($wire as $section) {
                $direction = $section[0];
                $amount    = (int) mb_substr((string) $section,1);
                $xShift    = 0;
                $yShift    = 0;

                switch ($direction) {
                    case 'D':
                        $yShift = -1;
                        break;
                    case 'U':
                        $yShift = 1;
                        break;
                    case 'L':
                        $xShift = -1;
                        break;
                    case 'R':
                        $xShift = 1;
                }
                for ($i = 0; $i < $amount; ++$i) {
                    $x += $xShift;
                    $y += $yShift;
                    ++$length;

                    $label = sprintf('%d %d',$x,$y);

                    if (!array_key_exists($label, $locations) || $locations[$label][2] > $length) {
                        $locations[$label] = [$x, $y, $length];
                    }
                }
            }
            $storage[$wirenumber] = $locations;
        }

        $intersections = array_intersect(array_keys($storage[0]),array_keys($storage[1]));
        $closestCross  = null;

        foreach ($intersections as $intersection) {
            $newDistance = $storage[0][$intersection][2] + $storage[1][$intersection][2];

            if ($closestCross === null || $newDistance < $closestCross) {
                $closestCross = $newDistance;
            }
        }

        return $closestCross;
    }

    private function manhattanDistance(int $x, int $y): float|int
    {
        return abs($x) + abs($y);
    }
}
