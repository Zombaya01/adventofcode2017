<?php

namespace App\Day\Y2019;

class Instruction
{
    public readonly OpCode $opCode;
    public readonly Mode $modeA;
    public readonly Mode $modeB;
    public readonly Mode $modeC;

    public function __construct(int $code)
    {
        $string       = str_pad((string) $code, 5, '0', STR_PAD_LEFT);
        $this->opCode = OpCode::tryFrom((int) mb_substr($string, 3, 2));
        $this->modeA  = Mode::from((int) $string[2]);
        $this->modeB  = Mode::from((int) $string[1]);
        $this->modeC  = Mode::from((int) $string[0]);
    }
}
