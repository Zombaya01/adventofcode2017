<?php

declare(strict_types=1);

namespace App\Day\Y2019;

interface SignalBufferInterface
{
    public function push(int $value): void;

    public function shift(): int;

    public function last(): ?int;

    /** @return array<int,int> */
    public function toArray(): array;

    public function count(): int;
}
