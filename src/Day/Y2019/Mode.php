<?php

namespace App\Day\Y2019;

enum Mode: int
{
    case Position  = 0;
    case Immediate = 1;
    case Relative  = 2;
}
