<?php

namespace App\Day\Y2019\Day11;

enum ThreadState
{
    case READY;
    case EXITED;
    case WAITING_ON_INPUT;
}
