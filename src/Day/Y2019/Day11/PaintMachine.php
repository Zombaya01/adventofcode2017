<?php

declare(strict_types=1);

namespace App\Day\Y2019\Day11;

use App\Day\Y2019\SignalBufferInterface;
use App\Exception\IntCodeMachine\MissingInputException;

class PaintMachine implements Thread\RunnableInterface
{
    private Direction $direction = Direction::Up;
    /** @var array<int,array<int,Color>> */
    private array $hull;
    private bool $keepPainting = true;
    private int $x             = 0;
    private int $y             = 0;
    /** @var array<string,int> */
    private array $paintedPanels = [];
    private \stdClass $window;

    public function __construct(private readonly SignalBufferInterface $input, private readonly SignalBufferInterface $output, Color $initialColor, public readonly bool $debug = false)
    {
        $this->output->push($initialColor->value);
        $this->window       = (new \stdClass());
        $this->window->minX = 0;
        $this->window->minY = 0;
        $this->window->maxX = 0;
        $this->window->maxY = 0;

        $this->paintIfDebug();
    }

    public function getInput(): SignalBufferInterface
    {
        return $this->input;
    }

    public function getOutput(): SignalBufferInterface
    {
        return $this->output;
    }

    /**
     * @return $this
     */
    public function stopPainting(): static
    {
        $this->keepPainting = false;

        return $this;
    }

    public function getNumberOfPaintedPanels(): int
    {
        return count($this->paintedPanels);
    }

    public function hasInputToParse(): bool
    {
        return $this->input->count() >= 2;
    }

    public function run(): int
    {
        while ($this->keepPainting) {
            if ($this->input->count() < 2) {
                throw new MissingInputException();
            }

            $nextColor    = Color::from($this->input->shift());
            $nextRotation = Turn::from($this->input->shift());

            $this
                ->paintCurrentPlace($nextColor)
                ->rotateAndMove($nextRotation)
                ->outputCurrentColor();

            $this->paintIfDebug();
        }

        return 0;
    }

    public function paint(int $min = 0, int $max = 0): string
    {
        $this->setWindow($min,$min);
        $this->setWindow($max,$max);

        $output = '';
        for ($j = $this->window->minY; $j <= $this->window->maxY; ++$j) {
            for ($i = $this->window->minX; $i <= $this->window->maxX; ++$i) {
                if ($this->x === $i && $this->y === $j) {
                    $output .= match ($this->direction) {
                        Direction::Up    => '^',
                        Direction::Left  => '<',
                        Direction::Down  => 'v',
                        Direction::Right => '>',
                    };
                } elseif (!isset($this->hull[$j][$i]) || $this->hull[$j][$i] === Color::Black) {
                    $output .= '.';
                } else {
                    $output .= '#';
                }
            }
            $output .= "\n";
        }

        return implode("\n",array_reverse(explode("\n",trim($output))));
    }

    /** @return $this */
    private function rotateAndMove(Turn $turn): static
    {
        $this->direction = $this->direction->turn($turn);
        $this->y += $this->direction->y();
        $this->x += $this->direction->x();

        $this->setWindow($this->x, $this->y);

        return $this;
    }

    /** @return $this */
    private function paintCurrentPlace(Color $nextColor): static
    {
        $this->hull[$this->y][$this->x] = $nextColor;

        // Save that we painted it
        $panelName = sprintf('%d|%d', $this->x, $this->y);
        if (!array_key_exists($panelName, $this->paintedPanels)) {
            $this->paintedPanels[$panelName] = 1;
        } else {
            ++$this->paintedPanels[$panelName];
        }

        return $this;
    }

    /** @return $this */
    private function outputCurrentColor(): static
    {
        $this->output->push($this->hull[$this->y][$this->x]->value ?? Color::Black->value);

        return $this;
    }

    private function setWindow(int $x, int $y): void
    {
        $this->window->minX = min($this->window->minX, $x);
        $this->window->minY = min($this->window->minY, $y);
        $this->window->maxX = max($this->window->maxX, $x);
        $this->window->maxY = max($this->window->maxY, $y);
    }

    private function paintIfDebug(): void
    {
        if ($this->debug) {
            echo $this->paint(-2, 2)."\n\n";
        }
    }
}
