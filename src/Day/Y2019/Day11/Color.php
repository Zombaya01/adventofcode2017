<?php

declare(strict_types=1);

namespace App\Day\Y2019\Day11;

enum Color: int
{
    case Black = 0;
    case White = 1;
}
