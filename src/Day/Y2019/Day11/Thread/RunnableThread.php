<?php

namespace App\Day\Y2019\Day11\Thread;

use App\Day\Y2019\Day11\ThreadState;
use App\Exception\IntCodeMachine\MissingInputException;

/**
 * Runs the intcodemachine untill it exits. Will halt when intcodemachine needs extra input.
 */
class RunnableThread extends AbstractThread
{
    public function __construct(private readonly RunnableInterface $runnable)
    {
        parent::__construct();
    }

    public function isRunnable(): bool
    {
        return match ($this->state) {
            ThreadState::READY            => true,
            ThreadState::EXITED           => false,
            ThreadState::WAITING_ON_INPUT => $this->runnable->hasInputToParse(),
        };
    }

    public function run(): ThreadState
    {
        if (!$this->isRunnable()) {
            throw new \RuntimeException('This thread is not in a runnable state and should not be run');
        }

        try {
            $this->exitCode = $this->runnable->run();
            $this->state    = ThreadState::EXITED;
        } catch (MissingInputException) {
            $this->state = ThreadState::WAITING_ON_INPUT;
        }

        return $this->state;
    }
}
