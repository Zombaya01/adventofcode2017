<?php

declare(strict_types=1);

namespace App\Day\Y2019\Day11\Thread;

use App\Day\Y2019\SignalBufferInterface;

interface RunnableInterface
{
    public function getInput(): SignalBufferInterface;

    public function getOutput(): SignalBufferInterface;

    public function hasInputToParse(): bool;

    /**
     * Run the class until it either runs out of input or returns a value.
     *
     * @throws \App\Exception\IntCodeMachine\MissingInputException When run out of input
     */
    public function run(): int;
}
