<?php

namespace App\Day\Y2019\Day11\Thread;

use App\Day\Y2019\Day11\ThreadState;
use App\Exception\IntCodeMachine\MissingInputException;

class GenericThread extends AbstractThread
{
    /**
     * @var callable
     */
    private $runFn;
    /**
     * @var callable
     */
    private $isRunnableFn;

    public function __construct(callable $runFn, callable $isRunnableFn)
    {
        parent::__construct();

        $this->runFn        = $runFn;
        $this->isRunnableFn = $isRunnableFn;
    }

    public function isRunnable(): bool
    {
        return call_user_func($this->isRunnableFn, $this->state);
    }

    public function run(): ThreadState
    {
        if (!$this->isRunnable()) {
            throw new \RuntimeException('This thread is not in a runnable state and should not be run');
        }

        try {
            $this->exitCode = call_user_func($this->runFn);
            $this->state    = ThreadState::EXITED;
        } catch (MissingInputException) {
            $this->state = ThreadState::WAITING_ON_INPUT;
        }

        return $this->state;
    }
}
