<?php

namespace App\Day\Y2019\Day11\Thread;

use App\Day\Y2019\Day11\ThreadState;

abstract class AbstractThread
{
    protected int $exitCode;

    protected ThreadState $state = ThreadState::READY;

    public function __construct()
    {
    }

    public function hasEnded(): bool
    {
        return isset($this->exitCode);
    }

    public function getExitCode(): int
    {
        if (!$this->hasEnded()) {
            throw new \RuntimeException('Thread has not ended yet');
        }

        return $this->exitCode;
    }

    public function getState(): ThreadState
    {
        return $this->state;
    }

    /**
     * Test if the thread is in a runnable state.
     *
     * E.g.: a thread waiting on input without input is not in a runnable state
     */
    abstract public function isRunnable(): bool;

    /**
     * Run underlying program.
     *
     * @return ThreadState State of the thread after running
     */
    abstract public function run(): ThreadState;
}
