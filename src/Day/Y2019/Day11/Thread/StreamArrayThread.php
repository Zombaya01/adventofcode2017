<?php

namespace App\Day\Y2019\Day11\Thread;

use App\Day\Y2019\Day11\ThreadState;
use App\Day\Y2019\SignalBuffer;
use App\Day\Y2019\SignalBufferInterface;
use App\Exception\IntCodeMachine\MissingInputException;

/**
 * This thread will add one value from the input to the output-SignalBuffer each time the thread is ran.
 * It will exit 0 after finishing.
 */
class StreamArrayThread extends GenericThread
{
    /**
     * @param int[] $input
     */
    public function __construct(private array $input, private readonly SignalBufferInterface $output)
    {
        if ($this->input !== []) {
            $this->state = ThreadState::READY;
        } else {
            $this->state    = ThreadState::EXITED;
            $this->exitCode = 0;
        }

        $runFn = function (): int {
            $this->output->push(array_shift($this->input));

            if ($this->input === []) {
                // Finish after clearing out the input
                return 0;
            }

            // Make sure to halt the thread after 1 push
            throw new MissingInputException();
        };
        $isRunnableFn = function (ThreadState $state): bool {
            return match ($state) {
                ThreadState::READY            => true,
                ThreadState::EXITED           => false,
                ThreadState::WAITING_ON_INPUT => $this->input !== [], // We can always run to pass on data to another thread, as long as we have input
            };
        };

        parent::__construct($runFn, $isRunnableFn);
    }
}
