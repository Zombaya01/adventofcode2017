<?php

declare(strict_types=1);

namespace App\Day\Y2019\Day11;

enum Direction: int
{
    case Up    = 0;
    case Left  = 1;
    case Down  = 2;
    case Right = 3;

    public function turn(Turn $turn): self
    {
        return match ($turn) {
            Turn::Left  => $this->left(),
            Turn::Right => $this->right(),
        };
    }

    public function left(): self
    {
        return self::from(($this->value + 1) % 4);
    }

    public function right(): self
    {
        return self::from(($this->value + 3) % 4);
    }

    public function x(): int
    {
        return match ($this) {
            self::Left  => -1,
            self::Right => 1,
            default     => 0,
        };
    }

    public function y(): int
    {
        return match ($this) {
            self::Down  => -1,
            self::Up    => 1,
            default     => 0,
        };
    }
}
