<?php

declare(strict_types=1);

namespace App\Day\Y2019\Day11;

enum Turn: int
{
    case Left  = 0;
    case Right = 1;
}
