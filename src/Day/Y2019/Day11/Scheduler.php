<?php

namespace App\Day\Y2019\Day11;

use App\Day\Y2019\Day11\Thread\AbstractThread;

class Scheduler
{
    /** @var array<int|string, AbstractThread> */
    private readonly array $threads;
    private int $maxRounds = 10_000;

    /** @param AbstractThread $threads */
    public function __construct(...$threads)
    {
        $this->threads = $threads;
    }

    public function setMaxRounds(int $maxRounds): void
    {
        $this->maxRounds = $maxRounds;
    }

    public function runUntillAllHaveEnded(): void
    {
        for ($i = 0; $i < $this->maxRounds && !$this->haveAllThreadsEnded() && $this->isAnyThreadRunnable(); ++$i) {
            foreach ($this->threads as $thread) {
                if ($thread->isRunnable()) {
                    $thread->run();
                }
            }
        }

        if ($i === $this->maxRounds) {
            throw new \RuntimeException(sprintf('Run aborted after %d rounds',$i));
        }

        if (!$this->haveAllThreadsEnded()) {
            throw new \RuntimeException('ThreadRunner is stuck - no threads are in a runnable shape but not all threads have ended');
        }
    }

    public function runUntillNumberOfThreadsHaveEnded(int $number): void
    {
        for ($i = 0; $i < $this->maxRounds && !$this->haveNumberOfThreadsEnded($number) && $this->isAnyThreadRunnable(); ++$i) {
            foreach ($this->threads as $thread) {
                if ($thread->isRunnable()) {
                    $thread->run();
                }
            }
        }

        if ($i === $this->maxRounds) {
            throw new \RuntimeException(sprintf('Run aborted after %d rounds',$i));
        }

        if (!$this->haveNumberOfThreadsEnded($number)) {
            throw new \RuntimeException('ThreadRunner is stuck - no threads are in a runnable shape but not all threads have ended');
        }
    }

    private function haveAllThreadsEnded(): bool
    {
        return array_reduce($this->threads,fn (bool $carry, AbstractThread $thread): bool => $carry && $thread->hasEnded(), true);
    }

    private function isAnyThreadRunnable(): bool
    {
        return array_reduce($this->threads,fn (bool $carry, AbstractThread $thread): bool => $carry || $thread->isRunnable(), false);
    }

    private function haveNumberOfThreadsEnded(int $number): bool
    {
        return array_sum(array_map(fn (AbstractThread $thread): int => $thread->hasEnded() ? 1 : 0, $this->threads)) >= $number;
    }
}
