<?php

namespace App\Day\Y2019;

use App\Exception\IntCodeMachine as IntCodeMachineException;

class SignalBufferWithMemory implements SignalBufferInterface
{
    private int $position = 0;

    /**
     * @param array<int,int> $buffer
     */
    public function __construct(private array $buffer = [])
    {
        foreach ($buffer as $item) {
            if (!is_int($item)) {
                throw new \InvalidArgumentException('Input invalid!'.var_export($buffer, true));
            }
        }
    }

    public function push(int $value): void
    {
        $this->buffer[] = $value;
    }

    public function shift(): int
    {
        if ($this->buffer === []) {
            throw new IntCodeMachineException\MissingInputException('Ran out of input');
        }

        $value = $this->buffer[$this->position];
        ++$this->position;

        return $value;
    }

    public function last(): ?int
    {
        return $this->buffer[array_key_last($this->buffer)] ?? null;
    }

    /** @return array<int,int> */
    public function toArray(): array
    {
        return array_slice($this->buffer,$this->position);
    }

    /** @return array<int,int> */
    public function allValues(): array
    {
        return $this->buffer;
    }

    public function count(): int
    {
        return count($this->buffer) - $this->position;
    }
}
