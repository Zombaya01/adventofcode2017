<?php

declare(strict_types=1);

namespace App\Day\Y2019;

use App\Day\AbstractDay;
use App\Day\Y2019\Day11\Color;
use App\Day\Y2019\Day11\PaintMachine;
use App\Day\Y2019\Day11\Scheduler;
use App\Day\Y2019\Day11\Thread\AbstractThread;
use App\Day\Y2019\Day11\Thread\RunnableThread;
use App\Utils\DataConverter;
use App\Utils\OCR;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day11 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input          = DataConverter::rowOfInt($puzzleInput,',');
        $intCodeMachine = new IntCodeMachine($input,new SignalBuffer(), new SignalBufferWithMemory());

        return $this
            ->runPart1(new RunnableThread($intCodeMachine),$intCodeMachine->getOutput(), $intCodeMachine->getInput(), Color::Black)
            ->getNumberOfPaintedPanels();
    }

    public function runPart1(AbstractThread $strategyCalculator, SignalBufferInterface $inputForPaintMachine, SignalBufferInterface $outputForPaintMachine, Color $initialColor): PaintMachine
    {
        $paintMachine = new PaintMachine($inputForPaintMachine,$outputForPaintMachine, $initialColor);
        $scheduler    = new Scheduler(
            $strategyCalculator,
            new RunnableThread($paintMachine)
        );

        $scheduler->setMaxRounds(1_000_000);

        $scheduler->runUntillNumberOfThreadsHaveEnded(1);

        if ($inputForPaintMachine instanceof SignalBufferWithMemory) {
            $allInputForPaintMachine = $inputForPaintMachine->allValues();
        }

        return $paintMachine;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input          = DataConverter::rowOfInt($puzzleInput,',');
        $intCodeMachine = new IntCodeMachine($input,new SignalBuffer(), new SignalBufferWithMemory());

        $output = $this
            ->runPart1(new RunnableThread($intCodeMachine),$intCodeMachine->getOutput(), $intCodeMachine->getInput(), Color::White)
            ->paint();

        return (new OCR())->scan($output);
    }
}
