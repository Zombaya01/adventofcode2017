<?php

namespace App\Day\Y2019;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use App\Utils\OCR;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day8 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::rowOfStrings($puzzleInput);

        $x        = (int) $parsedInput[0];
        $y        = (int) $parsedInput[1];
        $linesize = $x * $y;

        $data       = mb_str_split((string) $parsedInput[2],1);
        $dataPerRow = array_chunk($data,$linesize);

        $fewest = PHP_INT_MAX;
        $result = -1;
        foreach ($dataPerRow as $rowData) {
            $count = [
                '0' => 0,
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,
                '5' => 0,
                '6' => 0,
                '7' => 0,
                '8' => 0,
                '9' => 0,
            ];
            foreach ($rowData as $i => $rowDatum) {
                ++$count[$rowDatum];
            }
            if ($count['0'] < $fewest) {
                $fewest = $count['0'];
                $result = $count['1'] * $count['2'];
            }
        }

        return $result;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $parsedInput = DataConverter::rowOfStrings($puzzleInput);

        $x        = (int) $parsedInput[0];
        $y        = (int) $parsedInput[1];
        $linesize = $x * $y;

        $data       = mb_str_split((string) $parsedInput[2],1);
        $dataPerRow = array_reverse(array_chunk($data,$linesize));

        $fewest = PHP_INT_MAX;
        $result = -1;
        $output = [];

        for ($i = 0; $i < $linesize; ++$i) {
            $output[$i] = '$';
        }

        foreach ($dataPerRow as $rowData) {
            $count = [
                '0' => 0,
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,
                '5' => 0,
                '6' => 0,
                '7' => 0,
                '8' => 0,
                '9' => 0,
            ];
            for ($i = 0, $iMax = count($rowData); $i < $iMax; ++$i) {
                ++$count[$rowData[$i]];
                switch ($rowData[$i]) {
                    case '0': // Black
                        $output[$i] = '.';
                        break;
                    case '1': // White
                        $output[$i] = '#';
                        break;
                    case '2': // Transparent
                        break;
                }
            }
            if ($count['0'] < $fewest) {
                $fewest = $count['0'];
                $result = $count['1'] * $count['2'];
            }
        }

        $outputLines = array_chunk($output,$x);
        $strOutput   = '';
        foreach ($outputLines as $outputLine) {
            $strOutput .= implode('',$outputLine)."\n";
        }

        return (new OCR())->scan($strOutput);
    }
}
