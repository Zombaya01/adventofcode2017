<?php

namespace App\Day\Y2019;

use App\Day\AbstractDay;
use App\Day\Y2019\Day11\Scheduler;
use App\Day\Y2019\Day11\Thread\RunnableThread;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day7 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input        = DataConverter::rowOfInt($puzzleInput,',');
        $permutations = $this->getPermutations();

        $max     = 0;
        foreach ($permutations as $permutation) {
            $previousOutput = 0;
            foreach ($permutation as $phase) {
                $machine = new IntCodeMachine($input,new SignalBuffer([$phase, $previousOutput]), new SignalBuffer());
                $machine->run();
                $previousOutput = $machine->getOutput()->shift();
            }
            $max = max($previousOutput, $max);
        }

        return $max;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input        = DataConverter::rowOfInt($puzzleInput,',');
        $permutations = $this->getPermutationsPart2();

        $max     = 0;
        foreach ($permutations as $permutation) {
            $inputA   = new SignalBuffer([$permutation[0], 0]);
            $inputB   = new SignalBuffer([$permutation[1]]);
            $inputC   = new SignalBuffer([$permutation[2]]);
            $inputD   = new SignalBuffer([$permutation[3]]);
            $inputE   = new SignalBuffer([$permutation[4]]);

            $machineA = new IntCodeMachine($input,$inputA, $inputB);
            $machineB = new IntCodeMachine($input,$inputB, $inputC);
            $machineC = new IntCodeMachine($input,$inputC, $inputD);
            $machineD = new IntCodeMachine($input,$inputD, $inputE);
            $machineE = new IntCodeMachine($input,$inputE, $inputA);

            $scheduler = new Scheduler(
                new RunnableThread($machineA),
                new RunnableThread($machineB),
                new RunnableThread($machineC),
                new RunnableThread($machineD),
                new RunnableThread($machineE),
            );

            $scheduler->runUntillAllHaveEnded();

            $previousOutput = $inputA->shift();

            $max = max($previousOutput, $max);
        }

        return $max;
    }

    /**
     * @return int[][]
     */
    private function getPermutations(): array
    {
        $results = [];
        for ($i = 0; $i < 5; ++$i) {
            for ($j = 0; $j < 5; ++$j) {
                for ($k = 0; $k < 5; ++$k) {
                    for ($l = 0; $l < 5; ++$l) {
                        for ($m = 0; $m < 5; ++$m) {
                            if (true && $i !== $j && $i !== $k && $i !== $l && $i !== $m
                                     && $j !== $k && $j !== $l && $j !== $m
                                     && $k !== $l && $k !== $m
                                     && $l !== $m) {
                                $results[] = [$i, $j, $k, $l, $m];
                            }
                        }
                    }
                }
            }
        }

        return $results;
    }

    /**
     * @return int[][]
     */
    private function getPermutationsPart2(): array
    {
        $results = [];
        for ($i = 5; $i < 10; ++$i) {
            for ($j = 5; $j < 10; ++$j) {
                for ($k = 5; $k < 10; ++$k) {
                    for ($l = 5; $l < 10; ++$l) {
                        for ($m = 5; $m < 10; ++$m) {
                            if (true && $i !== $j && $i !== $k && $i !== $l && $i !== $m
                                     && $j !== $k && $j !== $l && $j !== $m
                                     && $k !== $l && $k !== $m
                                     && $l !== $m) {
                                $results[] = [$i, $j, $k, $l, $m];
                            }
                        }
                    }
                }
            }
        }

        return $results;
    }
}
