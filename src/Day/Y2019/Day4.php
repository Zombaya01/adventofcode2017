<?php

namespace App\Day\Y2019;

use App\Day\AbstractDay;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day4 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        for ($i = 138307; $i <= 654504; ++$i) {
            $input[] = (string) $i;
        }

        $validCount = 0;
        foreach ($input as $password) {
            $max       = 0;
            $hasDouble = false;
            for ($i = 0; $i < 6; ++$i) {
                // Test if always increasing
                if ((int) $password[$i] < $max) {
                    continue 2;
                }
                $max = max($max,(int) $password[$i]);

                if ($i < 5 && $password[$i] === $password[$i + 1]) {
                    $hasDouble = true;
                }
            }
            if (!$hasDouble) {
                continue;
            }
            ++$validCount;
        }

        return $validCount;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        for ($i = 138307; $i <= 654504; ++$i) {
            $input[] = (string) $i;
        }

        $validCount = 0;
        foreach ($input as $password) {
            $max       = 0;
            $hasDouble = false;
            for ($i = 0; $i < 6; ++$i) {
                // Test if always increasing
                if ((int) $password[$i] < $max) {
                    continue 2;
                }
                $max = max($max,(int) $password[$i]);

                if ($i < 5
                    && $password[$i] === $password[$i + 1]
                    && ($i === 4 || $password[$i + 1] !== $password[$i + 2])
                    && ($i === 0 || $password[$i - 1] !== $password[$i])
                ) {
                    $hasDouble = true;
                }
            }
            if (!$hasDouble) {
                continue;
            }
            ++$validCount;
        }

        return $validCount;
    }
}
