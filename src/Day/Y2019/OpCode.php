<?php

namespace App\Day\Y2019;

enum OpCode: int
{
    case Add                = 1;
    case Multiply           = 2;
    case Store              = 3;
    case Output             = 4;
    case JumpIfTrue         = 5;
    case JumpIfFalse        = 6;
    case LessThan           = 7;
    case Equals             = 8;
    case RelativeBaseOffset = 9;
    case Return             = 99;
}
