<?php

namespace App\Day\Y2019;

use App\Day\AbstractDay;
use App\Utils\DataConverter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day5 extends AbstractDay
{
    public function part1(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $machine    = new IntCodeMachine(DataConverter::rowOfInt($puzzleInput,','),new SignalBuffer([1]),new SignalBuffer());

        $machine->run();

        return $machine->getOutput()->last() ?: -1;
    }

    public function part2(string $puzzleInput, ?InputInterface $consoleInput = null, ?OutputInterface $consoleOutput = null): int|string|null
    {
        $input      = DataConverter::rowOfInt($puzzleInput,',');
        $machine    = new IntCodeMachine($input, new SignalBuffer([5]), new SignalBuffer());

        $machine->run();

        return $machine->getOutput()->last() ?: -1;
    }
}
