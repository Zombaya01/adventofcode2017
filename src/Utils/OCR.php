<?php

declare(strict_types=1);

namespace App\Utils;

class OCR
{
    private const fonts = [
        'ABCEFGHIJKLOPRSUYZ' => <<<'EOF'
        .##..###...##..####.####..##..#..#.###...##.#..#.#.....##..###..###...###.#..#.#...#.####
        #..#.#..#.#..#.#....#....#..#.#..#..#.....#.#.#..#....#..#.#..#.#..#.#....#..#.#...#....#
        #..#.###..#....###..###..#....####..#.....#.##...#....#..#.#..#.#..#.#....#..#..#.#....#.
        ####.#..#.#....#....#....#.##.#..#..#.....#.#.#..#....#..#.###..###...##..#..#...#....#..
        #..#.#..#.#..#.#....#....#..#.#..#..#..#..#.#.#..#....#..#.#....#.#.....#.#..#...#...#...
        #..#.###...##..####.#.....###.#..#.###..##..#..#.####..##..#....#..#.###...##....#...####
        EOF,
        'ABCEFGHJKLNPRXZ'    => <<<'EOF'
        ..##...#####...####..######.######..####..#....#....###.#....#.#......#....#.#####..#####..#....#.######
        .#..#..#....#.#....#.#......#......#....#.#....#.....#..#...#..#......##...#.#....#.#....#.#....#......#
        #....#.#....#.#......#......#......#......#....#.....#..#..#...#......##...#.#....#.#....#..#..#.......#
        #....#.#....#.#......#......#......#......#....#.....#..#.#....#......#.#..#.#....#.#....#..#..#......#.
        #....#.#####..#......#####..#####..#......######.....#..##.....#......#.#..#.#####..#####....##......#..
        ######.#....#.#......#......#......#..###.#....#.....#..##.....#......#..#.#.#......#..#.....##.....#...
        #....#.#....#.#......#......#......#....#.#....#.....#..#.#....#......#..#.#.#......#...#...#..#...#....
        #....#.#....#.#......#......#......#....#.#....#.#...#..#..#...#......#...##.#......#...#...#..#..#.....
        #....#.#....#.#....#.#......#......#...##.#....#.#...#..#...#..#......#...##.#......#....#.#....#.#.....
        #....#.#####...####..######.#.......###.#.#....#..###...#....#.######.#....#.#......#....#.#....#.######
        EOF,
    ];

    /**
     * @var array<int,array<string,string>>
     */
    private array $fontMaps = [];

    public function __construct()
    {
        foreach (self::fonts as $analyzed => $font) {
            $letterShapes                                      = $this->splitLetters($font);
            $this->fontMaps[$this->calculateFontHeight($font)] = array_combine(
                $letterShapes,
                str_split($analyzed),
            );
        }
    }

    private function calculateFontHeight(string $input): int
    {
        return count(explode("\n",trim($input)));
    }

    /**
     * Splits single string with multiple lettershapes in an array of lettershapes.
     *
     * @return array<string>
     */
    public function splitLetters(string $input): array
    {
        $rowsOfStrings = explode("\n",$input);
        $firstRow      = $rowsOfStrings[0];
        $columnMax     = strlen($firstRow);
        $fontHeight    = count($rowsOfStrings);
        $currentLetter = array_fill(0, $fontHeight,'');
        $result        = [];

        for ($column = 0; $column < $columnMax; ++$column) {
            $shouldBreak = true;

            foreach ($rowsOfStrings as $key => $row) {
                if ($row[$column] === '#') {
                    $shouldBreak = false;
                }
            }

            if ($shouldBreak) {
                if ($currentLetter[0] !== '') {
                    $result[]      = $currentLetter;
                    $currentLetter = array_fill(0,$fontHeight,'');
                }
            } else {
                foreach ($rowsOfStrings as $key => $row) {
                    $currentLetter[$key] .= $row[$column];
                }
            }
        }

        if ($currentLetter[0] !== '') {
            $result[] = $currentLetter;
        }

        return array_map(fn ($letter): string => implode("\n",$letter), $result);
    }

    public function scan(string $input): string
    {
        $input = trim($input);

        $letterShapes = $this->splitLetters($input);
        $fontHeight   = $this->calculateFontHeight($input);

        if (!isset($this->fontMaps[$fontHeight])) {
            throw new \RuntimeException(sprintf('No font known with height %d',$fontHeight));
        }

        $output = '';
        foreach ($letterShapes as $letterShape) {
            if (!isset($this->fontMaps[$fontHeight][$letterShape])) {
                throw new \RuntimeException(sprintf("Unknown lettershape for fontheight %d\n\n%s",$fontHeight,$letterShape));
            }
            $output .= $this->fontMaps[$fontHeight][$letterShape];
        }

        return $output;
    }
}
