<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Utils;

/**
 * This class is a light interface between an external Markdown parser library
 * and the application. It's generally recommended to create these light interfaces
 * to decouple your application from the implementation details of the third-party library.
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class DataConverter
{
    private static function table(string $input, DataConverterRowType $type, string $separator = ' '): array
    {
        $lines = explode("\n",trim($input));

        $output = [];
        foreach ($lines as $line) {
            $output[] = self::row($line,$type,$separator);
        }

        return $output;
    }

    private static function row(string $row, DataConverterRowType $type, string $separator = ' '): array
    {
        $row      = preg_replace('/ +/',' ',$row);
        $splitRow = $separator === '' ? mb_str_split(trim($row),1) : explode($separator,trim($row));
        $splitRow = array_map('trim',$splitRow);

        return match ($type) {
            DataConverterRowType::int   => array_map('intval', $splitRow),
            DataConverterRowType::float => array_map('floatval', $splitRow),
            default                     => $splitRow,
        };
    }

    /** @return array<int> */
    public static function rowOfInt(string $row, string $separator = ' '): array
    {
        return self::row($row,DataConverterRowType::int, $separator);
    }

    /** @return array<string> */
    public static function rowOfStrings(string $row, string $separator = ' '): array
    {
        return self::row($row,DataConverterRowType::string, $separator);
    }

    /** @return array<array<int>> */
    public static function tableOfInt(string $input): array
    {
        return self::table($input,DataConverterRowType::int);
    }

    /** @return array<array<int>> */
    public static function tableOfIntNumbers(string $input): array
    {
        return self::table($input,DataConverterRowType::int,'');
    }

    /** @return array<array<string>> */
    public static function tableOfStrings(string $input, string $separator = ' '): array
    {
        return self::table($input,DataConverterRowType::string,$separator);
    }

    /** @return array<int> */
    public static function columnOfInt(string $column): array
    {
        return array_column(self::tableOfInt($column),0);
    }

    /** @return array<array<int>> */
    public static function columnOfGroupedIntsSplitByBlankLine(string $input): array
    {
        $lines = explode("\n\n",trim($input));

        return array_map(fn ($row): array => trim($row) !== '' ? self::columnOfInt($row) : [],$lines);
    }

    /**
     * @param non-empty-string $delimiter
     *
     * @return array<string>
     */
    public static function columnOfStrings(string $input, string $delimiter = "\n"): array
    {
        $lines = explode($delimiter,trim($input));
        array_walk($lines,function (&$el): void {$el = trim($el); });

        return $lines;
    }

    /** @return array<string> */
    public static function columnOfStringsSplitByBlankLine(string $input): array
    {
        $lines = explode("\n\n",trim($input));
        array_walk($lines,function (&$el): void {$el = trim($el); });

        return $lines;
    }

    public static function humanReadableFilesize(int $bytes, int $decimals = 2): string
    {
        $size   = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $factor = floor((mb_strlen((string) $bytes) - 1) / 3);

        return sprintf("%.{$decimals}f", $bytes / 1024 ** $factor).@$size[$factor];
    }
}
