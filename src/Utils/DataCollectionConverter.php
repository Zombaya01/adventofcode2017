<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Utils;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class DataCollectionConverter
{
    /** @return Collection<int,int> */
    public static function rowOfInt(string $row, string $separator = ' '): Collection
    {
        return new ArrayCollection(DataConverter::rowOfInt($row, $separator));
    }

    /** @return Collection<int,string> */
    public static function rowOfStrings(string $row, string $separator = ' '): Collection
    {
        return new ArrayCollection(DataConverter::rowOfStrings($row, $separator));
    }

    /** @return Collection<int,int> */
    public static function columnOfInt(string $column): Collection
    {
        return new ArrayCollection(DataConverter::columnOfInt($column));
    }

    /**
     * @param non-empty-string $delimiter
     *
     * @return Collection<int,string>
     */
    public static function columnOfStrings(string $input, string $delimiter = "\n"): Collection
    {
        return new ArrayCollection(DataConverter::columnOfStrings($input,  $delimiter));
    }

    /** @return Collection<int,string> */
    public static function columnOfStringsSplitByBlankLine(string $input): Collection
    {
        return new ArrayCollection(DataConverter::columnOfStringsSplitByBlankLine($input));
    }
}
