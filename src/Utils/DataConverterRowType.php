<?php

namespace App\Utils;

enum DataConverterRowType
{
    case int;
    case string;
    case float;
}
