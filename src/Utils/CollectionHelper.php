<?php

declare(strict_types=1);

namespace App\Utils;

class CollectionHelper
{
    public static function sum(?int $carry, int $value): int
    {
        $carry ??= 0;

        return $carry + $value;
    }

    public static function product(?int $carry, int $value): int
    {
        $carry ??= 1;

        return $carry * $value;
    }

    public static function min(?int $carry, int $value): int
    {
        $carry ??= PHP_INT_MAX;

        return min($carry, $value);
    }

    public static function max(?int $carry, int $value): int
    {
        $carry ??= PHP_INT_MIN;

        return max($carry, $value);
    }
}
