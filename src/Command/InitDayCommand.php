<?php

declare(strict_types=1);

namespace App\Command;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Utils;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\ErrorHandler\ErrorRenderer\FileLinkFormatter;
use Symfony\Component\HttpFoundation\File\File;

#[AsCommand('app:init-day')]
class InitDayCommand extends Command
{
    private int $day;
    private int $year;

    public function __construct(
        private readonly string $projectDirectory,
        private readonly string $sessionToken,
        private readonly ?string $proxyPort,
        private readonly \Twig\Environment $twig,
        private readonly FileLinkFormatter $fileLinkFormatter,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('day', 'd', InputOption::VALUE_OPTIONAL, 'which day to calculate')
            ->addOption('year','y',InputOption::VALUE_OPTIONAL,'',date('Y'))
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);
        ini_set('memory_limit','12G');

        $day  = $input->getOption('day');
        $year = (int) $input->getOption('year');

        if ($day === null) {
            throw new InvalidOptionException('Day should be set');
        }

        if ($year < 2015 || $year > date('Y')) {
            throw new InvalidOptionException('Year outside of allowed range');
        }

        $this->day  = (int) $day;
        $this->year = $year;
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->testIfExists();

        $this->writeInputFile($output);
        $this->writeDayClassFile($output);
        $this->writeTestFile($output);

        return self::SUCCESS;
    }

    private function getDayClassPath(): File
    {
        return new File(sprintf('%s/src/Day/Y%d/Day%d.php',$this->projectDirectory,$this->year,$this->day),false);
    }

    private function getTestClassPath(): File
    {
        return new File(sprintf('%s/tests/Day/Y%d/Day%dTest.php',$this->projectDirectory,$this->year,$this->day),false);
    }

    private function getInputPath(): File
    {
        return new File(sprintf('%s/input/y%d/day%d',$this->projectDirectory,$this->year,$this->day),false);
    }

    private function testIfExists(): void
    {
        if ($this->getDayClassPath()->isFile()) {
            throw new LogicException(sprintf('file %s for year %d day %d allready exists','dayclass',$this->year,$this->day));
        }
        if ($this->getTestClassPath()->isFile()) {
            throw new LogicException(sprintf('file %s for year %d day %d allready exists','test',$this->year,$this->day));
        }
        if ($this->getInputPath()->isFile()) {
            throw new LogicException(sprintf('file %s for year %d day %d allready exists','input',$this->year,$this->day));
        }
    }

    private function writeFile(File $filepath, string $contents): void
    {
        if (!file_exists($filepath->getPath()) && (!mkdir($concurrentDirectory = $filepath->getPath()) && !is_dir($concurrentDirectory))) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        file_put_contents($filepath->getPathname(),$contents);
    }

    private function writeDayClassFile(OutputInterface $output): void
    {
        $contents = $this->twig->render('InitDay/Day.php.twig',[
            'year' => $this->year,
            'day'  => $this->day,
        ]);

        $filepath = $this->getDayClassPath();
        $this->writeFile($filepath,$contents);

        $this->writeFilePathToOutput($output, $filepath, 'class');
    }

    private function writeTestFile(OutputInterface $output): void
    {
        $contents = $this->twig->render('InitDay/Test.php.twig',[
            'year' => $this->year,
            'day'  => $this->day,
        ]);

        $filepath = $this->getTestClassPath();
        $this->writeFile($filepath,$contents);
        $this->writeFilePathToOutput($output, $filepath, 'test');
    }

    private function writeInputFile(OutputInterface $output): void
    {
        $client = new \GuzzleHttp\Client();

        $params = [
            'headers' => [
                'Cookie'     => sprintf('session=%s',$this->sessionToken),
                'User-Agent' => sprintf('%s gitlab.com/Zombaya01/adventofcode2017',Utils::defaultUserAgent()),
            ],
        ];

        if ($this->proxyPort !== null && $this->proxyPort !== '' && $this->proxyPort !== '0') {
            $params['proxy']  = $this->proxyPort;
            $params['verify'] = false;
        }

        try {
            $response = $client->request(
                'GET',
                sprintf('https://adventofcode.com/%d/day/%d/input',$this->year,$this->day),
                $params
            );
        } catch (RequestException $e) {
            throw new RuntimeException(sprintf('Unable to fetch input. Reason: %s',$e->getResponse()->getBody()->getContents()),$e->getCode(),$e);
        }

        $filepath = $this->getInputPath();
        $this->writeFile($filepath,$response->getBody()->getContents());
        $this->writeFilePathToOutput($output, $filepath, 'input');
    }

    private function writeFilePathToOutput(OutputInterface $output, File $file, string $type): void
    {
        $output->writeln(
            sprintf(
                '%s written to <href=%s>%s</>',
                ucfirst($type),
                $this->fileLinkFormatter->format($file->getPathName(),1),
                $file->getPathName(),
            )
        );
    }
}
