<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

#[AsCommand('app:calculate-day')]
class CalculateDay extends Command
{
    /** @var array<string> */
    private array $daysToRun = [];

    public function __construct()
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('day', 'd', InputOption::VALUE_OPTIONAL, 'which day to calculate')
            ->addOption('part', 'p', InputOption::VALUE_OPTIONAL, 'which part to calculate')
            ->addOption('year','y',InputOption::VALUE_OPTIONAL,'',date('Y'))
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);
        ini_set('memory_limit','12G');

        $day  = $input->getOption('day');
        $year = (int) $input->getOption('year');

        if ($day !== null) {
            if ($day === 'latest') {
                for ($i = 25; $i > 0; --$i) {
                    if (class_exists($this->getClassName($year,$i))) {
                        $this->daysToRun[$i] = $this->getClassName($year,$i);
                        break;
                    }
                }
            } elseif (is_numeric($day) && class_exists($this->getClassName($year,$day = (int) $day))) {
                $this->daysToRun[$day] = $this->getClassName($year,$day);
            }
        } else {
            for ($i = 1; $i <= 25; ++$i) {
                if (class_exists($this->getClassName($year,$i))) {
                    $this->daysToRun[$i] = $this->getClassName($year,$i);
                }
            }
        }
    }

    private function getClassName(int $year, int $day): string
    {
        return sprintf("\App\Day\Y%d\Day%d",$year,$day);
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($this->daysToRun === []) {
            $output->writeln('No tests to be ran!');

            return self::FAILURE;
        }

        $outputs   = [];
        $stopwatch = new Stopwatch();
        $stopwatch->start('total');
        foreach ($this->daysToRun as $day => $classname) {
            $t = [];
            $output->writeln('Test '.$classname);
            if (class_exists($classname)) {
                $class    = new $classname();
                $t['day'] = $day;
                $stopwatch->start($classname);
                $t['part1'] = method_exists($class,'part1command') ? $class->part1command($input,$output) : '';
                $stopwatch->lap($classname);
                $class      = new $classname();
                $t['part2'] = method_exists($class,'part2command') ? $class->part2command($input,$output) : '';
                $event      = $stopwatch->stop($classname);
                $durations  = $event->getPeriods();
                array_walk($durations,function (&$el): void { $el = sprintf('%.3fs',$el->getDuration() / 1000); });
                $t         = array_merge($t,$durations);
                $outputs[] = $t;
            }
        }
        $total = $stopwatch->stop('total');

        // In your console commands you should always use the regular output type,
        // which outputs contents directly in the console window. However, this
        // command uses the BufferedOutput type instead, to be able to get the output
        // contents before displaying them. This is needed because the command allows
        // to send the list of users via email with the '--send-to' option
        $io = new SymfonyStyle($input, $output);
        $io->table(
            ['day', 'part1', 'part2', 'timePart1', 'timePart2'],
            $outputs
        );
        $output->writeln(sprintf('Total time:   %.3fs',$total->getDuration() / 1000));
        $output->writeln('Total memory: '.\App\Utils\DataConverter::humanReadableFilesize($total->getMemory()));
        // $output->write(\App\Entity\Day1::part2command(),true);

        return self::SUCCESS;
    }
}
