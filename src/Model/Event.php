<?php

declare(strict_types=1);

namespace App\Model;

class Event
{
    public function __construct(
        private readonly int $year,
        /** @var array<Day> $days */
        private readonly array $days,
    ) {
    }

    public function getYear(): int
    {
        return $this->year;
    }

    /** @return array<Day> */
    public function getDays(): array
    {
        return $this->days;
    }
}
