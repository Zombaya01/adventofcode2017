<?php

declare(strict_types=1);

namespace App\Model;

class Day
{
    public function __construct(
        private readonly int $day,
        private readonly int $year,
    ) {
    }

    public function getDay(): int
    {
        return $this->day;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function hasImplementation(): bool
    {
        return class_exists(sprintf('App\\Day\\Y%d\\Day%d',$this->year,$this->day));
    }
}
