<?php

namespace App\Exception;

class NotImplementedException extends \Exception
{
    public function __construct()
    {
        parent::__construct();
    }
}
