<?php

namespace App\Exception;

class NoSolutionFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct();
    }
}
